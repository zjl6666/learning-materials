package com.example.mysql.学习笔记.高级.事务.MySQL事务日志.redo日志;

public class redo整体流程_4 {
}
/**
 * 1.4 redo的整体流程
 * 以一个更新事务为例，redo log流转过程，如下图所示:
 *
 * 第1步:先将原始数据从磁盘中读入内存中来，修改数据的内存拷贝
 * 第2步:生成一条重做日志并写入redo log buffer. 记录的是数据被修改后的值
 * 第3步:当事务commit时，将redo log buffer中的内容刷新到redo log file，
 *      对redo log file采用追加写的方式
 * 第4步:定期将内存中修改的数据刷新到磁盘中(redo log file --->磁盘真正的位置)
 *
 *
 */
