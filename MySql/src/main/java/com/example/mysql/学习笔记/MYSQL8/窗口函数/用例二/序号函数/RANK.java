package com.example.mysql.学习笔记.MYSQL8.窗口函数.用例二.序号函数;

public class RANK {
}

/**
 * **RANK()函数**
 *
 * 使用RANK()函数能够对序号进行并列排序，重复会跳过重复的序号，比如序号为1、1、3。
 *
 * 举例：使用RANK()函数获取 goods 数据表中各类别的价格从高到低排序的各商品信息。
 *
 * ```
 * mysql> SELECT RANK() OVER(PARTITION BY category_id ORDER BY price DESC) AS row_num,
 *     -> id, category_id, category, NAME, price, stock
 *     -> FROM goods;
 * +---------+----+-------------+---------------+------------+---------+-------+
 * | row_num | id | category_id | category      | NAME       | price   | stock |
 * +---------+----+-------------+---------------+------------+---------+-------+
 * |       1 |  6 |           1 | 女装/女士精品   | 呢绒外套    |  399.90 |  1200 |
 * |       2 |  3 |           1 | 女装/女士精品   | 卫衣        |   89.90 |  1500 |
 * |       2 |  4 |           1 | 女装/女士精品   | 牛仔裤      |   89.90 |  3500 |
 * |       4 |  2 |           1 | 女装/女士精品   | 连衣裙      |   79.90 |  2500 |
 * |       5 |  1 |           1 | 女装/女士精品   | T恤         |   39.90 |  1000 |
 * |       6 |  5 |           1 | 女装/女士精品   | 百褶裙      |   29.90 |   500 |
 * |       1 |  8 |           2 | 户外运动       | 山地自行车   | 1399.90 |  2500 |
 * |       2 | 11 |           2 | 户外运动       | 运动外套     |  799.90 |   500 |
 * |       3 | 12 |           2 | 户外运动       | 滑板        |  499.90 |  1200 |
 * |       4 |  7 |           2 | 户外运动       | 自行车      |  399.90 |  1000 |
 * |       4 | 10 |           2 | 户外运动       | 骑行装备    |  399.90 |  3500 |
 * |       6 |  9 |           2 | 户外运动       | 登山杖      |   59.90 |  1500 |
 * +---------+----+-------------+---------------+------------+---------+-------+
 * 12 rows in set (0.00 sec)
 * ```
 *
 * 举例：使用RANK()函数获取 goods 数据表中类别为“女装/女士精品”的价格最高的4款商品信息。
 *
 * ```mysql
 * mysql> SELECT *
 *     -> FROM(
 *     ->  SELECT RANK() OVER(PARTITION BY category_id ORDER BY price DESC) AS row_num,
 *     ->  id, category_id, category, NAME, price, stock
 *     ->  FROM goods) t
 *     -> WHERE category_id = 1 AND row_num <= 4;
 * +---------+----+-------------+---------------+----------+--------+-------+
 * | row_num | id | category_id | category      | NAME     | price  | stock |
 * +---------+----+-------------+---------------+----------+--------+-------+
 * |       1 |  6 |           1 | 女装/女士精品   | 呢绒外套  | 399.90 |  1200 |
 * |       2 |  3 |           1 | 女装/女士精品   | 卫衣      |  89.90 |  1500 |
 * |       2 |  4 |           1 | 女装/女士精品   | 牛仔裤    |  89.90 |  3500 |
 * |       4 |  2 |           1 | 女装/女士精品   | 连衣裙    |  79.90 |  2500 |
 * +---------+----+-------------+---------------+----------+--------+-------+
 * 4 rows in set (0.00 sec)
 * ```
 *
 * 可以看到，使用RANK()函数得出的序号为1、2、2、4，相同价格的商品序号相同，后面的商品序号是不连续的，跳过了重复的序号。
 */
