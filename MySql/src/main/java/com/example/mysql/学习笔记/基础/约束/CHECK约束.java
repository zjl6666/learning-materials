package com.example.mysql.学习笔记.基础.约束;

public class CHECK约束 {
}
/**
 * CHECK
 * 检查某个字段的值是否符号xx要求，一般指的是值的范围
 *
 * ### 说明：MySQL 5.7 不支持
 *
 * MySQL5.7 可以使用check约束，但check约束对数据验证没有任何作用。
 *          添加数据时，没有任何错误或警告
 * 但是**MySQL 8.0中可以使用check约束了**。
 *
 *
 * CREATE TABLE IF NOT EXISTS 表名(
 * 字段名 数据类型 CHECK(限制条件),
 * 字段名 数据类型,
 *
 * //表级约束   CONSTRAINT 约束名字   可不要
 * CONSTRAINT 约束名字 FOREIGN KEY(本表字段) REFERENCES 父表(父表中的列)
 * );
 */