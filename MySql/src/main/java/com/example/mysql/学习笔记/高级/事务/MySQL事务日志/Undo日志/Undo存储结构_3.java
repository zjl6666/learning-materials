package com.example.mysql.学习笔记.高级.事务.MySQL事务日志.Undo日志;

public class Undo存储结构_3 {
}
/**
 * 2.3 undo的存储结构
 * 1.回滚段与undo页
 * InnoDB对undo log的管理采用段的方式，也就是回滚段(rollback segment) 。
 * 每个回滚段记录了1024 个 undo log segment ，//表示最多同时执行1024个事务
 * 而在每个 undo log segment 段中进行undo页的申请。
 * ●在InnoDB 1.1版本之前(不包括1.1版本) ,只有一个 rollback segment ,
 * 因此支持同时在线的事务限制为 1024。虽然对绝大多数的应用来说都已经够用。
 * ●从1.1版本开始InnoDB支持最大128个rollback segment ，
 * 故其支持同时在线的事务限制提高到了128*1024。
 *
 * 虽然InnoDB 1.1版本支持了128个rollback segment,
 * 但是这些rollback segment都存储于共享表空间ibdata中。
 * 从 InnoDB1.2版本开始，可通过参数对rollback segment做进一步的设置。
 * 这些参数包括:
 * ●innodb_undo_directory :设置rollback segment文件所在的路径。
 * 这意味着rollback segment可以存放在共享表空间以外的位置，即可以设置为独立表空间。
 * 该参数的默认值为“./” ,表示当前InnoDB存储引擎的目录。
 * ●innodb_undo_logs :设置rollback segment的个数,默认值为128。
 * 在InnoDB 1.2版本中, 该参数用来替换之前版本的参数innodb_rollback_segments。
 * ●innodb_undo_tablespaces :设置构成rollback segment文件的数量,
 * 这样rollback segment可以较为平均地分布在多个文件中。
 * 设置该参数后，会在路径innodb_undo_directory看到undo为前缀的文件,
 * 该文件就代表rollback segment文件。
 *
 * undo log相关参数-般很少改动。
 *
 * show variables like '参数';
 *
 * undo页的重用
 * 当我们开启一个事务 需要写undo log的时候，就得先去undo log segment中去找到一个空闲的位置，
 * 当有空位的时候，就去申请undo页，在这个申请到的undo页中进行undo log的写入。
 * 我们知道mysql默认-页的大小是16k.为每一个事务分配一 个页, 是非常浪费的(除非你的事务非常长) ,
 * 假设你的应用的TPS (每秒处理的事务数目)为1000，
 * 那么1s就需要1000个页，大概需要16M的存储，1分钟大概需要1G的存储。
 * 如果照这样下去除非MySQL清理的非常勤快，否则随着时间的推移,磁盘空间会增长的非常快，
 * 而且很多空间都是浪费的。于是undo页就被设计的可以重用了，当事务提交时，并不会立刻删除undo页。
 * 因为重用,所以这个undo页可能混杂着其他事务的undo log。
 * undo log在commit后，会被放到一个链表中,然后判断undo页的使用空间是否小于3/4 ,如果小于3/4的话，
 * 则表示当前的undo页可以被重用，那么它就不会被回收,
 * 其他事务的undo log可以记录在当前undo页的后面。
 * 由于undo log是离散的，所以清理对应的磁盘空间时，效率不高。
 *
 */

/**
 * 2.回滚段与事务
 * 1.每个事务只会使用一个回滚段，一个回滚段在同一时刻可能会服务于多个事务。
 * 2.当一个事务开始的时候，会制定一个回滚段， 在事务进行的过程中，
 *   当数据被修改时，原始的数据会被复制到回滚段。
 * 3.在回滚段中，事务会不断填充盘区，直到事务结束或所有的空间被用完。
 *   如果当前的盘区不够用,事务会在段中请求扩展下一个盘区,
 *   如果所有已分配的盘区都被用完，
 *   事务会覆盖最初的盘区或者在回滚段允许的情 况下扩展新的盘区来使用。
 * 4.回滚段存在于undo表空间中，在数据库中可以存在多个undo表空间,
 *   但同一时刻只能使用一个undo表空间。
 * 5.当事务提交时，InnoDB存储引擎会做以下两件事情:
 *   ● 将undo log放入列表中,以供之后的purge操作
 *   ● 判断undo log所在的页是否可以重用，若可以分配给下个事务使用
 *
 */
/**
 * 3.回滚段中的数据分类
 * 1.未提交的回滚数据 (uncommitted undo information) :
 *  该数据所关联的事务并未提交,用于实现读一致性，所以该数据不能被其他事务的数据覆盖。
 * 2.已经提 交但未过期的回滚数据( committed undo information) :
 *   该数据关联的事务已经提交,但是仍受到 undo retention 参数的保持时间的影响。
 * 3. 事务已经提交并过期的数据(expired undo information) :
 *    事务已经提交,而且数据保存时间已经超过  undo retention参数指定的时间，
 *    属于已经过期的数据。
 *    当回滚段满了之后，会优先覆盖"事务已经提交并过期的数据"。
 *
 * 事务提交后并不能马.上删除undo log及undo log所在的页。
 * 这是因为可能还有其他事务需要通过undo log来得到行记录之前的版本。
 * 故事务提交时将undo log放入一个链表中，
 * 是否可以最终删除undo log及undo log所在页由 purge线程来判断。
 *
 */
