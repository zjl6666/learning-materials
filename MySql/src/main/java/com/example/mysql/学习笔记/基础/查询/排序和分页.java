package com.example.mysql.学习笔记.基础.查询;

/**
 * 数据库 mysql  在windows下全部不分大小写
 * mysql 在windows下关键字，表名 列明不区分大小写 表中的具体数据也不区分大小写
 * oracle 在windows下关键字，表名 列明不区分大小写  但表中的具体数据区分大小写
 */
public class 排序和分页 {
    public static void main(String[] args) {
        String sql="select * from 表名";
        String sql1="select " +
                "字段1 字段1的别名，" +
                "字段2 as 字段2的别名，" +
                "字段2 as ”字段2的别名（加双引号可以中间加空格）“ " +
                "from 表名";
        //尽量不要用单引号


        /**
         * 排序和分页
         * //默认是按照   当初添加的顺序 出来的
         *
         * select  字段1,字段2 from 表名 ORDER BY 字段 ASC  //默认升序 不写ASC是升序
         * select  字段1,字段2 from 表名 ORDER BY 字段 DESC //降序
         *
         * 使用别名   只能在 ORDER BY 中使用  不能在where中使用
         * ORDER BY 必须在   where后面
         *
         * 数据库执行顺序 是
         * select  字段1,字段2
         * from 表名 where  一系列操作
         * ORDER BY 字段1 ASC,字段2 DESC //按照字段1升序 二级排序字段2降序
         *
         * 数据库语句 执行顺序
         * from 表名 where  一系列操作   //查出所有字段
         * select  字段1 别名,字段2    //选择需要的字段
         * ORDER BY 字段 ASC          // 进行排序
         *
         * 所以别名不能在where中使用
         *
         * 分页
         * select  字段1,字段2 from 表名 LIMIT 0,20
         * //0偏移量(0偏移量可以省略)    20显示条数
         *
         * 声明顺序
         * select  字段1,字段2
         * from 表名 where  一系列操作
         * ORDER BY 字段1 ASC,字段2 DESC
         * LIMIT 0,20
         */

        /**
         * 8.0新特性
         * LIMIT 数字1 OFFSET 数字2  相当于 LIMIT 数字2,数字1
         *
         */

        /**
         * oracle:  每个数据库默认有个字段 rownum
         * select  rownum ,字段1,字段2 from 表名
         * where rownum<10
         *
         */

    }
}