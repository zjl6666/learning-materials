package com.example.mysql.学习笔记.高级.事务.MySQL事务日志.redo日志;

public class redo_log_buffer写入过程_7 {
}
/**
 * 1.补充概念: Mini-Transaction
 * MySQL把对底层页面中的一次原子访问的过程称之为一个Mini-Transaction,
 * 简称mtr，
 * 比如，向某个索引对应的B+树中插入一条记录的过程就是一个Mini-Transaction。
 * 一个所谓的mtr可以包含一组redo日志，
 * 在进行崩溃恢复时这一组redo日志作为一个不可分割的整体。
 * 一个事务可以包含若干条语句，每一条语句其实是由若干个mtr组成，
 * 每一个mtr又可以包含若干条redo日志，
 * 画个图表示它们的关系就是这样:
 *
 * 一个事务 == 多个语句
 * 一个语句 == 多个mtr
 * 一个mtr == 多个redo日志(因为修改可能修个多处信息)
 *
 */
/**
 * 2. redo日志写入log buffer
 *
 * redo log buffer(最大值是4096M，最小值为1M)
 * = n个redo log block(512字节)
 *
 * 向log buffer 中写入redo日志的过程是顺序的，也就是先往前边的block中写,
 * 当该 block 的空闲空间用完之后再往下一个block中写。
 * 当我们想往log buffer 中写入redo日志时,
 * 第一个遇到的问题就是应该写在哪个 block 的哪个偏移量处，
 * 所以InnoDB的设计者特意提供了一个称之为buf_free 的全局变量，
 * 该变量指明后续写入的redo日志应该写入到log buffer 中的哪个位置，如图所示:
 *
 *
 * redo log block == log block header//头
 *                   log block block//体
 *                   log block trailer//尾
 *
 */

/**
 * 3. redo log block的结构图
 * 一个redo log block是由日志头、日志体、日志尾 组成。
 * 日志头占用12字节，
 * 日志尾占用8字节,
 * 所以一个block真正能存储的数据就是512-12-8=492字节。
 *
 *
 * 为什么一个block设计成 512 字节?
 * 这个和磁盘的扇区有关，机械磁盘默认的扇区就是512字节，
 * 如果你要写入的数据大于512字节，那么要写入的扇区肯定不止一个，
 * 这时就要涉及到盘片的转动，找到下一个扇区，
 * 假设现在需要写入两个扇区A和B，如果扇区A写入成功，而扇区B写入失败，
 * 那么就会出现非原子性的写入，
 * 而如果每次只写入和扇区的大小一样的512字节，那么每次的写入都是原子性的。
 *
 *
 */
