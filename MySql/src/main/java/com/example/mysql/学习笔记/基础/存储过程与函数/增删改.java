package com.example.mysql.学习笔记.基础.存储过程与函数;

public class 增删改 {
}
/**
 * 查看
 * 存储过程和存储函数 的信息
 *      SHOW CREATE PROCEDURE 存储过程名字
 *      SHOW CREATE FUNCTION 存储函数名字
 *
 * SHOW  PROCEDURE/FUNCTION STATUS;//查看所有存储的状态信息
 * SHOW  PROCEDURE/FUNCTION STATUS LIKE '名字';//查看所有存储的状态信息
 * //LIKE 模糊查询
 *
 * //所有存储函数啥的信息在information_schema.Routines表里   直接查看表信息
 * SELECT * FROM information_schema.Routines
 * WHERE ROUTINE_NAME='存储过程或函数的名'
 * [AND ROUTINE_TYPE = {'PROCEDURE|FUNCTION'}];//在这里区分大小写
 *
 *
 */

/**
 * 修改
 * 修改存储过程或函数，不影响存储过程或函数功能，只是修改相关特性。使用ALTER语句实现。
 *
 * ALTER {PROCEDURE | FUNCTION} 存储过程或函数的名 [characteristic ...]
 *
 * 其中，characteristic指定存储过程或函数的特性，
 * 其取值信息与创建存储过程、函数时的取值信息略有不同。
 *
 * 只能改特性    不能改内容
 *
 *
 * - `CONTAINS SQL`，表示子程序包含SQL语句，但不包含读或写数据的语句。
 * - `NO SQL`，表示子程序中不包含SQL语句。
 * - `READS SQL DATA`，表示子程序中包含读数据的语句。
 * - `MODIFIES SQL DATA`，表示子程序中包含写数据的语句。
 * - `SQL SECURITY { DEFINER | INVOKER }`，指明谁有权限来执行。
 * - `DEFINER`，表示只有定义者自己才能够执行。
 * - `INVOKER`，表示调用者可以执行。
 * - `COMMENT 'string'`，表示注释信息。
 */

/**
 * 删除
 * DROP {PROCEDURE | FUNCTION} [IF EXISTS] 存储过程或函数的名
 */