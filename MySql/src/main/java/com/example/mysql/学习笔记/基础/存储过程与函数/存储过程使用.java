package com.example.mysql.学习笔记.基础.存储过程与函数;

public class 存储过程使用 {
}
/**
 * 创建：
 * DELIMITER $            //设置$为结束符号  $可变
 * CREATE PROCEDURE 存储过程名(IN|OUT|INOUT 参数名  参数类型,...)
 * [characteristics ...]   //特性  可有可无
 * BEGIN
 *      sql语句1;
 *      sql语句2;
 * END $
 * DELIMITER ;         //修改回来
 *
 * 调用  执行调用函数
 *  CALL 存储过程名();
 */

/**  OUT  的使用：
 *    创建
 *      DELIMITER $            //设置$为结束符号  $可变
 *      CREATE PROCEDURE 存储过程名(OUT 返回值名 返回值类型)
 *      BEGIN
 *           SELECT MIN(字段) INTO  返回值名1,返回值名2...
 *           FROM 表名;
 *      END $
 *      DELIMITER ;         //修改回来
 *    调用
 *      CALL 存储过程名(@返回值名);
 *    查看变量名
 *      SELECT @返回值名;
 *
 */

/**  IN  的使用：
 *    创建
 *      DELIMITER $            //设置$为结束符号  $可变
 *      CREATE PROCEDURE 存储过程名(IN 传入值名 传入值类型)
 *      BEGIN
 *           SELECT MIN(字段)
 *           FROM 表名;
 *           WHERE 一系列操作  可能用到传入值名
 *      END $
 *      DELIMITER ;         //修改回来
 *    调用 1:
 *      CALL 存储过程名(传入的内容);
 *    调用 2:
 *      SET @自己起的名字 = 自己命名的值;   :=  真正意义上的赋值符号  都行
 *      CALL 存储过程名(@自己起的名字);//相当与传了一个变量
 *
 */

/**  IN 和 OUT  的使用：
 *    创建
 *      DELIMITER $            //设置$为结束符号  $可变
 *      CREATE PROCEDURE 存储过程名(IN 传入值名 传入值类型,OUT 返回值名 返回值类型)
 *      BEGIN
 *           SELECT MIN(字段) INTO  返回值名
 *           FROM 表名;
 *           WHERE 一系列操作  可能用到传入值名
 *      END $
 *      DELIMITER ;         //修改回来
 *    调用 :
 *      SET @传入的名字 = 自己命名的值;   :=  真正意义上的赋值符号  都行
 *      CALL 存储过程名(@传入的名字,@返回值名);//相当与传了二个变量
 *    查看变量名
 *      SELECT @返回值名;
 */

/**  INOUT  的使用： 一般用到了子查询 传入类型和传出类型一样
 *    创建
 *      DELIMITER $            //设置$为结束符号  $可变
 *      CREATE PROCEDURE 存储过程名(INOUT 值名 值类型)
 *      BEGIN
 *           SELECT MIN(字段) INTO  返回值名
 *           FROM 表名;
 *           WHERE 一系列操作  可能用到传入值名
 *      END $
 *      DELIMITER ;         //修改回来
 *    调用 :
 *      SET @值名 = 自己命名的值;   :=  真正意义上的赋值符号  都行
 *      CALL 存储过程名(@值名);//相当与传了二个变量
 *    查看变量名
 *      SELECT @值名;
 */

/**
 * 缺点： 调试困难
 * 在 MySQL 中，
 * 存储过程不像普通的编程语言（比如 VC++、Java 等）那样有专门的集成开发环境。
 * 因此，你可以通过 SELECT 语句，把程序执行的中间结果查询出来，来调试一个 SQL 语句的正确性。
 * 调试成功之后，把 SELECT 语句后移到下一个 SQL 语句之后，再调试下一个 SQL 语句。
 * 这样`逐步推进`，就可以完成对存储过程中所有操作的调试了。
 * 当然，你也可以把存储过程中的 SQL 语句复制出来，逐段单独调试。
 */



