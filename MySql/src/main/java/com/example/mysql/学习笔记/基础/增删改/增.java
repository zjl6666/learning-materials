package com.example.mysql.学习笔记.基础.增删改;

public class 增 {
}
/**
 * 方式一：  不指明添加的字段
 *      INSERT INTO 表名
 *      VALUES (按顺序对应的信息);
 *
 *      有局限性
 * 方式二：  指明添加的字段
 *      INSERT INTO 表名(每个字段名字)
 *      VALUES  (按自己给的顺序对应的信息1),
 *              (按自己给的顺序对应的信息2),
 *              (按自己给的顺序对应的信息3);
 *
 * 方式三：
 * //字段的类型尽量一直并对应 可以 小->大   如果大-》小 除非小的能存下也行
 *  INSERT INTO 表名(每个字段名字)
 *       SELECT 每个字段名字
 *       FROM 其他表名
 *       WHERE 条件
 *
 *
 *  VALUE 也可以   但是规范 VALUES
 */