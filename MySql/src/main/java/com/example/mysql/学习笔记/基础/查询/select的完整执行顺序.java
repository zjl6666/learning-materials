package com.example.mysql.学习笔记.基础.查询;

import java.math.BigDecimal;

public class select的完整执行顺序 {
    public static void main(String[] args) {
        BigDecimal b = new BigDecimal("5445");
        System.out.println(b.toString());

        System.out.println(" ".equals(null));
    }
}

/**
 * select 完整顺序
 *
 * #sq192语法:
 * SELECT ....,....,... (存在聚合函数)
 * FROM ....,...
 * WHERE 多表的连接条件AND不包含聚合函数的过滤条件
 * HAVING 包含聚合函数的过滤条件
 * ORDER BY .....,... (ASC / DESC )排序
 * LIMIT ...,...
 *
 * #sq199语法:
 * SELECT ............ (存在聚合函数)    ②
 * FROM ... (LEFT / RIGHT)JOIN ....0N 多表的连接条件  |
 * (LEFT / RIGHT)JOIN ... ON .                     |
 * WHERE不包含聚合函数的过滤条件                        |      ①
 * GROUP BY......                                  |
 * HAVING包含聚合函数的过滤条件                         |
 * ORDER BY ........ (ASC / DESC ) 排序       |
 * LIMIT......                               | ③
 *
 *#4.2 SQL语句的执行过程: .
 * FROM ...,....-> ON -> (LEFT/RIGNT JOIN) -> WHERE -> GROUP BY ->
 * HAVING -> SELECT -> DISTINCT ->
 *  ORDER BY -> LIMIT
 *
 *  WHERE速度快   是因为他执行顺序靠前
 */