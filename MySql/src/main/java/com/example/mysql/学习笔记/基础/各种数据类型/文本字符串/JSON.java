package com.example.mysql.学习笔记.基础.各种数据类型.文本字符串;

public class JSON {
}

/**
 * JSON 类型
 *
 * JSON（JavaScript Object Notation）是一种轻量级的`数据交换格式`。
 *      简洁和清晰的层次结构使得 JSON 成为理想的数据交换语言。
 *      它易于人阅读和编写，同时也易于机器解析和生成，并有效地提升网络传输效率。
 *      **JSON 可以将 JavaScript 对象中表示的一组数据转换为字符串，
 *      然后就可以在网络或者程序之间轻松地传递这个字符串，
 *      并在需要的时候将它还原为各编程语言所支持的数据格式。**
 * 在MySQL 5.7中，就已经支持JSON数据类型。
 * 在MySQL 8.x版本中，JSON类型提供了可以进行自动验证的JSON文档和优化的存储结构，
 * 使得在MySQL中存储和读取JSON类型的数据更加方便和高效。
 * CREATE TABLE 表名(
 * js JSON
 * );
 * INSERT INTO 表名 (js)
 * VALUES ('{"name":"songhk", "age":18, "address":{"province":"beijing", "city":"beijing"}}');
 *
 * SELECT  js -> '$.name' AS NAME,
 *          js -> '$.age' AS age ,
 *          js -> '$.address.province' AS province,
 *          js -> '$.address.city' AS city
 *  FROM test_json;
 *
 *  AS 重命名
 */