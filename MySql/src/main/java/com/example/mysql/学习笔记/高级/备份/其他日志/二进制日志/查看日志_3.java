package com.example.mysql.学习笔记.高级.备份.其他日志.二进制日志;

public class 查看日志_3 {
}
/**
 * 5.3查看日志
 * 当MySQL创建二进制日志文件时，先创建一个以  filename 为名称、以".index' ”为后缀的文件,
 * 再创建一个以filename"为名称、以“00001”为后缀的文件。
 * MySQL服务重新启动一次，以0000为后缀的文件就会增加一个，并且后缀名按1递增。
 * 即日志文件的个数与MySQL服务启动的次数相同;
 * 如果日志长度超过了max_binlog_size 的上限(默认是1GB)，就会创建一个新的日志文件。
 *
 *
 * 除此之外，binlog还有2种格式，分别是Statement和Mixed
 * ●Statement
 * 每一条会修改数据的sql都会记录在binlog中。
 * 优点:不需要记录每一行的变化， 减少了binlog日志量，节约了I/0,提高性能。
 * ●Row
 * 5.1.5版本的MySQL才开始支持row level的复制，它不记录sql语句.
 * 上下文相关信息，仅保存哪条记录被修改。
 * 优点: row level的日志内容会非常清楚的记录下每一行数据修改的细节。
 * 而且不会出现某些特定情况下的存储过程，或function,
 * 以及trigger的调用和触发无法被正确复制的问题。
 * ●** Mixed
 * 从5.1.8版本开始，MySQL提供了Mixed格式，实际上就是Statement与Row的结合。
 */