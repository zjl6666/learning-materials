package com.example.mysql.学习笔记.高级.入门;

/**
 * 像InnoDB、MyISAM 这样的存储引擎都是把表存储在磁盘上的，
 * 操作系统用来管理磁盘的结构被称为文件系统，
 * 以用专业一点的话来表述就是:像InnoDB、MyISAM 这样的存储引擎都是把表存储在文件系统上的。
 * 当我们想读取数据的时候，这些存储引擎会从文件系统中把数据读出来返回给我们，
 * 当我们想写入数据的时候，这些存储引擎会把这些数据又写回文件系统。
 * 本章学习一下InnoDB和MyISAM这两个存储引擎的数据如何在文件系统中存储。
 *
 * 2.1查看默认数据库
 * 查看一下在我的计算机上当前有哪些数据库:
 * mysql> SHOW DATABASES;
 * 可以看到有4个数据库是属于MySQL自带的系统数据库。
 *
 * mysql
 * MySQL系统自带的核心数据库,它存储了MySQL的用户账户和权限信息，-些存储过程、事件的定义信息，
 * 一些运行过程中产生的日志信息，-些帮助信息以及时区信息等。
 *
 * information_schema
 * MySQL系统自带的数据库,这个数据库保存着MySQL服务器维护的所有其他数据库的信息，
 * 比如有哪些表、哪些视图、哪些触发器、哪些列、哪些索引。
 * 这些信息并不是真实的用户数据，而是一些描述性信息， 有时候也称之为元数据。
 * 在系统数据库information_schema中提供了一一些以innodb_sys开头的表，
 * 用于表示内部系统表。
 * mysql> USE information_ schema ;
 * Database changed
 *
 * performance_schema
 * MySQL系统自带的数据库,这个数据库里主要保存MySQL服务器运行过程中的一些状态信息，
 * 可以用来监控MySQL服务的各类性能指标。
 * 包括统计最近执行了哪些语句，在执行过程的每个阶段都花费了多长时间，内存的使用情况等信息。
 *
 * sys
 * MySQL系统自带的数据库，这个数据库主要是通过视图的形式把
 * information_schema 和performance_schema结合起来,
 * 帮助系统管理员和开发人员监控MySQL的技术性能。
 *
 */
public class 起始数据库 {
}