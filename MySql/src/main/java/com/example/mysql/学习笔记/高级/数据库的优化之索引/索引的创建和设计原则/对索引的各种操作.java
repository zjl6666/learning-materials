package com.example.mysql.学习笔记.高级.数据库的优化之索引.索引的创建和设计原则;

public class 对索引的各种操作 {
}

/**
 * 创建索引
 * #隐式的方式创建索引。
 *   在声明有主键约束、唯一性约束、外键约束的字段上，会自动的添加相关的索引
 * 在CREATE TABLE （创建表） 时创建索引
 *     CREATE TABLE dept (
 *     dept_id INT PRIMARY KEY AUTO_INCREMENT,（主键自动递增）
 *     dept_name VARCHAR (20 )
 *     );
 *     会自动创建主键索引（聚簇索引）
 *
 *     CREATE TABLE emp (
 *     emp_id INT PRIMARY KEY AUTO_INCREMENT ,（自动创建索引 主键约束）
 *     emp_name VARCHAR (20) UNIQUE,（自动创建索引 唯一性约束）
 *     dept_ id INT,
 *     CONSTRAINT emp_dept_id_fk FOREIGN KEY (dept_id) REFERENCES dept (dept_id)
 *          （自动创建索引 外键约束）
 *     );
 *
 * 显式的方式创建索引。
 *      CREATE TABLE dept (
 *      dept_id INT PRIMARY KEY AUTO_INCREMENT,
 *      dept_name VARCHAR (20 )
 *       INDEX 索引名字（dept_name（字段名））
 *         //创建dept_name的索引   不写索引名字  默认时字段名
 *      );
 *
 *
 *  声明有唯一性索引的字段 ， 此字段自动添加唯一性约束...
 *
 * #通过命令查看索引
 * #方式1:
 * SHOW CREATE TABLE 表名;
 * #方式2:
 * SHOW INDEX FROM 表名;
 *
 *
 * 性能分析工具 EXPLAIN
 *  在查询语句前加   会分析  你这个查询语句将要用的索引等东西
 *
 * #通过删除主键约束的方式删除主键索引  不能有递增属性
 * ALTER TABLE 表名；
 * DROP PRIMARY KEY;
 *
 * 联合索引   查询必须有索引（联合索引）的第一个字段时 会用到索引
 * CREATE TABLE dept (
 * dept_id INT PRIMARY KEY AUTO_INCREMENT,
 * dept_name VARCHAR (20 )
 *  INDEX 索引名字（字段名1，字段名2...）//联合索引
 * );
 *
 * 创建全文索引
 * FULLTEXT全文索引可以用于全文搜索,并且只为CHAR、VARCHAR 和TEXT列创建索引。
 * 索引总是对整个列进行，不支持局部(前缀)索引。
 * CREATE TABLE test4 (
 * id INT NOT NULL,
 * NAME CHAR(30) NOT NULL,
 * age INT NOT NULL,
 * info VARCHAR (255) ,
 * FULLTEXT INDEX futxt_idx_info(info(50)) //全文索引  50的意思是 以前50个字符创建索引
 * )
 * 不同于like方式的的查询:
 * SELECT * FROM 表名 WHERE content LIKE ‘%查询字符串%’ ;
 * 全文索引用match+against方式查询:   InnoDB  不支持
 * SELECT * FROM 表名 WHERE MATCH(title , content) AGAINST ( '查询字符串'):
 * 明显的提高查询效率。
 * 注意点
 * 1.使用全文索引前，搞清楚版本支持情况;
 * 2.全文索引比like+%快 N 倍，但是可能存在精度问题;
 * 3.如果需要全文索引的是大量数据，建议先添加数据，再创建索引。
 *
 *
 */

/**
 * 在已经创建的表上添加索引
 * 方式一 ALTER（改变） TABLE 表名 ADD　INDEX/UNIQUE(唯一) 自定义索引名（字段名）
 * 方式二 CREATE INDEX  UNIQUE(唯一性索引需添加) 索引名字 ON 表名(字段名)
 *
 */

/**
 * 索引的删除  有AUTO_INCREMENT（自动增长）的约束的唯一索引不能删除
 * 方式一 ALTER（改变） TABLE 表名 DROP　INDEX 索要删除的索引名
 * 方式二 DROP INDEX 要删除的索引名 ON 表名
 *
 *
 * 删除字段  会自动删除  此字段相关的索引
 * ALTER TABLE 表名 DROP COLUMN 某个字段名
 * 删除表中的列时，如果要删除的列为索引的组成部分，则该列也会从索引中删除。
 * 如果组成索引的所有列都被删除，则整个索引将被删除。
 *
 */
