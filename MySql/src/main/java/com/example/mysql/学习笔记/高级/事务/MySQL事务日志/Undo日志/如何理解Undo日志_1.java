package com.example.mysql.学习笔记.高级.事务.MySQL事务日志.Undo日志;

public class 如何理解Undo日志_1 {
}
/**
 * 2.1如何理解Undo日志
 * 事务需要保证原子性，也就是事务中的操作要么全部完成，要么什么也不做。
 * 但有时候事务执行到一半会出现一些情况，比如:
 * ●情况一:事务执行过程中可能遇到各种错误，比如服务器本身的错误，
 *        操作系统错误，甚至是突然断电导致的错误。
 * ●情况二:程序员可以在事务执行过程中手动输入 ROLLBACK (回滚)语句结束当前事务的执行。
 *       以上情况出现，我们需要把数据改回原先的样子，这个过程称之为回滚，
 *       这样就可以造成一个假象: 这个事务看起来什么都没做，所以符合原子性要求。
 *
 * 每当我们要对一条记录做改动时(这里的改动可以指INSERT、DELETE、 UPDATE) ，
 * 都需要"留一手”  把回滚时所需的东西记下来。比如:
 * ● 你插入一条记录时，至少要把这条记录的主键值记下来,
 *   之后回滚的时候只需要把这个主键值对应的记录删掉就好了。
 *   (对于每 个INSERT, InnoDB存储引擎会完成一个DELETE),你
 * ● 删除了一条记录，至少要把这条记录中的内容都记下来,
 *   这样之后回滚时再把由这些内容组成的记录插入到表中就好了。
 *   (对于每个DELETE， InnoDB存储引擎会执行一 个INSERT)
 *
 * ● 你修改了一条记录，至少要把修改这条记录前的旧值都记录下来，
 *   这样之后回滚时再把这条记录更新为旧值就好了。
 *   (对于每个UPDATE, InnoDB存储弓 |擎会执行一个相反的UPDATE， 将修改前的行放回去)
 *
 *   MySQL把这些为了回滚而记录的这些内容称之为撤销日志或者回滚日志(即undo log)。
 *   注意，由于查询操作( SELECT )并不会修改任何用户记录，所以在查询操作执行时，
 *   并不需要记录相应的undo日志。
 *
 *   此外，undo log会产生redo log ，
 *   也就是undo log的产生会伴随着redo log的产生，
 *   这是因为undo log也需要持久性的保护。
 *
 *
 *
 */