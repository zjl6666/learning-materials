package com.example.mysql.学习笔记.高级.事务.多版本并发控制;

public class 总结 {
}
/**
 * 通过MVCC我们可以解决:
 * 1.读写之 间阻塞的问题。通过MVCC可以让读写互相不阻塞,
 * 即读不阻塞写，写不阻塞读，这样就可以提升事并发处理能力。
 * 2. 降低了死锁的概率。 这是因为MVCC采用了乐观锁的方式，
 * 读取数据时并不需要加锁,对于写操作,也只锁定必要的行。
 * 3.‘解决快照读的问题。当我们查询数据库在某个时间点的快照时，
 * 只能看到这个时间点之前事务提交更新的结果，而不能看到这个时间点之后事务提交的更新结果。
 */

