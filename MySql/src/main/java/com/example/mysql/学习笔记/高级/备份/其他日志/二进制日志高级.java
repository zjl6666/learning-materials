package com.example.mysql.学习笔记.高级.备份.其他日志;

public class 二进制日志高级 {
}
/**
 * 6.1写入机制
 * binlog的写入时机也非常简单，事务执行过程中，先把日志写到binlog cache ，
 * 事务提交的时候，再把binlog cache写到binlog文件中。
 * 因为一个事务的binlog不能被拆开，无论这个事务多大，也要确保一次性写入，
 * 所以系统会给每个线程分配一个块内存作为binlog cache。
 * 我们可以通过binlog_cache_size 参数控制单个线程binlog cache大小，
 * 如果存储内容超过了这个参数，就要暂存到磁盘(Swap)。
 *
 * write(写入缓存)和fsync（刷盘）的时机
 * 可以由参数sync_binlog 控制，默认是0。
 *
 * 为0的时候，表示每次提交事务都只write,由系统自行判断什么时候执行fsync。
 * 虽然性能得到提升，但是机器宕机，page cache里面的bing log会丢失
 *
 * 为了安全起见，可以设置为1，
 * 表示每次提交事务都会执行fsync,就如同redo log刷盘流程一样。
 *
 * 最后还有一种折中方式，可以设置为N(N>1),
 * 表示每次提交事务都write,但累积N个事务后才fsync。
 *
 */
/**
 *6.2 binlog与redo log对比
 * ●redo log它是物理日志，记录内容是“在某个数据页上做了什么修改”，属于InnoDB存储引擎层产生的。
 * ●而binlog是逻辑日志，记录内容是语句的原始逻辑，
 * 类似于“给ID=2这一 行的c字段加1”，属于MySQL Server层。
 *
 *  ●虽然它们都属于持久化的保证，但是则重点不同。
 * 。redo log让InnoDB存储引擎拥有了崩溃恢复能力。
 * 。bin log 保证了MySQL集群架构的数据致性。
 *
 */
/**
 * 6.3两阶段提交
 * 在执行更新语句过程，会记录redo log与binlog两块日志,以基本的事务为单位，
 * redo log在事务执行过程中可以不断写入，而binlog只有在提交事务时才写入，
 * ****所以redo log与binlog的写入时机不一样。****
 *
 * redo log与binlog两份日志之间的逻辑不-致,会出现什么问题?
 * 以update语句为例，假设id=2的记录，字段c值是0,把字段c值更新成1,
 * SQL语句为update T set c=1 where id=2。
 * 假设执行过程中写完redo log日志后，binlog日志写期间发生了异常，会出现什么情况呢?
 *
 * 由于redo log是恢复信息的日志，不容易丢失
 * 而binlog日志一般用于主从复制的同步日志，只有在事务提交的时候才写入，
 * 可能出现binlog日志写一半的时候  失败
 * 导致主从回复的时候  信息不一致，
 * 所以在  binlog日志的前后 都执行一次redo log的阶段
 *
 * 写入redo log (prepare阶段)
 * 提交事务 写入 binlog日志
 * redo log设置 commit阶段
 *
 *
 */