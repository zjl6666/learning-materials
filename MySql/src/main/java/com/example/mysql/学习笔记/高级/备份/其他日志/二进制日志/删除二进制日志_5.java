package com.example.mysql.学习笔记.高级.备份.其他日志.二进制日志;

public class 删除二进制日志_5 {
}
/**
 *
 * MySQL的二进制文件可以配置自动删除，同时MySQL也提供了安全的手动删除二进制文件的方法。PURGE
 * MASTER LOGS 只删除指定部分的二进制日志文件，RESET MASTER 删除所有的二进制日志文件。具体如下:
 *
 * 1. PURGE MASTER LOGS:删除指定日志文件
 * SHOW BINARY LOGS;//查看日志文件
 * PURGE MASTER LOGS语法如下:
 * PURGE {MASTER| BINARY} LOGS TO '指定日志文件名'
 * PURGE {MASTER| BINARY} LOGS BEFORE '指定日期'
 *
 * 2. RESET MASTER:删除所有二进制日志文件
 * 使用RESET MASTER 语句，清空所有的binlog日志。MySQL会重新创建二进制文件，
 * 新的日志文件扩展名将重新从00001开始编号。慎用!
 * 举例:使用RESET MASTER语句删除所有日志文件。
 * 执行RESET MASTER语句，删除所有日志文件
 * RESET MASTER;
 * 执行完该语句后，原来的所有二进制日志已经全部被删除。
 *
 *
 *
 */