package com.example.mysql.学习笔记.高级.入门.存储引擎;

/**
 *  Memory引擎:置于内存的表
 * 概述:
 * Memory采用的逻辑介质是内存，响应速度 很快，但是当mysqld守护进程崩溃的时候数据会丢失。
 * 另外，要求存储的数据是数据长度不变的格式，比如，Blob和Text类型的数据不可用(长度不固定的)。
 * 主要特征:
 *   ●Memory同时支持哈希(HASH) 索引和B+树索引。
 *     。哈希索引相等的比较快，但是对于范围的比较慢很多。
 *     。默认使用哈希(HASH) 索引，其速度要比使用B型树(BTREE) 索引快。
 *     。如果希望使用B树索引，可以在创建索引时选择使用。
 *   ●Memory表至少比MyISAM表要快一个数量级。
 *   ●MEMORY 表的大小是受到限制的。表的大小主要取决于两个参数，
 *      分别是max_rows 和max_heap_table_size
 *      其中，max_rows可以在创建表时指定;
 *      max_heap _table_size的大小默认为16MB,可以按需要进行扩大。
 * ●数据文件与索引文件分开存储。
 * 。每个基于MEMORY存储引擎的表实际对应一个磁盘文件，该文件的文件名与表名相同，类型为frm类型，
 * 该文件中只存储表的结构，而其数据文件都是存储在内存中的。
 * 。这样有利于数据的快速处理， 提供整个表的处理效率。
 * ●缺点:其数据易丢失,生命周期短。基于这个缺陷，选择MEMORY存储引擎时需要特别小心。
 *
 * 使用Memory存储引擎的场景:
 * 1.目标数据比较小，而且非常频繁的进行访问，在内存中存放数据，如果太大的数据会造成内存溢出。可以通
 * 过参数max_heap_table_ size 控制Memory表的大小,限制Memory表的最大的大小。
 * 2.如果数据是临时的，而且必须立即可用得到，那么就可以放在内存中。
 * 3.存储在Memory表中的数据如果突然间丢失的话也没有太大的关系。
 *
 *
 */
public class Memory引擎 {
}
