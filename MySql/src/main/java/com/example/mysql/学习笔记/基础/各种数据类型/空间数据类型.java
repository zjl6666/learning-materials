package com.example.mysql.学习笔记.基础.各种数据类型;

public class 空间数据类型 {
}
/**
 * 空间类型  用的极少
 * MySQL 空间类型扩展支持地理特征的生成、存储和分析。
 *          这里的地理特征表示世界上具有位置的任何东西，可以是一个实体，
 *          例如一座山；可以是空间，例如一座办公楼；也可以是一个可定义的位置，
 *          例如一个十字路口等等。MySQL中使用`Geometry（几何）`来表示所有地理特征。
 *          Geometry指一个点或点的集合，代表世界上任何具有位置的事物。
 *
 * MySQL的空间数据类型（Spatial Data Type）对应于OpenGIS类，
 *      包括单值类型：GEOMETRY、POINT、LINESTRING、POLYGON以及
 *      集合类型：MULTIPOINT、MULTILINESTRING、MULTIPOLYGON、GEOMETRYCOLLECTION 。
 *
 * - Geometry是所有空间集合类型的基类，其他类型如POINT、LINESTRING、POLYGON都是Geometry的子类。
 *   - Point，顾名思义就是点，有一个坐标值。例如POINT(121.213342 31.234532)，POINT(30 10)，
 *      坐标值支持DECIMAL类型，经度（longitude）在前，维度（latitude）在后，用空格分隔。
 *   - LineString，线，由一系列点连接而成。如果线从头至尾没有交叉，那就是简单的（simple）；
 *      如果起点和终点重叠，那就是封闭的（closed）。例如LINESTRING(30 10,10 30,40 40)，
 *      点与点之间用逗号分隔，一个点中的经纬度用空格分隔，与POINT格式一致。
 *   - Polygon，多边形。可以是一个实心平面形，即没有内部边界，也可以有空洞，类似纽扣。
 *      最简单的就是只有一个外边界的情况，例如POLYGON((0 0,10 0,10 10, 0 10))。
 */