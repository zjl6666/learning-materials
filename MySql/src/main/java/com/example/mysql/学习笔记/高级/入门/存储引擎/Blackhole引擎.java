package com.example.mysql.学习笔记.高级.入门.存储引擎;

/**
 * 4.4 Blackhole引擎:丢弃写操作，读操作会返回空内容
 * ●Blackhole引擎没有实现任何存储机制，它会丢弃所有插入的数据，不做任何保存。
 * ●但服务器会记录Blackhole表的日志，所以可以用于复制数据到备库，或者简单地记录到日志。但这种应用方
 * 式会碰到很多问题，因此并不推荐。
 *
 */
public class Blackhole引擎 {
}
