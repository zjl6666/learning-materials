package com.example.mysql.学习笔记.基础.建库建表;

public class 建库 {
    /**
     *
     *
     * SHOW DATABASES//查看有哪些数据库
     *
     * CREATE DATABASE 数据库名;    创建数据库
     * CREATE DATABASE 数据库名 CHARACTER SET '字符集(如utf8,gbk等)';
     *          创建指定字符集数据库
     * CREATE DATABASE IF NOT EXISTS CHARACTER SET '字符集(如utf8,gbk等)';
     *          如果存在就不创建，不存在就创建
     * SHOW CREATE DATABASE 数据库名;//查看此数据库的信息
     *
     * USE 数据库名  //切换数据库
     * SELECT DATABASE() FROM DUAL//查看当前使用的数据库
     * SHOW TABLES FROM 数据库名//查看数据库的所有表  不加FROM 数据库名 是查看当前数据库所有表
     *
     * DATABASE:数据库
     * 更改数据库： 一般不要更改  除非是刚建的  信息不多  成本低
     *      注意: DATABASE不能改名。一些可视化工具可以改名，
     *      它是建新库，把所有表复制到新库，再删旧库完成的。
     * ALTER DATABASE 数据库名 CHARACTER SET '字符集'//更改数据库字符集
     *
     * 删除数据库：
     * DROP DATABASE IF EXISTS 数据库名//存在此数据库 删除  IF EXISTS 可以在没数据库时删除不报错
     *
     */
}
