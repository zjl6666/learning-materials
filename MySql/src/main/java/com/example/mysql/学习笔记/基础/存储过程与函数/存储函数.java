package com.example.mysql.学习笔记.基础.存储过程与函数;

public class 存储函数 {
}
/**
 * 存储函数
 *
 *      DELIMITER $            //设置$为结束符号  $可变
 *      CREATE FUNCTION 函数名(参数名 参数类型,...)
 *      RETURNS 返回值类型
 *          //特性约束  可有可无
 *          DETERMINISTIC       //确定的
 *          CONTAINS SQL       //包含SQL语句
 *          READS SQL DATA      //当前存储过程的子程序中包含读数据的SQL语句
 *      BEGIN
 *         //函数体   #函数体中肯定有 RETURN 语句 如
 *          RETURN (SELECT 返回的字段 FROM 表名 WHERE 条件，一般会用到参数);
 *
 *      END $
 *      DELIMITER ;         //修改回来
 *
 *  若在创建存储函数中报错“`you might want to use the less safe
 *          log_bin_trust_function_creators variable`”，有两种处理方法：
 * - 方式1：加上必要的函数特性“[NOT] DETERMINISTIC”和
 *      “{CONTAINS SQL | NO SQL | READS SQL DATA | MODIFIES SQL DATA}”
 * - 方式2：创建函数前 设置数据库 默认这个值为0  会检验特性约束  所以要写特性约束   改成1就不用管了
 *     SET GLOBAL log_bin_trust_function_creators = 1;  改成一才能使用存储函数
 *
 * 只有 IN 而且 默认不用写   因为 RETURNS 表示 OUT
 *
 *     SET @自己起的名字 = 自己命名的值;   :=  真正意义上的赋值符号  都行
 * 调用   值直接就返回了
 *      SELECT 函数名;
 *
 *
 */

