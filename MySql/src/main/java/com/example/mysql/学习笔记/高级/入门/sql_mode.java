package com.example.mysql.学习笔记.高级.入门;

/**
 * 宽松模式和严格模式
 * 如果设置的是宽松模式：
 * set global sql_mode = '';//全局
 * set session sql_mode = '';//当前会话
 * 永久：需要改配置文件
 * 那么我们在插入数据的时候，即便是给了一一个错误的数据，也可能会被接受，并且不报错。
 *
 * 改为严格模式后可能会存在的问题:
 * 若设置模式中包含了NO_ZERO_DATE，那么MySQL数据库不允许插入零日期，插入零日期会抛出错误而不是警告。
 * 例如，表中含字段TIMESTAMP列(如果未声明为NULL或显示DEFAULT子句)将自动分配
 * DEFAULT 000-00-000:00:00 (零时间戳)， 这显然是不满足sqL_mode中的NO_ZERO_DATE而报错。
 *
 */
public class sql_mode {
}
