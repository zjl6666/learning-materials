package com.example.mysql.学习笔记.基础.变量_流程控制_游标.流程控制;

public class 分支结构 {
}
/**
 * 分支结构之 IF
 *
 * - IF 语句的语法结构是：
 *
 * IF 表达式1 THEN 操作1
 * [ELSEIF 表达式2 THEN 操作2]……
 * [ELSE 操作N]
 * END IF
 *
 * 根据表达式的结果为TRUE或FALSE执行相应的语句。这里“[]”中的内容是可选的。
 */

/**
 * 分支结构之 CASE
 *
 * CASE 语句的语法结构1：
 *
 * #情况一：类似于switch
 * CASE 表达式
 *      WHEN 值1 THEN 结果1或语句1(如果是语句，需要加分号)
 *      WHEN 值2 THEN 结果2或语句2(如果是语句，需要加分号)
 *      ...
 *      ELSE 结果n或语句n(如果是语句，需要加分号)
 * END [case]（如果是放在begin end中需要加上case，如果放在select后面不需要）
 *
 * CASE 语句的语法结构2：
 *
 * #情况二：类似于多重if
 * CASE
 * WHEN 条件1 THEN 结果1或语句1(如果是语句，需要加分号)
 * WHEN 条件2 THEN 结果2或语句2(如果是语句，需要加分号)
 * ...
 * ELSE 结果n或语句n(如果是语句，需要加分号)
 * END [case]（如果是放在begin end中需要加上case，如果放在select后面不需要）
 *
 */



