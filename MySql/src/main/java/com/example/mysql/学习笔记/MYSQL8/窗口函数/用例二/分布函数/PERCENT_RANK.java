package com.example.mysql.学习笔记.MYSQL8.窗口函数.用例二.分布函数;

public class PERCENT_RANK {
}
/**
 *
 * **1．PERCENT_RANK()函数**
 * PERCENT_RANK()函数是等级值百分比函数。按照如下方式进行计算。
 * ```mysql
 * (rank - 1) / (rows - 1)
 * ```
 * 其中，rank的值为使用RANK()函数产生的序号，rows的值为当前窗口的总记录数。
 * 举例：计算 goods 数据表中名称为“女装/女士精品”的类别下的商品的PERCENT_RANK值。
 * ```mysql
 * #写法一：
 * SELECT RANK() OVER (PARTITION BY category_id ORDER BY price DESC) AS r,
 * PERCENT_RANK() OVER (PARTITION BY category_id ORDER BY price DESC) AS pr,
 * id, category_id, category, NAME, price, stock
 * FROM goods
 * WHERE category_id = 1;
 *
 * #写法二：
 * mysql> SELECT RANK() OVER w AS r,
 * -> PERCENT_RANK() OVER w AS pr,
 * -> id, category_id, category, NAME, price, stock
 * -> FROM goods
 * -> WHERE category_id = 1 WINDOW w AS (PARTITION BY category_id ORDER BY price DESC);
 *
 *  WINDOW w AS (PARTITION BY category_id ORDER BY price DESC)//把括号内代码提取
 * +---+-----+----+-------------+---------------+----------+--------+-------+
 * | r | pr  | id | category_id | category      | NAME     | price  | stock |
 * +---+-----+----+-------------+---------------+----------+--------+-------+
 * | 1 |   0 |  6 |           1 | 女装/女士精品   | 呢绒外套  | 399.90 |  1200 |
 * | 2 | 0.2 |  3 |           1 | 女装/女士精品   | 卫衣     |  89.90 |  1500 |
 * | 2 | 0.2 |  4 |           1 | 女装/女士精品   | 牛仔裤   |  89.90 |  3500  |
 * | 4 | 0.6 |  2 |           1 | 女装/女士精品   | 连衣裙   |  79.90 |  2500  |
 * | 5 | 0.8 |  1 |           1 | 女装/女士精品   | T恤      |  39.90 |  1000 |
 * | 6 |   1 |  5 |           1 | 女装/女士精品   | 百褶裙   |  29.90  |   500 |
 * +---+-----+----+-------------+---------------+----------+--------+-------+
 * 6 rows in set (0.00 sec)
 * ```
 */
