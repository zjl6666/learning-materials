package com.example.mysql.学习笔记.高级.备份.其他日志.二进制日志;

public class 使用二进制日志恢复数据_4 {
}
/**
 * 5.4使用日志恢复数据
 * 如果MySQL服务器启用了二进制日志, 在数据库出现意外丢失数据时，
 * 可以使用MySQL binlog工具从指定的时间点开始(例如，最后一次备份)
 * 直到现在或另一个指定的时间点的日志中恢复数据。
 * mysqlbinlog恢复数据的语法如下:
 *
 * mysqlbinlog [option] filename|mysql -uuser -ppass;
 *                       日志名
 *
 * show binary logs;//查看二进制日志文件
 * flush logs;//刷新日志  会重新创一个二进制文件
 *             当需要用二进制文件恢复信息时  最好新创一个二进制文件
 *             因为如果不新创  恢复的操作还记录在同一个文件中，不好
 *
 * 方式一
 * show binlog events in '文件名字';//会返回二进制文件的详细信息
 *                          用来查看日志中的信息，
 *   找到需要恢复的操作，
 *  可以根据Pos的  从begin开始到另一个begin前结束
 *  的值恢复
 *  语句不重要，可百度
 * 方式二
 * mysqlbinlog ‘文件具体位置 文件的名字’;
 * 根据时间
 *
 *
 *
 */
