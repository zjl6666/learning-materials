package com.example.mysql.学习笔记.高级.备份.其他日志.二进制日志;

public class Main {
}
/**
 * 5.二进制日志(bin log)  也叫作变更日志(update log)   每次重启都会创建一个
 * binlog可以说是MySQL中比较重要的日志了，在日常开发及运维过程中，经常会遇到。
 * binlog即binary log,二进制日志文件，也叫作变更日志(update log)。
 * 它记录了数据库所有执行的DDL和DML等数据库更新事件的语句，
 * 但是不包含没有修改任何数据的语句(如数据查询语句select、show等) 。
 * 它以事件形式记录并保存在二进制文件中。通过这些信息，我们可以再现数据更新操作的全过程。
 * 如果想要记录所有语句(例如，为了识别有问题的查询)，需要使用通用查询日志。
 *
 * binlog主要应用场景:
 * ●一是用于数据恢复，如果MySQL数据库意外停止，可以通过二进制日志文件来查看用户执行了哪些操作，
 * 对数据库服务器文件做了哪些修改，然后根据二进制日志文件中的记录来恢复数据库服务器。
 * ●二是用于数据复制，由于日志的延续性和时效性，
 * master把它的二进制日志传递给slaves来达到master-slave数据一致的目的。
 *
 * 可以说MySQL数据库的数据备份、主备、主主、主从都离不开binlog,
 * 需要依靠binlog来同步数据， 保证数据一致性。
 *
 *
 */
/**
 * 每次系统重启   都会创建一个二进制文件  binlog.000001开始累加
 * show variables Like ’%log_bing%‘;
 *
 * Log_bin                  二进制日志是否开启
 * Log_bin_basename         二进制文件位置
 * Log_bin_index        是binlog文件的索引文件，这个文件管理了所有的binlog文件的目录
 * Log_bin_trust_function_creators   存储过程    默认OFF(关闭)
 *      因为二进制文件主要功能是主从复制，而存储函数有可能导致主从数据不一致
 *      如 now()函数  返回当前时间  如果此函数在存储过程中，二进制文件保存的是
 *      now()函数，那么在主从复制时因为同步的时候不是同一时间 ，导致now()时间不一致，导致错误
 * Log_bin_use_v1_row_events   已废弃
 * sql_Log_bin
 *
 *
 *
 */
