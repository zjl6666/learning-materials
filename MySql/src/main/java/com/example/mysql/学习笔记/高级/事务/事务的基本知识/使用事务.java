package com.example.mysql.学习笔记.高级.事务.事务的基本知识;

public class 使用事务 {//myisam不支持事务  InnoDB 支持事务
}
/**
 *2.1显式事务
 * 步骤1: START TRANSACTION 或者 BEGIN，作用是显式开启一个事务。
 * mysq1> BEGIN;
 * mysq1> START TRANSACTION;
 *
 * START TRANSACTION 语句相较于BEGIN特别之处在于，后边能跟随几个修饰符:
 * ①READ ONLY:  只读事务
 *   标识当前事务是一个只读事务，也就是属于该事务的数据库操作只能读取数据，而不能修改数据。
 *   补充:只读事务中只是不允许修改那些其他事务也能访问到的表中的数据，
 *       对于临时表来说(我们使用CREATE TMEPORARY TABLE创建的表)，
 *       由于它们只能在当前会话中可见，所以只读事务其实也是可以对临时表进行增、删、改操作的。
 * ②READ WRITE :  读写事务(默认的)
 * 标识当前事务是一个读写事务，也就是属于该事务的数据库操作既可以读取数据，也可以修改数据。
 * ③WITH CONSISTENT SNAPSHOT :启动一致性读。
 *  ●READ ONLY和READ WRITE是用来设置所谓的事务访问模式的，
 *   就是以只读还是读写的方式来访问数据库中的数据，
 *   一个事务的访问模式不能同时既设置为只读的也设置为读写的,
 *   所以不能同时把READ ONLY 和READ WRITE 放到START TRANSACTION 语句后边。
 *  ●如果我们不显式指定事务的访问模式，那么该事务的访问模式就是读写模式。
 *
 *
 * 步骤2:一系列事务中的操作(主要是DML,不含DDL)
 * 步骤3:提交事务或中止事务(即回滚事务)
 *      mysq1> COMMIT ;#提交事务。当提交事务后，对数据库的修改是永久性的。
 *      mysq1> ROLLBACK;#回滚事务。即撤销正在进行的所有没有提交的修改
 *      mysql> ROLLBACK TO [ SAVEPOINT ] #将事务回滚到某个保存点。
 *   其中关于SAVEPOINT相关操作有:
 *     SAVEPOINT 保存点名称;#在事务中创建保存点，方便后续针对保存点进行回滚。一个事务中可以存在多个保存点。
 *
 *     RELEASE SAVEPOINT 保存点名称;//#删除某个保存点。
 *
 *
 */
/**
 * #2.2.隐式事务
 * 默认sql语句是  自动提交的(不可回滚)
 * # 3.1关键字: autocommit
 * #set autocommit = false;//关闭自动提交
 * SHOW VARIABLES LIKE 'autocommit' ;#默认是ON
 *
 * begin// (开始事务)  即使 autocommit = true;//自动提交    也不会提交
 * START TRANSACTION;//开始交易 (开始事务)
 * 一系列的sql语句执行
 * COMMIT;//提交事务    或者 👇
 * ROLLBACK;回滚数据，一旦执行ROLLBACK,则可以实现数据的回滚。 事务
 *
 * 补充: Oracle 默认不自动提交，需要手写COMMIT命令，而MySQL默认自动提交。
 *
 */
/**
  *2.3隐式提交数据的情况 简单来说  只有DML可以不自动提交
 * ●数据定义语言(Data definition language,缩写为: DDL)
 * 数据库对象，指的就是数据库、表、视图、存储过程等结构。
 * 当我们使用 CREATE(创建)、ALTER(更改)、 DROP 等语句去修改数据库对象时，
 * 就会隐式的提交前边语句所属于的事务。
 *●隐式使用或修改mysql数据库中的表
 * 当我们使用ALTER USER、 CREATE USER、 DROP USER、 GRANT、 RENAME USER、
 * REVOKE 、SET PASSWORD等语句时也会隐式的提交前边语句所属于的事务。
 *
 * ①当我们在一个事务还没提交或者回滚时就又使用START TRANSACTION
 *   或者BEGIN语句开启了另一个事务时，会隐式的提交，上一个事务。
 * ②当前的autocommit系统变量的值为0FF,我们手动把它调为ON时，
 *  也会隐式的提交前边语句所属的事务。
 * ③使用LOCK TABLES、UNLOCK TABLES 等关于锁定的语句也会隐式的提交前边语句所属的事务。
 * ●加载数据的语句
 * 使用LOAD DATA 语句来批量往数据库中导入数据时，也会隐式的提交前边语句所属的事务。
 * ●关于MySQL复制的一些语句
 * 使用START SLAVE、STOP SLAVE、RESET SLAVE、 CHANGE MASTER TO等语句时
 * 会隐式的提交前边语句所属的事务。
 * ●其它的一些语句
 * 使用ANALYZE TABLE、 CACHE INDEX、
 * CHECK TABLE、 FLUSH、 LOAD INDEX INTO CACHE 、
 * OPTIMIZE TABLE、 REPAIR TABLE、 RESET 等语句也会隐式的提交前边语句所属的事务。
 *
 */
/**
 * 你能看到相同的SQL代码，只是在事务开始之前设置了SET @@completion_type = 1;
 * 这里我讲解下MySQL中completion. type参数的作用，实际上这个参数有3种可能:
 * 1. completion=0, 这是默认情况。
 *   当我们执行COMMIT的时候会提交事务,在执行下一个事务时，
 *   还需要使用START TRANSACTION 或者BEGIN 来开启。
 * 2. completion=1，这种情况下，当我们提交事务后，
 *   相当于执行了COMMIT AND CHAIN,也就是开启一个链式事务，
 *   即当我们提交事务之后会开启一个相同隔离级别的事务。
 * 3. completion=2 ，这种情况下COMMIT=COMMIT AND RELEASE ，
 *   也就是当我们提交后，会自动与服务器断开连接。
 *
 * 当我们设置autocommit=0时，不论是否采用START TRANSACTION或者BEGIN的方式来开启事务，
 * 都需要用COMMIT进行提交，让事务生效，使用ROLLBACK对事务进行回滚。
 * 当我们设置autocommit=1时，每条SQL语句都会自动进行提交。
 * 不过这时，如果你采用START TRANSACTION或者BEGIN的方式来显式地开启事务，
 * 那么这个事务只有在COMMIT时才会生效，在ROLLBACK时才会回滚。
 *
 */