package com.example.mysql.学习笔记.基础.变量_流程控制_游标.流程控制;

public class 跳转语句 {
}

/**
 * 跳转语句之LEAVE语句
 *
 * LEAVE语句：
 *      可以用在循环语句内，或者以 BEGIN 和 END 包裹起来的程序体内，
 *      表示跳出循环或者跳出程序体的操作。
 *      如果你有面向过程的编程语言的使用经验，你可以把 LEAVE 理解为 break。
 *
 * 基本格式如下：
 *
 * LEAVE 标记名
 *
 * DELIMITER $            //设置$为结束符号  $可变
 * CREATE PROCEDURE 存储过程名(IN|OUT|INOUT 参数名  参数类型,...)
 * [characteristics ...]   //特性  可有可无
 *      起个名字:BEGIN
 *           IF 条件 THEN LEAVE 起个名字;//LEAVE根据名字跳出运行程序
 *      END $
 * DELIMITER ;         //修改回来
 *
 *
 */

/**
 * 跳转语句之ITERATE语句
 *
 * ITERATE语句：
 * ``只能用在循环语句（LOOP、REPEAT和WHILE语句）内，
 * ``表示重新开始循环，将执行顺序转到语句段开头处。
 * ``如果你有面向过程的编程语言的使用经验，
 * ``你可以把 ITERATE 理解为 continue，意思为“再次循环”。
 *
 * 语句基本格式如下：
 *
 * ITERATE label
 *
 * label参数表示循环的标志。ITERATE语句必须跟在循环标志前面。
 *
 */
