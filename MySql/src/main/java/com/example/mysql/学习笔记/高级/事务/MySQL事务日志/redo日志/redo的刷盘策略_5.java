package com.example.mysql.学习笔记.高级.事务.MySQL事务日志.redo日志;

public class redo的刷盘策略_5 {
}
/**
 * 1.5 redo log的刷盘策略
 * redo log的写入并不是直接写入磁盘的，
 * InnoDB引擎 会在写redo log的时候先写redo log buffer,
 * 之后以- 定的频率刷入到真正的redo log file(在磁盘中，持久化了)中。
 * 这里的一定频率怎么看待呢? 这就是我们要说的刷盘策略。
 *
 * 先写入redo log buffer--->文件系统缓存(page cache)--->
 * redo log file（也在磁盘中）--->磁盘
 *
 *
 * 注意，redo log buffer刷盘到redo log file的过程并不是真正的刷到磁盘中去，
 * 只是刷入到文件系统缓存(page cache)中去(这是现代操作系统为了提高文件写入效率做的一个优化)，
 * 真正的写入会交给系统自己来决定(比如page cache足够大了)。
 * 那么对于InnoDB来说就存在一个问题，如果交给系统来同步，同样如果系统宕机，
 * 那么数据也丢失了(虽然整个系统宕机的概率还是比较小的)。
 *
 * 针对这种情况，InnoDB给出 innodb_flush_log_at_trx_commit 参数,
 * 该参数控制commit提交事务时，如何将redo log buffer中的日志刷新到redo log file中。
 * 它支持3种策略:
 * ●设置为0 :表示每次事务提交时不进行刷盘操作。
 *         (系统默认master thread每隔1s进行一次重做日志的同步)
 * ●设置为1 :表示每次事务提交时都将进行同步，刷盘操作 (默认值)
 * ●设置为2 :表示每次事务提交时都只把redo log buffer内容写入page cache, 不进行同步。
 *         由os自己决定什么时候同步到磁盘文件。
 *
 * 另外，InnoDB存储引擎有一个后台线程，每隔1秒，就会把redo log buffer
 * 中的内容写到文件系统缓存(page cache（软件宕机没事，电脑宕机有事）) ，
 * 然后调用刷盘(停电都没事)操作。
 *
 * 除了后台线程每秒1次的轮询操作,还有一种情况，当redo log buffer 占用的空间即将达到
 * innodb_log_buffer_size (这个参数默认是16M) 的一半的时候，后台线程会主动刷盘。
 *
 *
 */