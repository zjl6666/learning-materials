package com.example.mysql.学习笔记;

/**
 * 8.0  信息支持中文
 * 5.7  不支持
 *
 * sql分类
 *
 * DDL:数据定义语言。CREATE \ ALTER \ DROP \ RENAME \ TRUNCATE  //一旦执行  不可回滚
 * DML:数据操作语言。INSERT \ DELETE \ UPDATE \ SELECT
 * DCL:数据控制语言。COMMIT \ ROLLBACK \ SAVEPOINT \ GRANT \ REVOKE
 *
 * DML//一旦执行  默认不可回滚  如果先执行SET autocommit = FALSE  就可以回滚（事务）
 * 可以把 SQL 分为两个部分：数据操作语言 (DML) 和 数据定义语言 (DDL)。
 * SQL (结构化查询语言)是用于执行查询的语法。但是 SQL 语言也包含用于更新、插入和删除记录的语法。
 *
 * 查询和更新指令构成了 SQL 的 DML 部分：
 * SELECT - 从数据库表中获取数据
 * UPDATE - 更新数据库表中的数据
 * DELETE - 从数据库表中删除数据
 * INSERT INTO - 向数据库表中插入数据
 * SQL 的数据定义语言 (DDL) 部分使我们有能力创建或删除表格。
 * 我们也可以定义索引（键），规定表之间的链接，以及施加表间的约束。
 *
 * SQL 中最重要的 DDL 语句:
 * CREATE DATABASE - 创建新数据库
 * ALTER DATABASE - 修改数据库
 * CREATE TABLE - 创建新表
 * ALTER TABLE - 变更（改变）数据库表
 * DROP TABLE - 删除表
 * CREATE INDEX - 创建索引（搜索键）
 * DROP INDEX - 删除索引
 *
 * DDL的原子化，
 *      在MySQL 8.0版本中, InnoDB表的DDL支持事务完整性,即DDL操作要么成功要么回滚。
 *      DDL操作回滚日志写入到data dictionary数据
 *      字典表mysql.innodb_ddl_log (该表是隐藏的表,通过show tables无法看到)中，
 *      用于回滚操作。通过设置参数,可将DDL操作日志打印输出到MySQL错误日志中。
 */
public class sql分类DDL_DML_DCL {
}
