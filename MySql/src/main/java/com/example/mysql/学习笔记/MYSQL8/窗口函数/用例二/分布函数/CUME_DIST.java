package com.example.mysql.学习笔记.MYSQL8.窗口函数.用例二.分布函数;

public class CUME_DIST {
}
/**
 * **2．CUME_DIST()函数**
 *
 * CUME_DIST()函数主要用于查询小于或等于某个值的比例。
 *
 * 举例：查询goods数据表中小于或等于当前价格的比例。
 *
 * ```mysql
 * mysql> SELECT CUME_DIST() OVER(PARTITION BY category_id ORDER BY price ASC) AS cd,
 *     -> id, category, NAME, price
 *     -> FROM goods;
 * +---------------------+----+---------------+------------+---------+
 * | cd                  | id | category      | NAME       | price   |
 * +---------------------+----+---------------+------------+---------+
 * | 0.16666666666666666 |  5 | 女装/女士精品   | 百褶裙      |   29.90 |
 * |  0.3333333333333333 |  1 | 女装/女士精品   | T恤        |   39.90 |
 * |                 0.5 |  2 | 女装/女士精品   | 连衣裙      |   79.90 |
 * |  0.8333333333333334 |  3 | 女装/女士精品   | 卫衣        |   89.90 |
 * |  0.8333333333333334 |  4 | 女装/女士精品   | 牛仔裤      |   89.90 |
 * |                   1 |  6 | 女装/女士精品   | 呢绒外套    |  399.90 |
 * | 0.16666666666666666 |  9 | 户外运动       | 登山杖      |   59.90 |
 * |                 0.5 |  7 | 户外运动       | 自行车      |  399.90 |
 * |                 0.5 | 10 | 户外运动       | 骑行装备     |  399.90 |
 * |  0.6666666666666666 | 12 | 户外运动       | 滑板        |  499.90 |
 * |  0.8333333333333334 | 11 | 户外运动       | 运动外套    |  799.90 |
 * |                   1 |  8 | 户外运动       | 山地自行车   | 1399.90 |
 * +---------------------+----+---------------+------------+---------+
 * 12 rows in set (0.00 sec)
 * ```
 */
