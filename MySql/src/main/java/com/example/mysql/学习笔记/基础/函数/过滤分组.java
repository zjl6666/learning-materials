package com.example.mysql.学习笔记.基础.函数;

public class 过滤分组 {
}

/**过滤分组
 * // 把不同的字段进行分组   声明在where之后    ORDER BY前面、LIMIT 前面
 * GROUP BY 字段1,字段2
 * //字段如果不唯一  那么应该是 字段1和字段2 都一样才是一组  两个字段 顺序不影响
 * 如果  查询的时候  显示的时候有个字段个数不一致，那么显示的是错误的
 *      mysql不会报错，有些可视化工具会错误 mysql严格来说是不报错的
 *      oracle会报错
 *
 *  GROUP BY 字段1,字段2 WITH ROLLUP
 *  使用WITH ROLLUP 关键字之后，在所有查询出的分组记录之后增加一条记录，
 *  该记录计算查询出的所有记录的总和，即统计记录数量。
 *
 *
 *  HAVING  放在GROUP BY之后 HAVING不要单独使用，和GROUP BY一起
 *  //查询各个部门中最高工资比10000高的部门信息
 *        和where作用一样   """"""where里不可以写聚合函数""""""  而HAVING可以
 *  WHERE 执行效率高于    HAVING
 * 当过滤条件中有聚合函数时，则此过滤条件必须声明在HAVING中。
 * 当过滤条件中没有聚合函数时，则此过滤条件声明在WHERE中或HAVING中都可以。
 *      但是，建议大家声明在WHERE中
 *
 *           优点                              缺点
 * WHERE  先筛选数据再关联，执行效率高     不能使用分组中的计算函数进行筛选
 * HAVING 可以使用分组中的计算函数         在最后的结果集中进行筛选，执行效率较低
 *
 *
 * in  以主表为基础，多次遍历从表          从表越小越好   用主表字段的索引
 * EXISTS   以从表为基础   多次遍历主表   主表越小越好   用从表字段的索引
 *
 * EXISTS 和  NOT EXISTS
 * ●如果在子查询中不存在满足条件的行:
 * 。条件返回FALSE
 * 。继续在子查询中查找
 * ●如果在子查询中存在满足条件的行:
 * 。不在子查询中继续查找
 * 。条件返回TRUE
 *
 * GROUP_CONCAT(CONCAT(name,"(",salary,")") SEPARATOR ';')
 * 将利用name(salary)进行连接，其中分隔符为';'
 *
 */