package com.example.mysql.学习笔记.基础.建库建表;

/**
 * CREATE TABLE IF NOT EXISTS 表名(
 * 字段名 数据类型 COMMENT ‘备注’,
 * 字段名 数据类型,
 * 字段名 数据类型,
 * 字段名 数据类型,
 * 字段名 数据类型
 * );
 *          使用VARCHAR来定义字符串，必须在使用VARCHAR时指明其长度。
 * COMMENT ‘备注’ 建表的时候加上最好
 * CREATE TEMPORARY TABLE 表名       -- 创建临时表
 * 查询语句
 * //一般临时表  都是查询出来的
 *
 * DESC 表名;   查看表结构
 * SHOW CREATE TABLE 表名;  //查看表的创建信息  如字符集
 *
 * 创建表的时候   如果没指明字符集   默认用数据库的
 *
 * 基于现有表  建一个表   这样复制的表 除了非空约束  没有别的约束
 *    CREATE TABLE 表名
 *    AS
 *    SELECT 字段1,字段2,字段3
 *    FROM 另一个表;//如果这个表不再这个数据库下可以
 *                  //数据库名.另一个表 有权限问题
 *
 * 把这个表的信息  摘取  创建一个表  并保留信息
 *   类似子查询的表  保存了
 *
 * 重命名表
 *      RENAME TABLE 旧表名
 *      TO 新表名
 *
 *      RENAME TABLE 旧表名
 *      RENAME TO 新表名
 * 删除表    不可以撤销  即回滚
 *     DROP TABLE IF EXISTS 表名;IF EXISTS 有这个  可以没表的时候不报错
 * 清空表 清空表中的数据 保留表结构
 *     TRUNCATE TABLE 表名;
 *         一旦执行   数据不可回滚    ROLLBACK：回滚数据
 *
 *     DELETE FROM 表名;
 *         "可以"回滚
 *
 * 都可以删除表的所有数据
 *
 *
 * 5.7中 DROP TABLE  表名1,表名2； 如果1存在2不存在  1会删掉
 * 8.0有原子性   都不会删掉
 *
 *
 */

/**
 * 字段的操作
 *  添加字段
 *      ALTER TABLE 表名
 *      ADD 字段名 数据类型 此字段的位置；//默认最后一个
 *
 *           FIRST 让这个字段当第一个
 *           AFTER 字段  加在此字段后面
 *  修改字段  数据类型一般不会修改   一般该长度，默认值啥的
 *      ALTER TABLE 表名
 *      MODIFY 字段 数据类型 DEFAULT '默认值'
 *
 *  重命名字段
 *      ALTER TABLE 表名
 *      CHANGE 旧字段名 新字段名 类型
 *
 *      类型必须加否则失败  也可以改字段
 *  删除字段
 *      ALTER TABLE 表名
 *      DROP COLUMN 字段名
 *
 * 如果字段原来的大小大于你修改后的大小  会报错
 *
 *
 *
 */
public class 建表 {
    public static void main(String[] args) {
        System.out.println('1'&15);
    }
}
