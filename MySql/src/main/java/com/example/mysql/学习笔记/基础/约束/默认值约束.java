package com.example.mysql.学习笔记.基础.约束;

public class 默认值约束 {
}
/**
 * DEFAULT约束
 * 作用
 *      给某个字段/某列指定默认值，一旦设置默认值，
 *      在插入数据时，如果此字段没有显式赋值，则赋值为默认值。
 *
 * ###  如何给字段加默认值
 *
 * ```mysql
 * create table 表名称(
 *     字段名  数据类型  not null default 默认值,
 * );
 *
 *  alter table 表名称 modify 字段名 字段类型 约束;
 */
