package com.example.mysql.学习笔记.基础.变量_流程控制_游标.变量;

public class 系统变量 {
}

/**
 * 变量由系统定义，不是用户定义，属于`服务器`层面。
 * 启动MySQL服务，生成MySQL服务实例期间，
 * MySQL将为MySQL服务器内存中的系统变量赋值，
 * 这些系统变量定义了当前MySQL服务实例的属性、特征。
 * 这些系统变量的值要么是`编译MySQL时参数`的默认值，
 * 要么是`配置文件`（例如my.ini等）中的参数值。
 *
 * 系统变量分为全局系统变量（需要添加`global` 关键字）
 *       以及会话系统变量（需要添加 `session` 关键字），
 *       有时也把全局系统变量简称为全局变量，
 *       有时也把会话系统变量称为local变量。
 *       **如果不写，默认会话级别。**
 *   静态变量（在 MySQL 服务实例运行期间它们的值不能使用 set 动态修改）
 *      属于特殊的全局系统变量。
 *
 * 每一个MySQL客户机成功连接MySQL服务器后，都会产生与之对应的会话。
 *      会话期间，MySQL服务实例会在MySQL服务器内存中生成与该会话对应的会话系统变量，
 *      这些会话系统变量的初始值是全局系统变量值的复制。
 *
 * - 全局系统变量针对于所有会话（连接）有效，但`不能跨重启`
 * - 会话系统变量仅针对于当前会话（连接）有效。会话期间，
 *   当前会话对某个会话系统变量值的修改，不会影响其他会话同一个会话系统变量的值。
 * - 会话1对某个全局系统变量值的修改会导致会话2中同一个全局系统变量值的修改。
 *
 * 在MySQL中有些系统变量只能是全局的，
 *      例如 max_connections 用于限制服务器的最大连接数；
 * 有些系统变量作用域既可以是全局又可以是会话，
 *      例如 character_set_client 用于设置客户端的字符集；
 * 有些系统变量的作用域只能是当前会话，
 *      例如 pseudo_thread_id 用于标记当前会话的 MySQL 连接 ID。
 *
 * 默认是 SESSION 会话
 *
 * #查看所有全局变量
 * SHOW GLOBAL VARIABLES;
 *
 * #查看所有会话变量
 * SHOW SESSION VARIABLES;
 * 或
 * SHOW VARIABLES;
 *
 * #查看满足条件的部分系统变量。
 * SHOW GLOBAL VARIABLES LIKE '%标识符%';
 *
 * #查看满足条件的部分会话变量
 * SHOW SESSION VARIABLES LIKE '%标识符%';
 */

/**
 * 查看
 * - **查看指定系统变量**
 * 作为 MySQL 编码规范，MySQL 中的系统变量以`两个“@”`开头，
 *      其中“@@global” 仅用于标记全局系统变量，
 *         “@@session”仅用于标记会话系统变量。
 *         “@@”首先标记会话系统变量，如果会话系统变量不存在，则标记全局系统变量。
 *
 *
 * #查看指定的系统变量的值
 * SELECT @@global.变量名;
 * #查看指定的会话变量的值
 * SELECT @@session.变量名;
 * #或者
 * SELECT @@变量名;
 */

/**
 * 修改
 * **修改系统变量的值**
 * 有些时候，数据库管理员需要修改系统变量的默认值，
 * 以便修改当前会话或者MySQL服务实例的属性、特征。具体方法：
 *
 * 方式1：修改MySQL`配置文件`，继而修改MySQL系统变量的值（该方法需要重启MySQL服务）
 * 方式2：在MySQL服务运行期间，使用“set”命令重新设置系统变量的值  一旦重启  就失效了

 * #为某个系统变量赋值
 * #方式1： SET @@global.变量名=变量值;
 * #方式2： SET GLOBAL 变量名=变量值;
 *
 *
 * #为某个会话变量赋值
 * #方式1： SET @@session.变量名=变量值;
 * #方式2： SET SESSION 变量名=变量值;
 *
 *
 */


