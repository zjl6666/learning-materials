package com.example.mysql.学习笔记.基础.触发器;

public class 概述 {
}
/**
 * 在实际开发中，我们经常会遇到这样的情况：
 * 有 2 个或者多个相互关联的表，如`商品信息`和`库存信息`分别存放在 2 个不同的数据表中，
 * 我们在添加一条新商品记录的时候，为了保证数据的完整性，必须同时在库存表中添加一条库存记录。
 *
 * 这样一来，我们就必须把这两个关联的操作步骤写到程序里面，而且要用`事务`包裹起来，
 * 确保这两个操作成为一个`原子操作`，要么全部执行，要么全部不执行。
 * 要是遇到特殊情况，可能还需要对数据进行手动维护，这样就很`容易忘记其中的一步`，导致数据缺失。
 *
 * 这个时候，咱们可以使用触发器。
 * **你可以创建一个触发器，让商品信息数据的插入操作自动触发库存数据的插入操作。**
 * 这样一来，就不用担心因为忘记添加库存数据而导致的数据缺失了。
 *
 * 触发器概述
 *
 * MySQL从`5.0.2`版本开始支持触发器。
 * MySQL的触发器和存储过程一样，都是嵌入到MySQL服务器的一段程序。
 *
 * 触发器是由`事件来触发`某个操作，这些事件包括`INSERT`、`UPDATE`、`DELETE`事件。
 * 所谓事件就是指用户的动作或者触发某项行为。
 * 如果定义了触发程序，当数据库执行这些语句时候，就相当于事件发生了，
 * 就会`自动`激发触发器执行相应的操作。
 *
 * 当对数据表中的数据执行插入、更新和删除操作，
 * 需要自动执行一些数据库逻辑时，可以使用触发器来实现。
 *
 */

/**
 * 创建触发器语法
 *
 * 创建触发器的语法结构是：
 *
 * CREATE TRIGGER 触发器名称
 * {BEFORE|AFTER} {INSERT|UPDATE|DELETE} ON 表名
 * FOR EACH ROW
 * 触发器执行的语句块;
 *
 * 说明：
 *
 * - `表名`：表示触发器监控的对象。
 * - `BEFORE|AFTER`：表示触发的时间。
 *      BEFORE 表示在事件之前触发；AFTER 表示在事件之后触发。
 * - `INSERT|UPDATE|DELETE`：表示触发的事件。
 *   - INSERT 表示插入记录时触发；
 *   - UPDATE 表示更新记录时触发；
 *   - DELETE 表示删除记录时触发。
 *
 *
 * - `触发器执行的语句块`：可以是单条SQL语句，
 *    也可以是由BEGIN…END结构组成的复合语句块。
 *
 *
 * DELIMITER //
 *
 * CREATE TRIGGER 触发器名字
 * BEFORE INSERT ON 监听的表 FOR EACH ROW
 * BEGIN
 * 	DECLARE 局部变量 DOUBLE;
 * 	SELECT 字段 INTO 局部变量 FROM 表名 WHERE 字段 = NEW.字段;//NEW 你添加的字段信息  NEW.字段 你添加的信息
 * // NEW :表示添加的数据     OLD:表示删除的信息
 * 	IF NEW.字段 > 局部变量 THEN
 * 		SIGNAL SQLSTATE 'HY000' SET MESSAGE_TEXT = '薪资高于领导薪资错误';//自定义错误信息
 * 	END IF;
 * END //
 *
 * DELIMITER ;
 *
 * FOR EACH ROW //如果是批量操作 也会批量触发
 *
 */

/**
 * 查看触发器
 *
 * 查看触发器是查看数据库中已经存在的触发器的定义、状态和语法信息等。
 *
 * 方式1：查看当前数据库的所有触发器的定义
 * //   \G 在命令行在最后加个 \G 可以分行查看
 * SHOW TRIGGERS
 *
 * 方式2：查看当前数据库中某个触发器的定义
 *
 * SHOW CREATE TRIGGER 触发器名
 *
 * 方式3：从系统库information_schema的TRIGGERS表中查询
 *       “salary_check_trigger”触发器的信息。
 *
 * SELECT * FROM information_schema.TRIGGERS;
 *
 *
 */

/**
 *  删除触发器
 *
 * 触发器也是数据库对象，删除触发器也用DROP语句，语法格式如下：
 *
 * DROP TRIGGER  IF EXISTS 触发器名称;
 *
 *
 */


