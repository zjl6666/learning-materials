package com.example.mysql.学习笔记.MYSQL8.共用表达式;

public class 递归公用表表达式 {
}
/**
 * 递归公用表表达式
 *
 * 递归公用表表达式也是一种公用表表达式，
 * 只不过，除了普通公用表表达式的特点以外，它还有自己的特点，
 * 就是**可以调用自己**。它的语法结构是：
 *
 * ```mysql
 * WITH RECURSIVE
 * CTE名称 AS （子查询）
 * SELECT|DELETE|UPDATE 语句;
 * ```
 *
 * 递归公用表表达式由 2 部分组成，分别是种子查询和递归查询，
 * 中间通过关键字 UNION  [ALL]进行连接。
 * 这里的**种子查询，意思就是获得递归的初始值**。
 * 这个查询只会运行一次，以创建初始数据集，之后递归查询会一直执行，
 * 直到没有任何新的查询数据产生，递归返回。
 *
 * WITH RECURSIVE cte
 * AS
 * (
 * SELECT employee_id,last_name,manager_id,
 * 1 AS n FROM employees WHERE employee_id = 100 //-- 种子查询，找到第一代领导
 * UNION ALL
 * SELECT a.employee_id,a.last_name,a.manager_id,
 * n+1 FROM employees AS a JOIN cte
 * ON (a.manager_id = cte.employee_id) -- 递归查询，找出以递归公用表表达式的人为领导的人
 * )
 * SELECT employee_id,last_name FROM cte WHERE n >= 3;
 *
 *
 */
