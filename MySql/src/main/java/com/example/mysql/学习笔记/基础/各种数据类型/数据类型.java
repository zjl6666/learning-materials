package com.example.mysql.学习笔记.基础.各种数据类型;

public class 数据类型 {
}
/**
 *   数据类型
 * | 整数类型         | TINYINT、SMALLINT、MEDIUMINT、**INT(或INTEGER)**、BIGINT     |
 * | 浮点类型         | FLOAT、DOUBLE                                                |
 * | 定点数类型        | **DECIMAL**                                                  |
 * | 位类型           | BIT                                                          |
 * | 日期时间类型      | YEAR、TIME、**DATE**、DATETIME、TIMESTAMP                    |
 * | 文本字符串类型    | CHAR、**VARCHAR**、TINYTEXT、TEXT、MEDIUMTEXT、LONGTEXT      |
 * | 枚举类型         | ENUM                                                         |
 * | 集合类型         | SET                                                          |
 * | 二进制字符串类型   | BINARY、VARBINARY、TINYBLOB、BLOB、MEDIUMBLOB、LONGBLOB      |
 * | JSON类型         | JSON对象、JSON数组                                           |
 * | 空间数据类型      | 单值：GEOMETRY、POINT、LINESTRING、POLYGON；
 *                     集合：MULTIPOINT、MULTILINESTRING、MULTIPOLYGON、GEOMETRYCOLLECTION |
 *
 *   加   **重点**
 *
 *
 * | INT           | 从-2^31到2^31-1的整型数据。存储大小为 4个字节                |
 * | CHAR(size)    | 定长字符数据。若未指定，默认为1个字符，最大长度255           |
 * | VARCHAR(size) | 可变长字符数据，根据字符串实际长度保存，**必须指定长度**     |
 * | FLOAT(M,D)    | 单精度，占用4个字节，M=整数位+小数位，D=小数位。 D<=M<=255,0<=D<=30，默认M+D<=6 |
 * | DOUBLE(M,D)   | 双精度，占用8个字节，D<=M<=255,0<=D<=30，默认M+D<=15         |
 * | DECIMAL(M,D)  | 高精度小数，占用M+2个字节，D<=M<=65，0<=D<=30，最大取值范围与DOUBLE相同。 |
 * | DATE          | 日期型数据，格式'YYYY-MM-DD'                                 |
 * | BLOB          | 二进制形式的长文本数据，最大可达4G                           |
 * | TEXT          | 长文本数据，最大可达4G                                       |
 *
 *
 *
 * | MySQL关键字        | 含义                     |
 * | ------------------ | ------------------------ |
 * | NULL               | 数据列可包含NULL值       |
 * | NOT NULL           | 数据列不允许包含NULL值   |
 * | DEFAULT            | 默认值                   |
 * | PRIMARY KEY        | 主键                     |
 * | AUTO_INCREMENT     | 自动递增，适用于整数类型 |
 * | UNSIGNED           | 无符号      表示这个数只能是个正数或0  |
 * | CHARACTER SET name | 指定一个字符集           |
 *
 */