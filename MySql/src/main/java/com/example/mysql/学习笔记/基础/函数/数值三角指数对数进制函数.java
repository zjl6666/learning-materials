package com.example.mysql.学习笔记.基础.函数;

public class 数值三角指数对数进制函数 {
    /**
     * 单行函数  放一个   出来一个
     * 多行函数  放多个   出来一个 如：最大值，求和等
     */

    /**
     * 数值函数
     * ABS(x)              | 返回x的绝对值
     * SIGN(X)             | 返回X的符号。正数返回1，负数返回-1，0返回
     * PI()                | 返回圆周率,返回 3.141593
     * CEIL(x)，CEILING(x) |  返回大于或等于某个值的最小整数
     * FLOOR(x)            | 返回小于或等于某个值的最大整数
     * LEAST(e1,e2,e3…)    | 返回列表中的最小值
     * GREATEST(e1,e2,e3…) | 返回列表中的最大值
     * MOD(x,y)            | 返回X除以Y后的余数
     * RAND()              | 返回0~1的随机值
     * RAND(x)             | 返回0~1的随机值，其中x的值用作种子值，相同的X值会产生相同的随机数 x相同
     *      伪随机数，通过某个算法  x相同 随机数也相同
     * ROUND(x)            | 返回一个对x的值进行四舍五入后，最接近于X的整数
     * ROUND(x,y)          | 返回一个对x的值进行四舍五入后最接近X的值，并保留到小数点后面Y位
     *              不足位补0  ，-1表示舍去个位  -2表示舍去百位---  不够舍结过位0
     * TRUNCATE(x,y)       | 返回数字x"截断"为y位小数的结果  直接舍掉
     * SQRT(x)             | 返回x的平方根。当X的值为负数时，返回NULL
     *
     */

    /**
     * 三角函数
     * | RADIANS(x) | 将角度转化为弧度，其中，参数x为角度值 |   RADIANS(180)=π
     * | DEGREES(x) | 将弧度转化为角度，其中，参数x为弧度值 |   DEGREES(2π)=360
     *
     * | SIN(x)     | 返回x的正弦值，其中，参数x为弧度值
     * | ASIN(x)    | 返回x的反正弦值，即获取正弦为x的值。如果x的值不在-1到1之间，则返回NULL
     * | COS(x)     | 返回x的余弦值，其中，参数x为弧度值
     * | ACOS(x)    | 返回x的反余弦值，即获取余弦为x的值。如果x的值不在-1到1之间，则返回NULL
     * | TAN(x)     | 返回x的正切值，其中，参数x为弧度值
     * | ATAN(x)    | 返回x的反正切值，即返回正切值为x的值
     * | ATAN2(m,n) | 返回两个参数的反正切值
     * | COT(x)     | 返回x的余切值，其中，X为弧度值
     *
     *       |\
     *       |  \  b
     *    a  |   \
     *       |————\
     *         c
     */
    /**
     * 指数和对数
     * | POW(x,y)，POWER(X,Y) | 返回x的y次方
     * | EXP(X)               | 返回e的X次方，其中e是一个常数，2.718281828459045
     * | LN(X)，LOG(X)        | 返回以e为底的X的对数，当X <= 0 时，返回的结果为NULL
     * | LOG10(X)             | 返回以10为底的X的对数，当X <= 0 时，返回的结果为NULL
     * | LOG2(X)              | 返回以2为底的X的对数，当X <= 0 时，返回NULL
     *
     */
    /**
     * 进制函数
     * BIN(x)           返回x的二进制
     * HEX(x)           返回x的十六进制
     * OCT(x)           返回x的八进制
     * CONV(x,f1,f2)   返回f1进制数变成f2进制数
     */
}