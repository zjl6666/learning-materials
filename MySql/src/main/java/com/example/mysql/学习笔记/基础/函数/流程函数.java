package com.example.mysql.学习笔记.基础.函数;

public class 流程函数 {
}
/**
 * IF(value,value1,value2)  如果value的值为TRUE，返回value1， 否则返回value2
 * IFNULL(value1, value2)  如果value1不为NULL，返回value1，否 则返回value2
 * CASE   WHEN 条件1 THEN 结果1     WHEN 条件2 THEN 结果2
 * .... ELSE 结果n END 重命名 //ELSE可以不要 重命名可以不要  没在条件的时null
 * 相当于Java的if...else if...else...
 *
 *
 * CASE expr WHEN 常量值1 THEN 值1 WHEN 常量值2 THEN 值2 .... [ELSE 值n] END
 * expr=常量值1 执行 值1
 * expr=常量值2 执行 值2
 * 相当于Java的switch...case...
 *
 * CASE  WHEN 计算(数量>5) THEN '数比较多'
 *      WHEN 计算(数量>5) THEN '数比较多'
 *      WHEN 计算(数量>5) THEN '数比较多'
 *      ELSE '数比较多'  END
 *
 * 循环  为啥没  因为查询自带循环   因为查找是把所有信息都循环查看了
 */
/**
 * 加密   一般在数据库   不进行加密  都在服务器加密
 * 不可逆
 * PASSWORD(str) 在8.0就废弃了
 * PASSWORD(str)   返回字符串str的加密版本，41位长的字符串。加密结果不可逆，常用于用的密码加密
 * MD5(str)
 * SHA(str)  从原明文密码str计算并返回加密后的密码字符串，当参数为NULL时， 返回NULL。SHA加密 算法比MD5更加安全。
 */
/**
 * VERSION() 返回当前MySQL的版本号
 * CONNECTION_ID() 返回当前MySQL服务器的连接数
 * DATABASE()，SCHEMA() 返回MySQL命令行当前所在的数据库
 * USER()，CURRENT_USER()、SYSTEM_USER()，SESSION_USER()
 *          返回当前连接MySQL的用户名，返回结果格式为“主机名@用户名”
 * CHARSET(value) 返回字符串value自变量的字符集
 * COLLATION(value) 返回字符串value的比较规则
 */
/**
 * FORMAT(value,n) 返回对数字value进行格式化后的结果数据。n表示 四舍五入 后保留到小数点后n位
 *          如果n《=0  那么只保留整数位
 * CONV(value,from,to) 将value的值进行不同进制之间的转换
 * INET_ATON(ipvalue) 将以点分隔的IP地址转化为一个数字
 * INET_NTOA(value) 将数字形式的IP地址转化为以点分隔的IP地址
 * BENCHMARK(n,expr) 将表达式expr重复执行n次。用于测试MySQL处理expr表达式所耗费的时间
 * CONVERT(value USING char_code) 将value所使用的字符编码修改为char_code
 */