package com.example.mysql.学习笔记.高级.事务.多版本并发控制;

public class 例子 {
}
/**
 * 5.1 READ COMMITTED(读已提交)隔离级别下
 * READ COMMITTED :每次读取数据前都生成一个ReadView。
 *
 *
 * 当某个查询事务查询一条记录的时候，会查看当前数据最新的 trx_id
 * 如果此trx_id在 ReadView(记录了对表修改的信息和记录) 中
 * 表示此事务还没提交 ，在 版本链 再往前翻  知道翻到已经提交的事务的数据
 *
 * 5.2 REPEATABLE READ（可重复读,）隔离级别下
 * 使用REPEATABLE READ隔离级别的事务来说,
 * 只会在第一次执行查询语句时生成一个ReadView,之后的查询就不会重复生成了。
 * 所以就保证 可重复读  因为在两次查询之间，虽然别的事务已经修改并提交了
 * 但是 我没有生成新的ReadView，所以在第二次的查询中，和第一次是一样的
 *
 */
/**
 * 如何解决幻读
 * 在REPEATABLE READ（可重复读）隔离级别下
 * 解决了不可重复读，他新的查询的ReadView 只在第一次生成
 * 所以幻读的新添加的数据 在第一次的 ReadView中是活跃的(实际已经提交了)，
 * 所以查不出来
 *
 */
