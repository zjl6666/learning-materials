package com.example.mysql.学习笔记.高级.数据库的优化之索引.索引的数据结构.InnoDB的索引历程;

public class B加数 {
}
/**
 *
 * InnoDB的B+树索引的注意事项
 * 1. 根页面位置万年不动
 *   我们前边介绍B+树索引的时候，为了大家理解上的方便，先把存储用户记录的叶子节点都画出来,
 *   然后接着画存储目录项记录的内节点，实际上B+树的形成过程是这样的:
 *   ●每当为某个表创建一个B+树索引 (聚簇索引不是人为创建的，默认就有)的时候，
 *   都会为这个索引创建一个根节点页面。最开始表中没有数据的时候，
 *   每个B+树索引对应的根节点中既没有用户记录，也没有目录项记录。
 *   ●随后向表中插入用户记录时，先把用户记录存储到这个根节点中。
 *   ●当根节点中的可用空间用完时继续插入记录，此时会将根节点中的所有记录复制到一个新分配的页，
 *   比如页a中，然后对这个新页进行页分裂的操作，得到另一个新页，比如页b。
 *   这时新插入的记录根据键值(也就是聚簇索引|中的主键值，二级索引中对应的索引列的值)
 *   的大小就会被分配到页a或者页b中，而根节点便升级为存储目录项记录的页。
 *   这个过程特别注意的是: 一个B+树索引的根节点自诞生之日起，便不会再移动。
 *   这样只要我们对某个表建立-个索引，那么它的根节点的页号便会被记录到某个地方，
 *   然后凡是InnoDB存储弓|擎需要用到这个索引的时候，都
 *   会从那个固定的地方取出根节点的页号，从而来访问这个索引。
 *
 *   综合来说 就是最初存放索引的  有一层
 *   当数据过多时  会复制一份原来的页面  再把添加的信息 添加在一个新的页面
 *   把原来的页面当成目录  重新填写新的信息
 *
 *
 *   3.一个页面最少存储2条记录
 *    一个B+树只需要很少的层级就可以轻松存储数亿条记录,
 *    查询速度相当不错!这是因为B+树本质上就是一个大的多层级目录，
 *    每经过一个目录时都会过滤掉许多无效的子目录，直到最后访问到存储真实数据的目录。
 *    那如果一个大的目录中只存放一个子目录是个啥效果呢?
 *    那就是目录层级非常非常非常多,而且最后的那个存放真实数据的目录中只能存放一条记录。
 *    费了半天劲只能存放一条真实的用户记录?
 *    所以InnoDB的一个数据页至少可以存放两条记录。
 *
 *
 */
