package com.example.mysql.学习笔记.高级.数据库的优化之索引.索引优化与查询优化;

public class GROUP_BY优化 {//分组
}
/**
 *6. GROUP BY优化
 * ● group by使用索引的原则几乎跟order by-致，
 *   group by即使没有过滤条件用到索引，也可以直接使用索引。
 * ● group by先排序再分组，遵照索引建的最佳左前缀法则
 * ● 当无法使用索引列，增大max_length_for_sort_data和sort_buffer_size参数的设置
 * ● where效率高于having,能写在where限定的条件就不要写在having中了
 * ● 减少使用order by,和业务沟通能不排序就不排序,或将排序放到程序端去做。
 *  Order by、group by、distinct这些语句较为耗费CPU，数据库的CPU资源是极其宝贵的。
 * ● 包含了order by、 group by、 distinct这些查询的语句
 *  where条件过滤出来的结果集请保持在1000行以内，否则SQL会很慢。.
 *
 *
 */

