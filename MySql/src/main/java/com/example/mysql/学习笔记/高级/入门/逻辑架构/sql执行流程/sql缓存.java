package com.example.mysql.学习笔记.高级.入门.逻辑架构.sql执行流程;

/**
 * MySQL8中SQL执行原理
 * 前面的结构图很复杂，我们需要抓取最核心的部分: SQL的执行原理。
 *      不同的DBMS的SQL的执行原理是相通的，只是在不同的软件中，各有各的实现路径。
 * 既然一条SQL 语句会经历不同的模块，那我们就来看下，
 * 在不同的模块中，SQL执行所使用的资源(时间) 是怎样的。
 * 如何在MySQL中对一条SQL语句的执行时间进行分析。
 *
 * 5.7有： query_cache_type=1
 * query_cache_type=0 表示关闭/OFF  1表示开启/ON
 *        2表示在sql语句有关键词（SQL_CACHE）才使用缓存
 * 8.0没有： query_cache_type
 *
 *1.确认profiling是否开启
 * 了解查询语句底层执行的过程:
 *     select @@profiling;   0表示未开启sql执行详情  1表示保存执行详情
 *     或者show variables like '%profiling%'查看是
 * 否开启计划。开启它可以让MySQL收集在SQL执行时所使用的资源情况，命令如下:
 *
 * set profiling=1;
 *
 * show profiles; //查看sql执行的历史记录
 * show profile; //查看sql执行的最近一条详细信息
 * show profile for query 数字; //查看sql执行的指定一条的详细信息
 * show profile cpu,block io for query 数字;
 * //查看sql执行的指定一条的详细信息  其他详细信息 cpu等
 *
 */
public class sql缓存 {
}
