package com.example.mysql.学习笔记.基础.函数;

public class 字符串函数 {
}
/**
 * 下标从1开始   java下标从0开始
 * 字符串函数
 * | ASCII(S)                          | 返回字符串S中的"第一个"字符的ASCII码值
 * | CHAR_LENGTH(s)                    | 返回字符串s的"字符数"。作用与CHARACTER_LENGTH(s)相同
 * | LENGTH(s)                         | 返回字符串s的"字节数"，和字符集有关
 * | CONCAT(s1,s2,......,sn)           | 连接s1,s2,......,sn为一个字符串
 * | CONCAT_WS(x, s1,s2,......,sn)     | 同CONCAT(s1,s2,...)函数，但是每个字符串之间要加上  有分隔符
 * | INSERT(str, idx, len, replacestr) | 将字符串str从第idx位置开始，len个字符长的子串替换为字符串replacestr
 *                  当idx<1  返回str     当len过长  相当与 len等于str的长度减去idx
 * | REPLACE(str, a, b)                | 用字符串b替换字符串str中所有出现的字符串a
 * | UPPER(s) 或 UCASE(s)              | 将字符串s的所有字母转成大写字母
 * | LOWER(s)  或LCASE(s)              | 将字符串s的所有字母转成小写字母
 * | LEFT(str,n)                       | 返回字符串str最左边的n个字符    n过大 全返回  0或负数返回 ''
 * | RIGHT(str,n)                      | 返回字符串str最右边的n个字符   n过大 全返回  0或负数返回 ''
 * | LPAD(str, len, pad)               | 用字符串pad对str最左边进行填充，直到str的长度为len个字符
 * | RPAD(str ,len, pad)               | 用字符串pad对str最右边进行填充，直到str的长度为len个字符
 *                        如果str长度小于len 还是返回len长度    如 RPAD/LPAD('123456', 5, *)='12345'
 * | LTRIM(s)                          | 去掉字符串s左侧的空格
 * | RTRIM(s)                          | 去掉字符串s右侧的空格
 * | TRIM(s)                           | 去掉字符串s开始与结尾的空格
 * | TRIM(s1 FROM s)                   | 去掉字符串s开始与结尾的s1  如 TRIM('oo' FROM 'oolaefalo')='laefalo'
 * | TRIM(LEADING s1 FROM s)           | 去掉字符串s"开始处"的s1
 * | TRIM(TRAILING s1 FROM s)          | 去掉字符串s"结尾处"的s1
 * | REPEAT(str, n)                    | 返回str重复n次的结果
 * | SPACE(n)                          | 返回n个空格
 * | STRCMP(s1,s2)                     | 比较字符串s1,s2的ASCII码值的大小
 * | SUBSTR(s,index,len)               | 返回从字符串s的index位置其len个字符，作用与SUBSTRING(s,n,len)、MID(s,n,len)相同 下表从1开始
 * | LOCATE(substr,str)                | 返回字符串substr在字符串str中首次出现的位置，作用于POSITION(substr IN str)、INSTR(str,substr)相同。未找到，返回0 |
 * | ELT(m,s1,s2,…,sn)                 | 返回指定位置的字符串，如果m=1，则返回s1，如果m=2，则返回s2，如果m=n，则返回sn |
 * | FIELD(s,s1,s2,…,sn)               | 返回字符串s在字符串列表中第一次出现的位置                    |
 * | FIND_IN_SET(s1,s2)                | 返回字符串s1在字符串s2中出现的位置。其中，字符串s2是一个以逗号分隔的字符串 |
 * | REVERSE(s)                        | 返回s反转后的字符串                                          |
 * | NULLIF(value1,value2)             | 比较两个字符串，如果value1与value2相等，则返回NULL，否则返回value1 |
 */

