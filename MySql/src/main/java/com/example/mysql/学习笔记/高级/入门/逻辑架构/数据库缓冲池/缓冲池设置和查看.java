package com.example.mysql.学习笔记.高级.入门.逻辑架构.数据库缓冲池;

/**
 * 如果你使用的是MySQL MyISAM存储引擎，它只缓存索引，不缓存数据
 * 对应的键缓存参数为 key_ buffer_ size ，你可以用它进行查看。
 * 如果你使用的是InnoDB存储引擎，可以通过查看innodb_buffer_pool_size 变量来查看缓冲池的大小。
 *
 * show variables like 'innodb_buffer_pool_size';//查看缓存总共大小  b  默认128MB
 * set global innodb_buffer_pool_size = 数字   //修改缓存大小
 *
 *
 * 缓冲池  当多线程使用缓冲池是  会存在安全问题 就会进行加锁  但会降低高并发
 *
 * Buffer Pool本质是InnoDB向操作系统申请的--块连续的内存空间，在多线程环境下，访问Buffer Pool中的数据都需
 * 要加锁处理。在Buffer Pool特别大而且多线程并发访问特别高的情况下，单--的Buffer Pool可能会影响请求的处
 * 理速度。所以在Buffer PooI特别大的时候，我们可以把它们拆分成若干个小的Buffer Pool ，海个Buffer Pool都称
 * 为-个实例，它们都是独立的，独立的去申请内存空间，独立的管理各种链表。所以在多线程并发访问时并不会
 * 相互影响，从而提高并发处理能力。
 * 我们可以在服务器启动的时候通过设置innodb_buffer_pool_instances 的值来修改Buffer Pool实例的个数,
 *
 *
 * show variables like 'innodb_buffer_pool_instances';查看Buffer Pool实例的个数  默认为1
 *
 * 不过也不是说Buffer Pool实例创建的越多越好，分别管理各个Buffer Pool也 是需要性能开销的,
 * InnoDB规定: 当innodb_buffer_pool_size的值小于1G的时候设置多个实例是无效的
 * InnoDB会默认把innodb_buffer_pool_instances 的值修改为1。
 * 而我们鼓励在Buffer Pool大于或等于1G的时候设置多个Buffer Pool实例。
 */
public class 缓冲池设置和查看 {
}
