package com.example.mysql.学习笔记.MYSQL8.共用表达式;

public class 普通公用表表达式 {
}
/**
 * 普通公用表表达式
 *
 * 普通公用表表达式的语法结构是：
 *
 * ```mysql
 * WITH CTE名称
 * AS （子查询）
 * SELECT|DELETE|UPDATE 语句;
 * ```
 *
 * 普通公用表表达式类似于子查询，不过，跟子查询不同的是，它可以被多次引用，
 * 而且可以被其他的普通公用表表达式所引用。
 *
 * WITH 类似于临时表表名
 * AS （子查询）
 * 之后写sql语句   连起来写，
 *
 *
 */
