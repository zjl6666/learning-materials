package com.example.mysql.学习笔记.高级.备份.其他日志;

public class Main {
}
/**
 * 我们在讲解数据库事务时，讲过两种日志: 重做日志redo日志、 回滚日志Undo日志。
 *
 * 对于线上数据库应用系统，突然遭遇数据库宕机怎么办?
 * 在这种情况下，定位宕机的原因就非常关键。
 * 我们可以查看数据库的错误日志。
 * 因为日志中记录了数据库运行中的诊断信息，包括了错误、警告和注释等信息。
 * 比如:
 * 从日志中发现某个连接中的SQL操作发生了死循环，导致内存不足,被系统强行终止了。
 * 明确了原因,处理起来也就轻松了，系统很快就恢复了运行。
 * 除了发现错误，日志在数据复制、数据恢复、操作审计，以及确保数据的永久性和一致性等方面，
 * 都有着不可替代的作用。
 * 干万不要小看日志。很多看似奇怪的问题，答案往往就藏在日志里。
 * 很多情况下，只有通过查看日志才能发现问题的原因，真正解决问题。
 * 所以，-定要学会查看日志,养成检查日志的习惯，对提升你的数据库应用开发能力至关重要。
 *
 */
