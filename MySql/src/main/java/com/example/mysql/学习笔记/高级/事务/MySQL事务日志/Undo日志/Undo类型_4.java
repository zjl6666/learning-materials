package com.example.mysql.学习笔记.高级.事务.MySQL事务日志.Undo日志;

public class Undo类型_4 {
}
/**
 * 2.4 undo的类型
 * 在InnoDB存储引擎中，undo log分为:
 * ●insert undo log
 * insert undo log是指在 insert 操作中产生的undo log。
 * 因为insert操作的记录，只对事务本身可见，对其他事务不可见(这是事务隔离性的要求)，
 * 故该undo log可以在事务提交后直接删除。不需要进行purge操作。
 * ●update undo log
 * update undo log记录的是对delete和update操作产生的undo log。
 * 该undo log可能需要提供MVCC机制，因此不能在事务提交时就进行删除。
 * 提交时放入undo log链表，等待purge线程进行最后的删除。
 *
 *
 */