package com.example.mysql.学习笔记.高级.事务.MySQL事务日志.redo日志;

public class redo_log_file_8 {
}
/**
 * redo log file（在磁盘中，日志文件）
 * 1.相关参数设置
 * ● innodb_log_group_home_dir :
 *   指定redo log文件组所在的路径,默认值为    ./
 *   表示在数据库的数据目录下。
 *   MySQL的默认数据目录( var/lib/mysql)下
 *   默认有两个名为ib_logfile0和ib_logfile1的文件,
 *   log buffer中的日志默认情况下就是刷新到这两个磁盘文件中。
 * 此redo日志文件位置还可以修改。
 *
 * ● innodb_log_files_jin_group:
 *   指明redo log file的个数,
 *   命名方式如: ib_logfile0, ib_logfile1,ib_logfile100。 默认2个，最大100个。
 *
 * ● innodb_flush_log_at_trx_commit: 控制redo log刷新到磁盘的策略，默认为1。
 *
 * ●innodb_log_file_size:
 * 单个redo log文件设置大小，默认值为48M。 最大值为512G,
 * 注意最大值指的是整个 redo log系列文件之和，
 * 即(innodb_log_files_in_group * innodb_log_file_size )不能大于最大值512G。
 *
 * 在数据库实例更新比较频繁的情况下，可以适当加大redo log组数和大小。但也不推荐redo log设置过大，
 * 在MySQL崩溃恢复时会重新执行RED0日志中的记录。
 *
 */

/**
 * 2.日志文件组
 * 从上边的描述中可以看到，磁盘上的redo日志文件不只一个，而是以一个日志文件组的形式出现的。
 * 这些文件以ib_logfile[数字]
 * ( 数字可以是0、1、2... 的形式进行命名,每个的redo日志文件大小都是一样的。
 * 在将redo日志写入日志文件组时，是从ib_logfile0开始写,如果1ib_logfile0写满了，
 * 就接着ib_logfile1写。
 * 同理，ib_logfile1写满了就去写ib_logfile2,依此类推。
 * 如果写到最后一个文件该咋办?
 * 那就重新转到ib_logfile0继续写，所以整个过程如下图所示:
 * ib_logfile0->ib_logfile1->ib_logfile2 ---- ib_logfile n->ib_logfile0
 * 循环往复
 *
 *
 *
 */
/**
 * 3. checkpoint
 * ● write pos 是当前记录的位置，一边写一边后移
 * ● checkpoint 是当前要擦除的位置，也是往后推移
 * 每次刷盘redo log记录到日志文件组中，write pos位置就会后移更新。
 * 每次MySQL加载日志文件组恢复数据时，会清空加载过的redo log记录，
 * 并把checkpoint后移更新。write pos和checkpoint之间的还空着的部分可以
 * 用来写入新的redo log记录。
 *
 *
 */
