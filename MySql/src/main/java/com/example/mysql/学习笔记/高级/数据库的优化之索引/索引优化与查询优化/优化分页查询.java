package com.example.mysql.学习笔记.高级.数据库的优化之索引.索引优化与查询优化;

public class 优化分页查询 {
}

/**
 * 7.优化分页查询
 * 一般分页查询时，通过创建覆盖索引能够比较好地提高性能。
 * 一个常见又非常头疼的问题就是limit 200000,10，
 * 此时需要MySQL排序前2000010记录,仅仅返回200000 - 2000010的记录,其他记录丢弃，
 * 查询排序的代价非常大。
 *
 *  *  在索引上完成排序分页操作,最后根据主键关联回原表查询所需要的其他列内容。
 *  * EXPLAIN SELECT * FROM student  t, (SELECT id FROM student ORDER BY id LIMIT 2000000,10) a
 *  * WHERE t.id = a.id;
 *
 *  如果是自增主键  可以
 *  EXPLAIN SELECT * FROM student WHERE id > 2000000 LIMIT 10;
 *
 *
 */
