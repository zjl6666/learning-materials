package com.example.mysql.学习笔记.基础.变量_流程控制_游标.变量;

public class MySql8 {
}
/**
 * MySQL 8.0的新特性—全局变量的持久化
 *
 * 在MySQL数据库中，全局变量可以通过SET GLOBAL语句来设置。
 * 例如，设置服务器语句超时的限制，可以通过设置系统变量max_execution_time来实现：
 *
 * SET GLOBAL MAX_EXECUTION_TIME=2000;//重启失效
 *
 *
 * 使用SET GLOBAL语句设置的变量值只会`临时生效`。
 * `数据库重启`后，服务器又会从MySQL配置文件中读取变量的默认值。
 * MySQL 8.0版本新增了`SET PERSIST`命令。
 * 例如，设置服务器的最大连接数为1000：
 *
 * SET PERSIST global max_connections = 1000;//文件修改
 *
 * MySQL会将该命令的配置保存到数据目录下的`mysqld-auto.cnf`文件中，
 * 下次启动时会读取该文件，用其中的配置来覆盖默认的配置文件。
 *
 * 举例：
 *
 * 查看全局变量max_connections的值，结果如下：
 *
 * ```mysql
 * mysql> show variables like '%max_connections%';
 * +------------------------+-------+
 * | Variable_name          | Value |
 * +------------------------+-------+
 * | max_connections        | 151   |
 * | mysqlx_max_connections | 100   |
 * +------------------------+-------+
 * 2 rows in set, 1 warning (0.00 sec)
 * ```
 *
 * 设置全局变量max_connections的值：
 *
 * ```mysql
 * mysql> set persist max_connections=1000;
 * Query OK, 0 rows affected (0.00 sec)
 * ```
 *
 * `重启MySQL服务器`，再次查询max_connections的值：
 *
 * ```mysql
 * mysql> show variables like '%max_connections%';
 * +------------------------+-------+
 * | Variable_name          | Value |
 * +------------------------+-------+
 * | max_connections        | 1000  |
 * | mysqlx_max_connections | 100   |
 * +------------------------+-------+
 * 2 rows in set, 1 warning (0.00 sec)
 * ```
 */
