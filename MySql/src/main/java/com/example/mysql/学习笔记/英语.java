package com.example.mysql.学习笔记;

public class 英语 {
}
/**
 *
 *
 * TEMPORARY    临时的
 * DISTINCT     不同的
 *
 * DROP          减少   在sql中  一般用于删除函数 删除表等操作
 * ALTER         改变
 * SET           设置
 * CREATE        创造
 * BEGIN         开始
 * END           结束
 * DELIMITER     分隔符
 * PROCEDURE     程序
 * FUNCTION      函数
 * CALL          呼叫  在Mysql是调用自定义函数的
 * DETERMINISTIC 确定性
 *
 * GLOBAL        全球的
 * SESSION       会话
 * DECLARE       声明
 * CONDITION     条件
 *
 * TRIGGER       触发
 * BEFORE        在...之前
 * AFTER         在...之后
 * FOR EACH ROW  每行      (对于  每个  一行)
 *
 * PARTITION      隔断
 * RECURSIVE      递归的
 *
 *
 */
