package com.example.mysql.学习笔记.高级.事务.锁;

public class 锁监控 {
}
/**
 *5.锁监控
 * 关于MySQL锁的监控，我们一般可以通过检查InnoDB_row_lock
 * 等状态变量来分析系统上的行锁的争夺情况
 *  show status like 'innodb_row_1ock%';
 *
 *     Innodb_row_Lock_current_waits     当前正在等待锁的数量
 *     Innodb_row_Lock_time        从系统启动到现在锁定总时间长度; ( 等待总时长)
 *     Innodb_row_Lock_time_avg  每次等待所花平均时间; (等待平均时长)
 *     Innodb_row_Lock_time_max  从系统启动到现在等待最长的一次所花的时间;
 *     Innodb_row_Lock_waits   系统启动后到现在总共等待的次数; (等待总次数)
 *
 *
 *
 */