package com.example.mysql.学习笔记.基础.查询;

public class 多表查询各种连接 {
}
/**
 *select * from 表名1，表名2
 *
 * //返回的总数 是表名1数量*表名2数量   他叫  笛卡尔积错误
 *
 * select * from 表名1 别名1，表名2
 * where 别名1.字段 = 表名2.字段
 * //可进行不同表的匹配
 * 如果两张表  有相同的字段  必须指明是那个表
 * 如果给表起了别名   在其他地方必须使用别名
 *
 */

/**
 * 角度1:等值连接  vs 非等值连接
 *      SELECT e.last_name,e.salary,j.grade_level
 *      FROM employees e,job_grades j
 *      #where e.salary between j.lowest_sal and j.highest_sal;
 *      WHERE e.salary >= j.lowest_sal AND e.salary <= j.highest_sal;
 * 角度2:自连接    vs  非自连接    查询员工id,员工姓名及其管理者的id和姓名
 *      SELECT emp.employee_id,emp.last_name,mgr.employee_id,mgr.last_name
 *      FROM employees emp ,employees mgr
 *      WHERE emp.manager_id = mgr.employee_id;
 * 角度3:内连接   vs   外连接
 * # 内连接：合并具有同一列的两个以上的表的行, 结果集中不包含一个表与另一个表不匹配的行
 * # 外连接：合并具有同一列的两个以上的表的行, 结果集中除了包含一个表与另一个表匹配的行之外，
 *         还查询到了左表 或 右表中不匹配的行。
 * # 外连接的分类：左外连接、右外连接、满外连接
 * # 左外连接：两个表在连接过程中除了返回满足连接条件的行以外还返回左表中不满足条件的行，这种连接称为左外连接。
 * # 右外连接：两个表在连接过程中除了返回满足连接条件的行以外还返回右表中不满足条件的行，这种连接称为右外连接。
 * 左外连接  查询"所有"的员工的last_name,department_name信
 *
 * #SQL92语法实现外连接：使用 +  ----------MySQL不支持SQL92语法中外连接的写法！
 * SELECT employee_id,department_name
 * FROM employees e,departments d
 * WHERE e.department_id = d.department_id(+);//可以查出所有的  即便不匹配也会显示 给谁加谁全部显示
 *
 * #SQL99语法中使用 JOIN ...ON 的方式实现多表的查询。这种方式也能解决外连接的问题。MySQL是支持此种方式的。
 * #SQL99语法如何实现多表的查询。
 *
 * #SQL99语法实现内连接： INNER（可省略） JOIN 连接表    ON:后面加连接条件
 * SELECT 字段1，字段2....
 * FROM 表名1
 * INNER JOIN 表名2  ON 连接条件
 * INNER JOIN 表名2 ON 连接条件;//可以一直加
 *
 *  //外连接  LEFT OUTER  左外    RIGHT OUTER   右外    OUTER（可省略）
 *  SELECT 字段1，字段2....
 *  FROM 表名1
 *  LEFT OUTER JOIN 表名2  ON 连接条件
 *  RIGHT OUTER JOIN 表名3 ON 连接条件;//可以一直加
 *
 * //满外链接  mysql不支持 FULL  满外
 *  SELECT 字段1，字段2....
 *  FROM 表名1
 *  FULL OUTER JOIN 表名2  ON 连接条件;//mysql不支持 FULL  满外
 *
 *  mysql咋办 ？
 *  UNION     //UNION操作符返回两个查询的结果集的并集，去除重复记录。
 *  UNION ALL //UNION ALL操作符返回两个查询的结果集的并集。对于两个结果集的重复部分，不去重。
 *   在sql语句中  能用 UNION ALL 就用   少用UNION 因为UNION会进行去重  效率较低
 *
 *  SELECT 字段1，字段2....
 *  FROM 表名1
 *  LEFT/RIGHT OUTER JOIN 表名2  ON 连接条件;
 *  where 条件 某个条件可以为空的事
 *
 *  sql语句1  UNION ALL sql语句2  //强行拼接 形成满外链接
 *
 * SQL99 新特性
 *   自然连接
 *      NATURAL JOIN 用来表示自然连接。我们可以把自然连接理解为SQL92中的等值连接。
 *      它会帮你自动查询两张连接表中所有相同的字段，然后进行等值连接。
 *   SELECT 字段1，字段2....
 *   FROM 表名1 NATURAL JOIN 表名2
 *
 *   USING
 *     SELECT 字段1，字段2....
 *     FROM 表名1 JOIN 表名2  USING(表名1和表名2字段名字相同的字段);
 *
 *
 */
