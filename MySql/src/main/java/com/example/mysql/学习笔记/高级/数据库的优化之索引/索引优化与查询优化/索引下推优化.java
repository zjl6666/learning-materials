package com.example.mysql.学习笔记.高级.数据库的优化之索引.索引优化与查询优化;

public class 索引下推优化 {
    public static void main(String[] args) {
    }
}
/**
 * 10.索引下推   ICP  仅适用于二级索引(非聚簇索引)  因为主键索引(聚簇索引) 他有完整信息  不需要回表
 *  一般用于联合索引  它会先把一个符合索引的都先条件判断，筛选后在进行回表查询  这个过程叫做索引下推
 * 10.1使用前后对比
 * Index Condition Pushdown(ICP)是MySQL 5.6中新特性，是一种在存储引擎层使用索引过滤数据的优化方式。
 * ●如果没有ICP，存储引擎会遍历索引以定位基表中的行，并将它们返回给MySQL服务器，
 * 由MySQL服务器评估WHERE后面的条件是否保留行。
 * ●启用ICP后，如果部分WHERE条件可以仅使用索引中的列进行筛选，
 * 则MySQL服务器会把这部分WHERE条件放到存储引擎筛选。
 * 然后，存储引擎通过使用索引条目来筛选数据，
 * 并且只有在满足这一条件时才从表中读取行。
 *
 * 。好处: ICP可以减少存储引擎必须访问基表的次数和MySQL服务器必须访问存储引擎的次数。
 * 。但是，ICP的加速效果取决于在存储引擎内通过ICP筛选掉的数据的比例。
 *
 * 简单来说就是，在索引(一般是联合索引) 所需要的字段都是此索引有的(主键和索引字段) 且没有 LIMIT
 * 那么他会把where后面的所有条件都判断后，才确定要不要，不需要，一个一个判断
 *
 * 10.2 ICP的开启/关闭    默认开启
 * 默认情况下启用索引条件下推。可以通过设置系统变量optimizer_switch 控制:
 * index_condition_pushdown
 * #关闭索引下推
 * SET optimizer_switch = 'index_condition_pushdown=off' ;
 * #打开索引下推
 * SET optimizer_switch = ' index_condition_pushdown=on' ;
 * ●当使用索引条件下推时，EXPLAIN 语句输出结果中Extra列内容显示为Using index condition.
 *
 * 10.5 ICP的使用条件
 * 1.如果表访问的类型为range_ref、 eq_ref和ref_or_null 可以使用ICP
 * 2. ICP可以用于 InnoDB和MyISAM表,包括分区表InnoDB和MyISAM表
 * 3.对于InnoDB表, ICP 仅用于二级索引。ICP的目标是减少全行读取次数,从而减少1/0操作。
 * 4. 当SQL使用覆盖索引时,不支持ICP。因为这种情况下使用ICP不会减少1/0。
 * 5.相关子查询的条件不能使用ICP
 *
 *
 *
 */
