package com.example.mysql.学习笔记.基础.变量_流程控制_游标.流程控制;

public class 循环结构 {
}
/**
 *  循环结构之LOOP
 *
 * LOOP循环语句用来重复执行某些语句。
 * LOOP内的语句一直重复执行直到循环被退出（使用LEAVE子句），
 * 跳出循环过程。
 *
 * LOOP语句的基本格式如下：
 *
 * [loop_label:] LOOP
 * 	循环执行的语句
 * END LOOP [loop_label]
 *
 * 其中，loop_label表示LOOP语句的标注名称，该参数可以省略。
 *
 * 名字:LOOP
 *
 *          IF 条件 THEN LEAVE 名字;//根据名字跳出循环
 *      ELSEIF 条件 THEN LEAVE 名字;//跳出循环
 *      ELSE 条件;
 *      END IF
 *
 * END LOOP 名字
 *
 *
 */

/**
 * 循环结构之WHILE
 *
 * WHILE语句创建一个带条件判断的循环过程。
 * WHILE在执行语句执行时，先对指定的表达式进行判断，
 * 如果为真，就执行循环内的语句，否则退出循环。
 *
 * WHILE语句的基本格式如下：
 *
 * [while_label:] WHILE 循环条件  DO
 * 	循环体;
 * END WHILE [while_label];
 *
 * while_label为WHILE语句的标注名称；[while_label:] 可省略
 * 如果循环条件结果为真，WHILE语句内的语句或语句群被执行，
 * 直至循环条件为假，退出循环。
 *
 * WHILE 循环条件  DO
 * 循环体;
 * END WHILE;
 *
 *
 *
 */

/**
 * 循环结构之REPEAT
 *
 * REPEAT语句创建一个带条件判断的循环过程。
 * 与WHILE循环不同的是，REPEAT 循环首先会执行一次循环，
 * 然后在 UNTIL 中进行表达式的判断，
 * 如果满足条件就退出，即 END REPEAT；
 * 如果条件不满足，则会就继续执行循环，直到满足退出条件为止。
 *
 * REPEAT语句的基本格式如下：
 *
 * [repeat_label:] REPEAT
 * 　　　　循环体的语句;//有分号
 * UNTIL 结束循环的条件表达式//无分号
 * END REPEAT [repeat_label];
 *
 * repeat_label为REPEAT语句的标注名称，该参数可以省略；
 * REPEAT语句内的语句或语句群被重复，直至expr_condition为真。
 *
 * REPEAT
 * 　　　　循环体的语句;
 * UNTIL 结束循环的条件表达式       false继续循环 即不满足  继续循环
 * END REPEAT;
 *
 */

/**
 * **对比三种循环结构：**
 *
 * 1、这三种循环都可以省略名称，
 * 但如果循环中添加了循环控制语句（LEAVE或ITERATE）则必须添加名称。
 *
 * 2、  LOOP：一般用于实现简单的"死"循环
 *      WHILE：先判断后执行
 *      REPEAT：先执行后判断，无条件至少执行一次
 */

