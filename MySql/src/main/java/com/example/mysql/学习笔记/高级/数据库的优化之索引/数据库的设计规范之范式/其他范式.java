package com.example.mysql.学习笔记.高级.数据库的优化之索引.数据库的设计规范之范式;

public class 其他范式 {
}
/**
 * 5.第四范式
 * 多值依赖的概念:
 * ●多值依赖即属性之间的一对多关系,记为K-→A。
 * ●函数依赖事实.上是单值依赖，所以不能表达属性值之间的一对多关系。
 * ●平凡的多值依赖:全集U=K+A,一个k可以对应于多个A,即K-→-→A。 此时整个表就是一组一对多关系。
 * ●非平凡的多值依赖:全集U=K+A+B, 一个k可以对应于多个A,也可以对应于多个B, A与B互相独立,即
 * K→→A, K→→B。整个表有多组一对多关系，
 * 且有:“一 ”部分是相同的属性集合，“多”部分是互相独立的属性集合。
 * 第四范式即在满足巴斯科德范式(BCNF) 的基础上，
 * 消除非平凡且非函数依赖的多值依赖(即把同-表内的多对多关系删除)
 *
 * 举例:职工表(职工编号,职工孩子姓名，职工选修课程)。
 * 在这个表中，同一个职工可能会有多个职工孩子姓名。
 *      同样，同一个职工也可能会有多个职工选修课程，
 *      即这里存在着多值事实，不符合第四范式。
 * 如果要符合第四范式，只需要将上表分为两个表,使它们只有一一个多值事实,
 * 例如:职工表一 (职工编号， 职工孩子姓名)，
 *     职工表二 (职工编号，职工选修课程)，
 *     两个表都只有一个多值事实，所以符合第四范式。
 *
 *
 */
/**
 * 6.第五范式、域键范式
 * 除了第四范式外,我们还有更高级的第五范式(又称完美范式)和域键范式(DKNF) 。
 * 在满足第四范式(4NF) 的基础上，消除不是由候选键所蕴含的连接依赖。
 * 如果关系模式R中的每一个连接依赖均由R的候选键所隐含，则称此关系模式符合第五范式。
 * 函数依赖是多值依赖的一种特殊的情况，而多值依赖实际上是连接依赖的一种特殊情况。
 * 但连接依赖不像函数依赖和多值依赖可以由语义直接导出，而是在关系连接运算时才反映出来。
 * 存在连接依赖的关系模式仍可能遇到数据冗余及插入、修改、删除异常等问题。
 *
 * 第五范式处理的是无损连接问题，这个范式基本没有实际意义，因为无损连接很少出现，而且难以察觉。
 * 而域键范式试图定义--个终极范式，该范式考虑所有的依赖和约束类型，
 * 但是实用价值也是最小的，只存在理论研究中。
 *
 *
 */
