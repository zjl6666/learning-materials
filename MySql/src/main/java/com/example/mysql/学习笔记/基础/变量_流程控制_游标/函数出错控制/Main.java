package com.example.mysql.学习笔记.基础.变量_流程控制_游标.函数出错控制;

public class Main {
}
/**
 * 类似try catch
 * 一般用于存储函数与存储过程的 begin end之间
 *
 * 定义条件
 * 定义条件就是给MySQL中的错误码命名，这有助于存储的程序代码更清晰。
 *      它将一个`错误名字`和`指定的错误条件`关联起来。
 *      这个名字可以随后被用在定义处理程序的`DECLARE HANDLER`语句中。
 *
 * 定义条件使用DECLARE语句，语法格式如下： 给编码错误弄个通俗易懂的名字
 *
 * DECLARE 错误名称 CONDITION FOR 错误码（或错误条件）
 *
 * 错误码的说明：
 * - `MySQL_error_code`和`sqlstate_value`都可以表示MySQL的错误。
 *   - MySQL_error_code是数值类型错误代码。
 *   - sqlstate_value是长度为5的字符串类型错误代码。
 * - 例如，在ERROR 1418 (HY000)中，1418是MySQL_error_code，'HY000'是sqlstate_value。
 * - 例如，在ERROR 1142（42000）中，1142是MySQL_error_code，'42000'是sqlstate_value。
 *
 */

/**
 * 定义处理程序
 *
 * 可以为SQL执行过程中发生的某种类型的错误定义特殊的处理程序。
 * 定义处理程序时，使用DECLARE语句的语法如下：
 *
 * DECLARE 处理方式 HANDLER FOR 错误类型/错误名称 处理语句
 *
 * - **处理方式**：处理方式有3个取值：CONTINUE、EXIT、UNDO。
 *   - `CONTINUE`：表示遇到错误不处理，继续执行。
 *   - `EXIT`：表示遇到错误马上退出。
 *   - `UNDO`：表示遇到错误后撤回之前的操作。  MySQL中暂时不支持这样的操作。
 * - **错误类型**（即条件）可以有如下取值：
 *   - `SQLSTATE '字符串错误码'`：表示长度为5的sqlstate_value类型的错误代码；
 *   - `MySQL_error_code`：匹配数值类型错误代码；
 *   - `错误名称`：表示DECLARE ... CONDITION定义的错误条件名称。
 *
 *   - `SQLWARNING`：匹配所有以01开头的SQLSTATE错误代码；
 *   - `NOT FOUND`：匹配所有以02开头的SQLSTATE错误代码；
 *   - `SQLEXCEPTION`：匹配所有没有被SQLWARNING或NOT FOUND捕获的SQLSTATE错误代码；
 *
 * - **处理语句**：如果出现上述条件之一，则采用对应的处理方式，并执行指定的处理语句。
 *          语句可以是像“`SET 变量 = 值`”这样的简单语句，
 *          也可以是使用`BEGIN ... END`编写的复合语句。
 *
 * 在函数开头写处理程序
 *
 * DELIMITER $            //设置$为结束符号  $可变
 * CREATE FUNCTION/PROCEDURE 函数名(参数名 参数类型,...)
 * RETURNS 返回值类型
 *     //特性约束  可有可无
 *     DETERMINISTIC       //确定的
 *     CONTAINS SQL       //包含SQL语句
 *     READS SQL DATA      //当前存储过程的子程序中包含读数据的SQL语句
 * BEGIN
 *      DECLARE 处理方式 HANDLER FOR 错误类型 处理语句；//异常处理  要先写处理方式
 *
 *    //函数体   #函数体中肯定有 RETURN 语句 如
 *     RETURN (SELECT 返回的字段 FROM 表名 WHERE 条件，一般会用到参数);
 * END $
 * DELIMITER ;         //修改回来
 *
 */
