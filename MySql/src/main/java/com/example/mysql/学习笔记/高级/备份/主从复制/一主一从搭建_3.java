package com.example.mysql.学习笔记.高级.备份.主从复制;

public class 一主一从搭建_3 {
}
/**
 * 3.一主一从搭建
 * -台主机用于处理所有写请求，-台从机负责所有读请求
 * Java程序-》Mycat(中间件) -》分配给主服务器还是从服务器
 *
 * 3.1准备工作
 * 1、准备2台CentOS虚拟机
 * 2、每台虚拟机上需要安装好 MySQL (可以是MySQL8.0)
 *
 * 说明:前面我们讲过如何克隆一台CentOS.
 * 大家可以在一台CentOS 上安装好MySQL,进而通过克隆的方式复制出
 * 1台包含MySQL的虚拟机。
 * 注意:克隆的方式需要修改新克隆出来主机的:
 *  ①MAC地址②hostname ③IP地址④UUID。
 * 此外，克隆的方式生成的虚拟机(包含MySQL Server) ，
 * 则克隆的虚拟机MySQL Server的UUID相同，必须修改,
 * 否则在有些场景会报错。比如: show slave status\G ，报如下的错误:
 * Last. I0_ Error: Fatal error: The slave I/0 thread stops because master and slave have equal
 * MySQL server UUIDs; these UUIDs must be different for replication to work.
 *
 * 修改MySQL Server的UUID方式:
 * vim /var/lib/mysql/auto.cnf
 * systemctl restart mysqld
 *
 *
 */
/**
 *3.2主机配置文件
 * 建议mysql版本一致且后台以服务运行，
 * 主从所有配置项都配置在[mysqld]节点下，且都是小写字母。
 *具体参数配置如下:
 * ●必选
 * #[必须]主服务器唯一ID   server-id=1
 * #[必须]启用二进制日志，指名路径。比如:自已本地的路径/log/mysqlbin
 *        log-bin=名字-bin
 * ●可选
 * #[可选]0(默认)表示读写(主机)，1表示只读(从机)
 *      read-only=0
 * #设置日志文件保留的时长，单位是秒
 *     binlog_expire_logs_seconds=6000
 * #控制单个二进制日志大小。此参数的最大和默认值是1GB
 *      max_binlog_size=200M
 * #[可选]设置不要复制的数据库
 *    binlog-ignore-db=test
 * #[可选]设置需要复制的数据库,默认全部记录。比如: binlog- do-db-atguigu. master. slave
 *    binlogEdo-db=需要复制的主数据库名字
 * #[可选]设置binlog格式
 * binlog_ format-STATEMENT
 * 重启后台mysq|服务，使配置生效。
 * 注意:
 * 先搭建完主从复制，再创建数据库。
 * MySQL主从复制起始时，从机不继承主机数据。
 *
 *
 */
/**
 * 3.3从机配置文件
 * 要求主从所有配置项都配置在my.cnf的[mysqld]栏位下，且都是小写字母。
 * ●必选  #[必须]从服务器唯一-ID
 *   server-id=2
 * ●可选   #[可选]启用中继日志
 * relay-log=mysql-relay
 * 重启后台mysql服务,使配置生效。
 * 注意:主从机都关闭防火墙
 * service iptables stop #CentOS 6
 * systemctl stop firewalld.service #CentOS 7
 *
 *
 */
/**
 *3.4主机:建立账户并授权
 * #在主机MySQL里执行授权主从复制的命令
 * GRANT REPLICATION SLAVE ON *.* TO 'slave1'@'从机器数据库IP’IDENTIFIED BY '密码'; #5.5,5.7
 * 注意:如果使用的是MySQL8,需要如下的方式建立账户，并授权slave:
 * CREATE USER 'slave1'@'%' IDENTIFIED BY '123456';
 * GRANT REPLICATION SLAVE ON *.* TO' slave1'@'%' ;
 * #此语句必须执行。否则见下面。
 * ALTER USER 'slave1'@'%' IDENTIFIED WITH mysql_native. password BY '123456';
 * flush privileges;
 *
 * show master status ;//查看信息   后面用到
 *
 */
/**
 * 3.5从机:配置需要复制的主机
 * 步骤1:从机上复制主机的命令
 * CHANGE MASTER T0
 * MASTER_ HOST='主机的IP地址’，
 * MASTER_ USER=' 主机用户名’，
 * MASTER_ PASSWORD=' 主机用户 名的密码’，
 * MASTER_ LOG_ FILE= ' mysql-bin.具体数字'，
 * MASTER_ LOG_ POS=具体值;
 */