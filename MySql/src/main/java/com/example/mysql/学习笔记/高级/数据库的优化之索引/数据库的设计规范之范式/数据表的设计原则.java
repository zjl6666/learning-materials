package com.example.mysql.学习笔记.高级.数据库的优化之索引.数据库的设计规范之范式;

public class 数据表的设计原则 {
}
/**
 * 9.数据表的设计原则
 * 综合以上内容.总结出数据表设计的一般原则:“三少一多"
 * 1.数据表的个数越少越好
 *   RDBMS的核心在于对实体和联系的定义,也就是E-R图(Entity Relationship Diagram)，
 *   数据表越少,证明实体和联系设计得越简洁，既方便理解又方便操作。
 * 2.数据表中的字段个数越少越好
 *   字段个数越多，数据冗余的可能性越大。
 *   设置字段个数少的前提是各个字段相互独立,而不是某个字段的取值可以由其他字段计算出来。
 *   当然字段个数少是相对的,我们通常会在数据冗余和检索效率中进行平衡。
 * 3.数据表中联合主键的字段个数越少越好
 *   设置主键是为了确定唯一性,当-个字段无法确定唯--性的时候，
 *   就需要采用联合主键的方式(也就是用多个字段来定义一个主键)。
 *   联合主键中的字段越多，占用的索引空间越大，
 *   不仅会加大理解难度,还会增加运行时间和索引空间，因此联合主键的字段个数越少越好。
 * 4.使用主键和外键越多越好(这里的外键指的是这种约束，不是是传统上的外键)
 *   数据库的设计实际上就是定义各种表，以及各种字段之间的关系。
 *   这些关系越多，证明这些实体之间的冗余度越低，利用度越高。
 *   这样做的好处在于不仅保证了数据表之间的独立性，还能提升相互之间的关联使用率。
 *
 * “三少一多”原则的核心就是简单可复用。
 * 简单指的是用更少的表、更少的字段、更少的联合主键字段来完成数据表的设计。
 * 可复用则是通过主键、外键的使用来增强数据表之间的复用率。
 * 因为一个主键可以理解是一张表的代表。键设计得越多，证明它们之间的利用率越高。
 * 注意:这个原则并不是绝对的，有时候我们需要牺牲数据的冗余度来换取数据处理的效率。
 *
 */