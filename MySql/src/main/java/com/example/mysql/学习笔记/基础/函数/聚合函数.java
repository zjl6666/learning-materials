package com.example.mysql.学习笔记.基础.函数;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

public class 聚合函数 {
    public int[] findingUsersActiveMinutes(int[][] logs, int k) {
        int[] answer = new int[k];
        HashMap<Integer,HashSet<Integer>> m = new HashMap<>();
        for (int i = 0; i < logs.length; i++) {
            if(m.containsKey(logs[i][0])){
                m.get(logs[i][0]).add(logs[i][1]);
            }else {
                HashSet<Integer> set = new HashSet<>();
                set.add(logs[i][1]);
                m.put(logs[i][0],set);
            }
        }
        for (Map.Entry<Integer,HashSet<Integer>> set1 :m.entrySet()){
            answer[set1.getValue().size()-1]++;
        }
        return answer;

    }
    public int[] cycleLengthQueries(int n, int[][] queries) {
        int[] ns = new int[n];
        for(int i=0;i<queries.length;i++){
            int max = Math.max(queries[i][0],queries[i][1]);
            int min = Math.min(queries[i][0],queries[i][1]);
            int c = 2;
            do{
                c++;
                max/=2;
                if(max<min){
                    int k = max;
                    max=min;
                    min=k;
                }
            }while (max!=min);
            ns[i]=c-1;
        }
        return ns;
    }
    public long countGood(int[] nums, int k) {
        int len = nums.length;
        if(len==1){
            return 0;
        }
        long m = 0L;
        int kk = 0;
        int begin = 0;
        HashMap<Integer,Integer> map = new HashMap<>();
        map.put(nums[0],1);
        for (int i = 1; i < len; i++) {
            if(map.containsKey(nums[i])){
                int mm = map.get(nums[i]);
                kk+=mm;
                map.put(nums[i],mm+1);
            }else {
                map.put(nums[i],1);
            }
            while (kk>k){


                m+=(len-i);
                int mm = map.get(nums[begin])-1;
                map.put(nums[begin],mm);
                begin++;
                kk-=mm;
            }
        }
        for (int i = begin; i < len; i++) {
            if(map.get(nums[i])==1){
                m++;
            }else {
                break;
            }
        }
        return m;
    }

    public int waysToMakeFair(int[] nums) {
        int m = nums.length;
        int j = 0;
        int o = 0;
        for (int i = 0; i < m; i++) {
            if((i&1)==1){
                j+=nums[i];
            }else {
                o+=nums[i];
            }
        }
        int k = 0;
        int s = 0;
        for (int i = 0; i < m; i++) {
            if((i&1)==1){//1
                j=j-nums[i]+s;
            }else {//0
                o=o-nums[i]+s;
            }
            if(j==o){
                k++;
            }
            s=nums[i];
        }
        return k;
    }
}
/**
 * 聚合函数
 * 都会自动过滤空值
 *
 * AVG() 平均 (空值不在计算中)   如果计算字符串mysql直接返回0 其他报错
 *      AVG()计算的是不是空值的平均值
 * SUM()  和  (空值不在计算中)
 * 只适用于数值类型
 *
 * MAX()  最大值
 * MIN()  最小值
 * 可以进行字符串比较
 *
 * GROUP_CONCAT(CONCAT(name,"(",salary,")") SEPARATOR ';')
 * 将利用name(salary)进行连接，其中分隔符为';'
 *
 *
 * COUNT() 计数
 * 计算指定字段结构中"不为空"的"个数"
 *
 * 想要计算一个表一共有多少数据
 * COUNT(*)
 * COUNT(1)
 * COUNT(没用空的字段)
 * 那到底哪个效率高呢？
 * MylSAM存储引擎//5.7及一下   效率相等
 * 如果是MySQL MyISAM存储引擎,统计数据表的行数只需要0(1)的复杂度，
 * 这是因为每张MylSAM的数据表都有个meta信息存储了row-count值,而-致性则由表级锁来保证。
 *
 * mysql 8.0用的是InnoDB存储引擎
 * COUNT(*)=COUNT(1)>COUNT(没用空的字段)
 * 因为InnoDB支持事务,采用行级锁和MVCC机制，所以无法像MylSAM-样，
 * 只维护一个row_c ount变量,因此需要采用扫描全表，进行循环+计数的方式
 * 来完成统计。
 *
 *
 * 方差、标准差、中位数
 */