package com.example.mysql.学习笔记.高级.事务.MySQL事务日志.redo日志;

public class redo组成_3 {
}
/**
 * 1.3 redo的组成
 * Redo log可以简单分为以下两个部分:
 * ●重做日志的缓冲(redo log buffer) ，保存在内存中，是易失的。
 *  在服务器启动时就向操作系统申请了一大片称之为redo log buffer的连续内存空间，
 *  翻译成中文就是redo日志缓冲区。
 *  这片内存空间被划分成若干个连续的 redo log block。
 *  一个redo log block占用512字节大小。
 *
 * 参数设置: innodb_log_buffer_size: .
 * redo log buffer大小，默认16M ,
 * 最大值是4096M，最小值为1M。
 *
 * redo log buffer = n个redo log block
 *
 * 重做日志文件(redo 1og file)，保存在硬盘中,是持久的。
 * REDO日志文件如图所示，其中的ib_logfile0 和ib_logfile1即为REDO日志。
 *
 *
 */
