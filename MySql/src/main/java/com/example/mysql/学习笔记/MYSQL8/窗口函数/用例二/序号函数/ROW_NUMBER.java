package com.example.mysql.学习笔记.MYSQL8.窗口函数.用例二.序号函数;

public class ROW_NUMBER {
}
/**
 * 序号函数
 *
 * **1．ROW_NUMBER()函数**   重复还会牌序 1，2，3
 *
 * ROW_NUMBER()函数能够对数据中的序号进行顺序显示。
 *
 * 举例：查询 goods 数据表中每个商品分类下价格降序排列的各个商品信息。
 *
 * ```mysql
 * mysql> SELECT ROW_NUMBER() OVER(PARTITION BY category_id ORDER BY price DESC) AS row_num,
 *     -> id, category_id, category, NAME, price, stock
 *     -> FROM goods;
 * +---------+----+-------------+---------------+------------+---------+-------+
 * | row_num | id | category_id | category      | NAME       | price   | stock |
 * +---------+----+-------------+---------------+------------+---------+-------+
 * |       1 |  6 |           1 | 女装/女士精品   | 呢绒外套    |  399.90 |  1200 |
 * |       2 |  3 |           1 | 女装/女士精品   | 卫衣        |   89.90 |  1500 |
 * |       3 |  4 |           1 | 女装/女士精品   | 牛仔裤      |   89.90 |  3500 |
 * |       4 |  2 |           1 | 女装/女士精品   | 连衣裙      |   79.90 |  2500 |
 * |       5 |  1 |           1 | 女装/女士精品   | T恤        |   39.90 |  1000 |
 * |       6 |  5 |           1 | 女装/女士精品   | 百褶裙      |   29.90 |   500 |
 * |       1 |  8 |           2 | 户外运动       | 山地自行车   | 1399.90 |  2500 |
 * |       2 | 11 |           2 | 户外运动       | 运动外套     |  799.90 |   500 |
 * |       3 | 12 |           2 | 户外运动       | 滑板        |  499.90 |  1200 |
 * |       4 |  7 |           2 | 户外运动       | 自行车      |  399.90 |  1000 |
 * |       5 | 10 |           2 | 户外运动       | 骑行装备    |  399.90 |  3500 |
 * |       6 |  9 |           2 | 户外运动       | 登山杖      |   59.90 |  1500 |
 * +---------+----+-------------+---------------+------------+---------+-------+
 *
 * 举例：查询 goods 数据表中每个商品分类下价格最高的3种商品信息。
 *
 * ```mysql
 * mysql> SELECT *
 *     -> FROM (
 *     ->  SELECT ROW_NUMBER() OVER(PARTITION BY category_id ORDER BY price DESC) AS row_num,
 *     ->  id, category_id, category, NAME, price, stock
 *     ->  FROM goods) t
 *     -> WHERE row_num <= 3;
 * +---------+----+-------------+---------------+------------+---------+-------+
 * | row_num | id | category_id | category      | NAME       | price   | stock |
 * +---------+----+-------------+---------------+------------+---------+-------+
 * |       1 |  6 |           1 | 女装/女士精品   | 呢绒外套    |  399.90  |  1200 |
 * |       2 |  3 |           1 | 女装/女士精品   | 卫衣        |   89.90 |  1500 |
 * |       3 |  4 |           1 | 女装/女士精品   | 牛仔裤      |   89.90  |  3500 |
 * |       1 |  8 |           2 | 户外运动       | 山地自行车   | 1399.90  |  2500 |
 * |       2 | 11 |           2 | 户外运动       | 运动外套     |  799.90  |   500 |
 * |       3 | 12 |           2 | 户外运动       | 滑板        |  499.90  |  1200 |
 * +---------+----+-------------+---------------+------------+----------+-------+
 * 6 rows in set (0.00 sec)
 * ```
 *
 * 在名称为“女装/女士精品”的商品类别中，有两款商品的价格为89.90元，分别是卫衣和牛仔裤。两款商品的序号都应该为2，而不是一个为2，另一个为3。此时，可以使用RANK()函数和DENSE_RANK()函数解决。
 */
