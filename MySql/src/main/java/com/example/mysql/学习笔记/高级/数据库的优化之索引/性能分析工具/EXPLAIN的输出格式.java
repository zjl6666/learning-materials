package com.example.mysql.学习笔记.高级.数据库的优化之索引.性能分析工具;

public class EXPLAIN的输出格式 {
}
/**
 * 7.1 EXPLAIN四种输出格式
 * 这里谈谈EXPLAIN的输出格式。EXPLAIN可以输出四种格式: 传统格式，JSON格式， TR格式以及可视化输出。
 * 用户可以根据需要选择适用于自己的格式。
 *
 * 1.传统格式
 *
 * 2. JSON格式
 * 第1种格式中介绍的EXPLAIN语句输出中缺少了一个衡量执行计划好坏的重要属性--成本。而JSON格式是四种
 * 格式里面输出信息最详尽的格式，里面包含了执行的成本信息。
 * ● JSON格式:在EXPLAIN单词和真正的查询语句中间加上FORMAT=JSON。
 *
 * EXPLAIN FORMAT=JSON。
 *
 * 3. TREE格式
 * TREE格式是8.0.16版本之后引入的新格式，主要根据查询的各个部分之间的关系和各部分的执行顺序来描述如何查
 * 询。
 * EXPLAIN FORMAT=TREE
 *
 * 4.可视化输出
 * 可视化输出，可以通过MySQL Workbench可视化查看MySQL的执行计划。
 * 通过点击Workbench的放大镜图标，即.可生成可视化的查询计划。
 *
 *
 * SHOW WARNINGSE  查询优化器可能对涉及子查询的查询语句进行重写，从而转换为连接查询。
 *
 *
 */