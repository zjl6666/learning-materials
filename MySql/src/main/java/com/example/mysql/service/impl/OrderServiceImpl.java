package com.example.mysql.service.impl;

import com.example.mysql.bean.Order;
import com.example.mysql.mapper.OrderMapper;
import com.example.mysql.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class OrderServiceImpl implements OrderService {

    @Autowired
    OrderMapper orderMapper;

    @Override
    public List<Order> getListOrder() {
        List<Order> list = orderMapper.getListOrder();
        System.out.println(list);
        return list;
    }

    @Override
    public Order getOrderId(Integer id) {
        return orderMapper.getOrderId(id);
    }
}
