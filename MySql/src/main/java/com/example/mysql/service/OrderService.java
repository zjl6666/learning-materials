package com.example.mysql.service;

import com.example.mysql.bean.Order;

import java.util.List;

public interface OrderService {
    List<Order> getListOrder();
    Order getOrderId(Integer id);
}
