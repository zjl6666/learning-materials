package com.example.mysql.mapper;

import com.example.mysql.bean.Order;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface OrderMapper {

    List<Order> getListOrder();
    Order getOrderId(Integer id);
}
