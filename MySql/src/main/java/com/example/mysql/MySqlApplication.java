package com.example.mysql;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.example.mysql.mapper")//可以批量加入@Mapper注解
public class MySqlApplication {

    public static void main(String[] args) {
        SpringApplication.run(MySqlApplication.class, args);
    }

}
