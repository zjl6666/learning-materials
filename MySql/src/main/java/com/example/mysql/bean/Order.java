package com.example.mysql.bean;

import lombok.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode
public class Order {
    private Integer orderId;
    private String orderName;
}
