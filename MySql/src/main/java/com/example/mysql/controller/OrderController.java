package com.example.mysql.controller;


import com.example.mysql.bean.Order;
import com.example.mysql.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class OrderController {
    @Autowired
    OrderService orderService;

    @ResponseBody
    @GetMapping("/sql")
    public List<Order> getOr(){
        List<Order> list = orderService.getListOrder();
        System.out.println(list);
        return list;
    }
    @GetMapping("/getId")
    public Order getId(){
        Order list = orderService.getOrderId(1);
        System.out.println(list);
        return list;
    }

}
