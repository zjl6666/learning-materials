package com.example.nettydemo.NIO.组件.NIOSelector;

import java.io.IOException;
import java.nio.channels.Selector;

/**
 * 特点再说明:
 * 1) Netty 的I/O线程NioEventLoop聚合了Selector(选择器 ，
 * 也叫多路复用器)，可以同时并发处理成百上千个客
 * 户端连接。
 * 2)当线程从某客户端 Socket通道进行读写数据时，若没
 * 有数据可用时，该线程可以进行其他任务。
 * 3) 线程通常将非阻塞I0的空闲时间用于在其他通道上
 * 执行I0操作，所以单独的线程可以管理多个输入和
 * 输出通道。
 * 4) 由于读写操作都是非阻塞的，这就可以充分提升10
 * 线程的运行效率，避免由于频繁I/O阻塞导致的线程
 * 挂起。
 * 5) 一个I/O线程可以并发处理N个客户端连接和读写操
 * 作，这从根本上解决了传统同步阻塞I/O一连接一线
 * 程模型，架构的性能、弹性伸缩能力和可靠性都得到
 * 了极大地提升。
 */

public class Main {
    public static void main(String[] args) throws IOException {
        Selector selector = Selector.open();
        selector.select();//阻塞
        selector.select(1000);//阻塞1000毫秒
        selector.wakeup();//唤醒selector
        selector.selectNow();//不阻塞  立马返还
        /**
         * NIO非阻塞网络编程原理
         *
         */
    }
}
