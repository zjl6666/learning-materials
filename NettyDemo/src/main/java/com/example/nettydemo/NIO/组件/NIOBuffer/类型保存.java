package com.example.nettydemo.NIO.组件.NIOBuffer;

import java.nio.ByteBuffer;

public class 类型保存 {
    public static void main(String[] args) {
        ByteBuffer buffer = ByteBuffer.allocate(1024);

        buffer.putInt(100);
        buffer.putLong(100);
        buffer.putChar('龙');
        buffer.putShort((short) 100);
        buffer.putDouble(100.1D);

        buffer.flip();
        System.out.println(buffer.getInt());
        System.out.println(buffer.getLong());
        System.out.println(buffer.getChar());
        System.out.println(buffer.getShort());
        System.out.println(buffer.getDouble());

    }
}
