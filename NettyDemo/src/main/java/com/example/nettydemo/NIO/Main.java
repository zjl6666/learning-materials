package com.example.nettydemo.NIO;

/**
 * I/O模型
 * BIO、NIO、 AIO适用场景分析
 * 1) BI0方 式适用于连接数目比较小且固定的架构，
 * 这种方式对服务器资源要求比较高，并发局限于应用中，
 * JDK1 .4以前的唯-选择，但程序简单易理解。
 * 2) NI0方 式适用于连接数目多且连接比较短(轻操作)的架构，
 * 比如聊天服务器，弹幕系统，服务器间通讯等。编程比较复杂，
 * JDK1.4开始支持。
 * 3) AI0方式使用于连接数目多且连接比较长(重操作)的架构，
 * 比如相册服务器，充分调用OS参与并发操作，编程比较复杂，
 * JDK7开始支持。
 */

/**
 *5) Java NIO的非阻塞模式，使一个线程从某通道发送请求或者读取数据，但是它仅能得
 * 到目前可用的数据，如果目前没有数据可用时，就什么都不会获取，而不是保持线
 * 程阻塞，所以直至数据变的可以读取之前，该线程可以继续做其他的事情。非阻塞
 * 写也是如此，一个线程请求写入一些数据到某通道，但不需要等待它完全写入，这
 * 个线程同时可以去做别的事情。[ 后而有案例说明]
 * 6)通俗理解: NIO是可 以做到用-一个线程来处理多个操作的。假设有10000个请求过来,
 * 根据实际情况，可以分配50或者100个线程来处理。不像之前的阻塞10那样，非得分
 * 配10000个。
 * 7) HTTP2.0使用 了多路复用的技术，做到同一个连接并发处理多个请求，而且并发请求
 * 的数量比HTTP1.1大了好几个数量级。
 */
public class Main {
    public static void main(String[] args) {
        /**
         * 每一个Channel对应一个Buffer
         * 一个线程对应多个Channel
         * 一个Selector对应一个线程
         * Selector会根据不同的事件  在各个通道上切换
         * Buffer就是一个内存块，底层是一个数组
         * 数据的读取写入是通过  Buffer这个BIO，
         * BIO中   要么输入流   要么输出流不能双向
         * 但是NIO的Buffer是可以读也可以写,需要flip方法切换
         * Channel是双向的，可以返回底层操作系统的情况
         * 比如Linux，底层的操作系统通道就是双向的
         *
         */
    }
}
