package com.example.nettydemo.NIO.组件.NIOSelector;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.util.Scanner;

public class 客户端 {
    public static void main(String[] args) throws IOException {
        SocketChannel socketChannel = SocketChannel.open();
        socketChannel.configureBlocking(false);//设置为非阻塞
//        socketChannel.socket().bind();

        InetSocketAddress inetSocketAddress = new InetSocketAddress(7777);

        if (!socketChannel.connect(inetSocketAddress)) {
            while (!socketChannel.finishConnect()){
                System.out.println("连接需要时间 ");
            }
        }
        Scanner cin = new Scanner(System.in);
        String s = "hello,张蛟龙";
        while (true){

            ByteBuffer wrap = ByteBuffer.wrap(s.getBytes());//将数据存在缓存里
            socketChannel.write(wrap);
//            System.in.read();
            s = cin.nextLine();
            System.out.println(s);

        }


    }
}
