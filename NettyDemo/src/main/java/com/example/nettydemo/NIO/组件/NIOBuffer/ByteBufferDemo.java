package com.example.nettydemo.NIO.组件.NIOBuffer;

import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;

public class ByteBufferDemo {
    public static void main(String[] args) throws Exception {
        MappedByteBufferTest();
    }

    private static void MappedByteBufferTest() throws Exception {
        //可让文件直接在内存(堆外内修改)，操作系统不需要拷贝一次
        RandomAccessFile rw = new RandomAccessFile("File/NIO/MappedByteBuffer.txt", "rw");
        FileChannel channel = rw.getChannel();//获取通道

        MappedByteBuffer map = channel.map(FileChannel.MapMode.READ_WRITE, 0, 5);

        map.put(0,(byte) '2');
        map.put(3,(byte) '2');
        ByteBuffer byteBuffer = ByteBuffer.allocate(1024);
        int read = channel.read(byteBuffer);
        byteBuffer.flip();
        byte[] array = byteBuffer.array();
//        rw.write(array,0,read);
        System.out.println(new String(array,0,read));

        channel.close();
        rw.close();

    }
}
