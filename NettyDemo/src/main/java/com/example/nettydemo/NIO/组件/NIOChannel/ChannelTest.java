package com.example.nettydemo.NIO.组件.NIOChannel;

/**
 * Channel是一个通道，可以通过它读取和写入数据，它就像水管一样，
 * 网络数据通过Channel读取和写入。通道与流的不同之处在于通道是双向的，
 * 流只是在一一个方向 上移动(一个流必须是InputStream或者OutputStream的子类)，
 * 而且通道可以用于读、写或者同时用于读写。
 * 因为Channel是全双工的，所以它可以比流更好地映射底层操作系统的API。
 * NIO中通过channel封装了对数据源的操作。通过channel我们可以操作数据源，
 * 但又不必关心数据源的具体物理结构。这个数据源可能是多种的。
 * 比如，可以是文件,也可以是网络socket.
 * 在大多数应用中，channel 与文件描述符或者socket是一对应的。
 * Channel 用于在字节缓冲区和位于通道另-侧的实体
 * (通常是一个文件或套接字)之间有效地传输数据。
 */
public class ChannelTest {
    public static void main(String[] args) {
        /**
         * 常用的Channel类有: FileChannel、DatagramChannel、
         *      ServerSocketChannel 和SocketChannel。
         *      [ ServerSocketChannel类似ServerSocket , SocketChannel类似Socket]
         * FileChannel 用于文件的数据读写，
         *      DatagramChannel用于UDP的数据读写，
         *      ServerSocketChannel和SocketChannel用于TCP的数据读写。
         *
         *   FileChannel主要用来对本地文件进行I/O操作，常见的方法有
         * 1) public int read(ByteBuffer dst)，从通道读取数据并放到缓冲区中
         * 2) public int write(ByteBuffer src)，把缓冲区的数据写到通道中
         * 3) public long transferFrom(ReadableByteChannel src, long position, long count)，
         *      从目标通道中复制数据到当前通道
         * 4) public long transferTo(long position, long count, WritableByteChannel target)，
         *      把数据从当前通道复制给目标通道
         */

    }
}
