package com.example.nettydemo.NIO.组件.NIOChannel;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.StandardCharsets;
public class 文件操作 {
    public static void main(String[] args) {
        File file = new File("File/NIO/zjlInput.txt");

        //创建一个缓冲区
        ByteBuffer byteBuffer = ByteBuffer.allocate(1024);//当内容过多   会读取不完

        文件读(file,byteBuffer);

        File file1 = new File("File/NIO/zjlOutput.txt");
        文件写(file1,byteBuffer);


    }

    /**
     *  //直接内存是不支持array()方法的。
     * //文件-》输入流的（通道）里-》缓冲区-》输出
     */
    private static void 文件读(File file,ByteBuffer byteBuffer) {

        FileInputStream fis = null;//创建一个输入流   内置有通道
        try {
            fis = new FileInputStream(file);
            FileChannel channel = fis.getChannel();//获取对应的FileChannel

            //将通道的信息读入到  Buffer
            channel.read(byteBuffer);

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                fis.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * //信息（输入）-》缓冲区（ByteBuffer）-》对缓冲区反转-》
     * 写入到通道里（通道在这个输出流里）-》写道文件中
     */
    private static void 文件写(File file,ByteBuffer byteBuffer) {

        FileOutputStream fos = null;//创建一个输出流   内置有通道
        try {
            fos = new FileOutputStream(file);
            FileChannel channel = fos.getChannel();//获取对应的FileChannel

            //对缓冲区反转
            byteBuffer.flip();

            //写入到FileChannel 通道
            channel.write(byteBuffer);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                fos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
