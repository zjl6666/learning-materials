package com.example.nettydemo.NIO;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * NIO  新的流   比IO强
 * 阻塞IO
 */
public class PathTest {
    //E:\\xunlian\\untitled\\File\\hello.txt
    public static void main(String[] args) {
        Files常用方法();
    }

    private static void Files常用方法() {
        Path path1 = Paths.get("E:\\", "xunlian\\untitled\\File\\hello.txt");
        Path path2 = Paths.get("File\\hello3.txt");
        System.out.println(Files.exists(path2));
        /**
         * Files.
         * ➢Path copy(Path sre, Path dest, CopyOption .. how):文件的复制
         * ➢Path createDirectory(Path path, FileAttribute<?> ... attr): 创建一个目录
         * ➢Path createFile(Path path, FileAttribute<?> ... arr): 创建一个文件
         * ➢void delete(Path path):删除一个文件/目录， 如果不存在，执行报错
         * ➢void deleteifExists(Path path): Path对应的文件/目录如果存在，执行删除
         * ➢Path move(Path src, Path dest, CopyOptin...how):将src移动到dest位置
         * ➢long size(Path path):返回path指定文件的大小
         * Files常用方法:用于判断
         * ➢boolean exists(Path path, LinkOption ... opts):判断文件是否存在
         * ➢boolean isDirectory(Path path, LinkOption ... opts) :判断是否是目录
         * ➢boolean isRegularFile(Path path, LinkOption ... opts):判断是否是文件
         * ➢boolean isHidden(Path path) :判断是否是隐藏文件
         * ➢boolean isReadable(Path path) :判断文件是否可读
         * ➢boolean isWritable(Path path):判断文件是否可写
         * ➢boolean notExists(Path path, LinkOption ... opts) :判断文件是否不存在
         * Files常用方法:用于操作内容
         * ➢SeekableByteChannel newByteChannel(Path path, OpenOption.. .how) :获职与指定文件的连
         * 接，how指定打开方式。
         * ➢DirectoryStream<Path> newDirectoryStream(Path path):打开path指定的目录
         * ➢InputStream newInputStream(Path path, OpenOption how): 获取InputStream对象
         * ➢OutputStream newOutputStream(Path path. OpenOption how) :获取OutputStream对象
         */}

    private static void Path方法() {
        Path path1 = Paths.get("E:\\", "xunlian\\untitled\\File\\hello.txt");
        Path path2 = Paths.get("File\\hello.txt");

//		String toString() ： 返回调用 Path 对象的字符串表示形式
        System.out.println(path1);//E:\\xunlian\\untitled\\File\\hello.txt

//		boolean startsWith(String path) : 判断是否以 path 路径开始
        System.out.println(path1.startsWith("E:\\"));//true
//		boolean endsWith(String path) : 判断是否以 path 路径结束
        System.out.println(path1.endsWith("hello.txt"));//true
//		boolean isAbsolute() : 判断是否是绝对路径
        System.out.println(path1.isAbsolute() + "~");//true
        System.out.println(path2.isAbsolute() + "~");//false
//		Path getParent() ：返回Path对象包含整个路径，不包含 Path 对象指定的文件路径
        System.out.println(path1.getParent());//E:\\xunlian\\untitled\\File
        System.out.println(path2.getParent());//File     返回文件的之前路径
//		Path getRoot() ：返回调用 Path 对象的根路径
        System.out.println("path1 对象的根路径:"+path1.getRoot());//E:\\
        System.out.println("path2 对象的根路径:"+path2.getRoot());//相对路径 无根路径
//		Path getFileName() : 返回与调用 Path 对象关联的文件名
        System.out.println("path1文件名"+path1.getFileName());//hello.txt
        System.out.println("path2文件名"+path2.getFileName());//hello.txt
//		int getNameCount() : 返回Path 根目录后面元素的数量
//		Path getName(int idx) : 返回指定索引位置 idx 的路径名称
        for (int i = 0; i < path1.getNameCount(); i++) {
            System.out.println(path1.getName(i) + "*****");
        }// xunlian\\untitled\\File\\hello.txt

//		Path toAbsolutePath() : 作为绝对路径返回调用 Path 对象  返回此文件的绝对路径
        System.out.println(path1.toAbsolutePath());//E:\\xunlian\\untitled\\File\\hello.txt
        System.out.println(path2.toAbsolutePath());//E:\\xunlian\\untitled\\File\\hello.txt
//		Path resolve(Path p) :合并两个路径，返回合并后的路径对应的Path对象
        Path path3 = Paths.get("E:\\", "xunlian");
        Path path4 = Paths.get("untitled\\File\\hello.txt");
        path3 = path3.resolve(path4);//合并两个目录
        System.out.println("合并后的路径："+path3);

//		File toFile(): 将Path转化为File类的对象
        File file = path1.toFile();//Path--->File的转换
        Path newPath = file.toPath();//File--->Path的转换
    }

    private static void Path的基础应用() {
        Path path1 = Paths.get("E:\\xunlian\\untitled\\File\\hello.txt");//new File(String filepath)

        Path path2 = Paths.get("E:\\", "xunlian\\untitled\\File\\hello.txt");//new File(String parent,String filename);

        System.out.println(path1);
        System.out.println(path2);

        Path path3 = Paths.get("E:\\", "xunlian");
        System.out.println(path3);
    }
}
