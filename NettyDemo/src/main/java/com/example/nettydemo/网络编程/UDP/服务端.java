package com.example.nettydemo.网络编程.UDP;

import java.net.DatagramPacket;
import java.net.DatagramSocket;

public class 服务端 {
    public static void main(String[] args) throws Exception {
        DatagramSocket socket = new DatagramSocket(65533);

        byte[] bytes = new byte[100];
        DatagramPacket packet = new DatagramPacket(bytes,0,bytes.length);

        socket.receive(packet);
        System.out.println(new String(packet.getData(),0, packet.getLength()));

        socket.close();

    }
}
