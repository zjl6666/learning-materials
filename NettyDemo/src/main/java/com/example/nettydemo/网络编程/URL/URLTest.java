package com.example.nettydemo.网络编程.URL;

import java.net.URL;

/**
 * URL网络编程
 * 1.URL:统一资源定位符，对应着互联网的某一资源地址
 * 2.格式：
 *  http://localhost:8080/examples/beauty.jpg?username=Tom
 *  协议   主机名     端口号  资源地址             参数列表
 *
 */
public class URLTest {
    public static void main(String[] args) throws Exception {
        URL url = new URL("https://localhost:8080/untitled/File/1.jpg?username=Tom");

        System.out.println("该URL的协议名:"+url.getProtocol());//获取该URL的协议名
        System.out.println("该URL的主机名:"+url.getHost());//获取该URL的主机名
        System.out.println("该URL的端口号:"+url.getPort());//获取该URL的端口号
        System.out.println("URL的文件路径:"+url.getPath());//获取该URL的文件路径
        System.out.println("URL的文件名:"+url.getFile());// 获取该URL的文件名
        System.out.println("URL的查询名:"+url.getQuery());//获取该URL的查询名
        /**
         * 该URL的协议名:http
         * 该URL的主机名:localhost
         * 该URL的端口号:8080
         * URL的文件路径:/examples/beauty.jpg
         * URL的文件名:/examples/beauty.jpg?username=Tom
         * URL的查询名:username=Tom
         */



    }
}
