package com.example.nettydemo.网络编程.UDP;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

public class 客户端 {
    public static void main(String[] args) throws Exception{
        DatagramSocket socket = new DatagramSocket();

        String str = "我是UDP发送的";
        byte[] bytes = str.getBytes();
        InetAddress inet = InetAddress.getByName("127.0.0.1");
        DatagramPacket packet = new DatagramPacket(bytes,0,bytes.length,inet,65533);
        socket.send(packet);


        socket.close();
    }
}
