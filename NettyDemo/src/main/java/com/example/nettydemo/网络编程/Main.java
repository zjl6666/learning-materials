package com.example.nettydemo.网络编程;

/**
 * ●计算机网络:
 * 把分布在不同地理区域的计算机与专门的外部设备用通信线路互连成一个规
 * 模大、功能强的网络系统，从而使众多的计算机可以方便地互相传递信息、
 * 共享硬件、软件、数据信息等资源。
 * ●网络编程的目的:
 *  直接或间接地通过网络协议与其它计算机实现数据交换，进行通讯。
 * ●网络编程中有两个主要的问题:
 * ➢如何准确地定位网络上一台或多台主机;定位主机上的特定的应用
 * ➢找到主机后如何可靠高效地进行数据传输
 *
 * 如何实现网络中的主机互相通信
 * ●通信双方地址
 *  ➢IP
 *  ➢端口号
 * ●一定的规则(即:网络通信协议。有两套参考模型)
 * ➢OSI参考模型:模型过于理想化，未能在因特网上进行广泛推广
 * ➢TCP/IP参考模型(或TCP/IP协议):事实上的国际标准。
 */

import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * 一、网络编程中有两个主要的问题：
 *   1.如何准确地定位网络上一台或多台主机；定位主机上的特定的应用
 *   2.找到主机后如何可靠高效地进行数据传输
 * 二、网络编程中的两个要素：
 *   1.对应问题一：IP和端口号
 *   2.对应问题二：提供网络通信协议：
 *     TCP(传输层)/IP(网络层)参考模型（应用层、传输层、网络层、物理+数据链路层）
 * 三、通信要素一：IP和端口号     域名--》DNS（域名服务器）-》网络服务器
 *   1. IP:唯一的标识 Internet 上的计算机（通信实体）
 *   2. 在Java中使用InetAddress类代表IP
 *   3. IP分类：IPv4 和 IPv6 ; 万维网 和 局域网
 *   4. 域名:   www.baidu.com   www.mi.com
 *             www.sina.com  www.jd.com  www.vip.com
 *   5. 本地回路地址：127.0.0.1 对应着：localhost
 *
 *   6. 如何实例化InetAddress:两个方法：getByName(String host) 、 getLocalHost()
 *        两个常用方法：getHostName() / getHostAddress()
 *
 *   7.端口号标识正在计算机上运行的进程(程序)
 *      ➢不同的进程有不同的端口号
 *      ➢被规定为一个16位的整数0~65535。
 *      ➢端口分类:
 *          ➢公认端口: 0~1023。 被预先定义的服务通信占用(如:
 *           HTTP占用端口80，FTP占用端口21，Telnet占用端口23)
 *          ➢注册端口: 1024~49151。 分配给用户进程或应用程序。( 如: Tonjcat占
 *            用端口8080，MySQL 占用端口3306，Oracle 占用端口1521等)。
 *          ➢动态/私有端口: 49152~65535.
 *   8.端口号与IP地址的组合得出- -个网络套接字: Socket.
 */
public class Main {
    public static void main(String[] args) {
        try {
            //File file = new File("hello.txt");
            InetAddress inet1 = InetAddress.getByName("192.168.10.14");

            System.out.println(inet1);//  /192.168.10.14

            InetAddress inet2 = InetAddress.getByName("www.atguigu.com");
            System.out.println(inet2);//  www.atguigu.com/111.7.163.158

            //获取本机
            InetAddress inet3 = InetAddress.getByName("localhost");
            System.out.println(inet3);//localhost/127.0.0.1

            //获取本地ip
            InetAddress inet4 = InetAddress.getLocalHost();
            System.out.println(inet4);

            //getHostName()
            System.out.println(inet2.getHostName());//获得他的域名
            //getHostAddress()
            System.out.println(inet2.getHostAddress());//获得他的ip

        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
    }
}
