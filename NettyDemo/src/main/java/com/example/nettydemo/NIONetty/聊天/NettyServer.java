package com.example.nettydemo.NIONetty.聊天;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;

public class NettyServer {
    public static void main(String[] args) {
        /*
        创建两个线程组  都是无限循环
        bossGroup和workerGroup的子线程的个数
        默认是CPU核数*2
         */
        System.out.println(Runtime.getRuntime().availableProcessors());//查看CPU核数

        NioEventLoopGroup bossGroup = new NioEventLoopGroup();//专门负责接受客户端的连接
        NioEventLoopGroup workerGroup = new NioEventLoopGroup();//专门负责网络的编写

        ServerBootstrap serverBootstrap = new ServerBootstrap();//创建服务器端的启动对象

        serverBootstrap.group(bossGroup,workerGroup)//使用链式编程进行设置
                /*
                 * 不同协议、不同的阻塞类型的连接都有不同的Channel类型与之对应，常用的
                 * Channel类型:
                 * ● NioSocketChannel, 异步的客户端TCP Socket连接。
                 * ●NioServerSocketChannel, 异步的服务器端TCP Socket连接。
                 * ●NioDatagramChannel, 异步的UDP连接。
                 * ●NioSctpChannel, 异步的客户端Sctp连接。
                 * ●NioSctpServerChannel, 异步的Sctp 服务器端连接，这些通道涵盖了UDP和TCP网络10
                 * 以及文件I0.
                 */
                .channel(NioServerSocketChannel.class)//使用NioSocketChannel做通道实现
                .option(ChannelOption.SO_BACKLOG,128)//设置线程队列连接个数
                .childOption(ChannelOption.SO_KEEPALIVE,true)//设置保持活动连接状态
                .childHandler(new ChannelInitializer<SocketChannel>() {
                    @Override//给pipeline设置处理器
                    protected void initChannel(SocketChannel ch) throws Exception {
                        ch.pipeline().addLast(new NettyServerHandler());//添加处理器
                    }
                });//给我们的workerGroup 的EventLoop 对应的管道设置处理器

        System.out.println("服务器准备好了");
        try {
            //绑定一个端口并且同步，生成了一个ChannelFuture对象
            ChannelFuture cf = serverBootstrap.bind(5438).sync();
            cf.addListener(new ChannelFutureListener() {
                @Override
                public void operationComplete(ChannelFuture future) throws Exception {
                    if(cf.isSuccess()){
                        System.out.println("监听5438成功");
                    }else {
                        System.out.println("监听5438失败");
                    }
                }
            });
            cf.channel().closeFuture().sync();//对关闭通道进行监听
        } catch (InterruptedException e) {
            e.printStackTrace();
        }finally {
            bossGroup.shutdownGracefully();
            workerGroup.shutdownGracefully();
        }


    }
}
