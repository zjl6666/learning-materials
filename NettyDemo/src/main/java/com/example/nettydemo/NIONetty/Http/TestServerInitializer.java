package com.example.nettydemo.NIONetty.Http;

import io.netty.channel.*;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.http.*;
import io.netty.handler.stream.ChunkedWriteHandler;


/**
 * HttpObject//表示客户端和服务端互相通信的数据封装成  HttpObject
 */
public class TestServerInitializer extends ChannelInitializer<SocketChannel> {

    @Override
    protected void initChannel(SocketChannel ch) throws Exception {
        ChannelPipeline pipeline = ch.pipeline();//得到管道

        //HttpServerCodec  是netty提供的http编码解码器
        pipeline.addLast("HttpServerCodec:",new HttpServerCodec());
        pipeline.addLast("TestHttpServerHandler:",new TestHttpServerHandler());

    }

}
