package com.example.nettydemo.NIONetty.buf;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

public class buf1 {
    public static void main(String[] args) {

        System.out.println("----------------------");
        //创建
        ByteBuf buf = Unpooled.copiedBuffer("hello,张蛟龙", Charset.forName("utf-8"));

        if(buf.hasArray()){
            byte[] array = buf.array();
//            StandardCharsets.UTF_8
            System.out.println(new String(array,0,array.length).trim());
            System.out.println(buf);
            //UnpooledByteBufAllocator$InstrumentedUnpooledUnsafeHeapByteBuf(ridx: 0, widx: 15, cap: 64)

            System.out.println(buf.arrayOffset());//0
            System.out.println(buf.readerIndex());//默认0
            System.out.println(buf.writerIndex());
            //到第几个  15个字符 显示十五 也就是下一个写  就是16个，但是下标为15
            System.out.println(buf.capacity());//总容量  64
            System.out.println(buf.readableBytes());//15  可读字节数
            buf.readByte();//读了一个会影响readerIndex
            buf.getByte(1);//读取第几个 不会影响readerIndex
            System.out.println(buf.readerIndex());//1

            System.out.println(buf.readableBytes());
            //14  可读字节数 即writerIndex-readerIndex

            buf.getCharSequence(0,4,StandardCharsets.UTF_8);//下标为0开始 读4个字符




        }

    }

    private static void buf001() {
        //创建一个ByteBuf
        //1.创建一个对象，该对象包含一个数组arr，是一个byte[10]
        //在netty的buf中，不需要进行反转
        // 底层维护了readerIndex   读的位置
        // 和writerIndex         写的位置
        //capacity 总容量
        ByteBuf buf = Unpooled.buffer(10);

        for (int i = 0; i < 10; i++) {
            buf.writeByte(i);//writerIndex 会自动增加
        }
        for (int i = 0; i < buf.capacity(); i++) { //buf.capacity()容量
            System.out.println(buf.getByte(i));
        }
        buf.readByte();//readerIndex  会自动增加
    }
}
