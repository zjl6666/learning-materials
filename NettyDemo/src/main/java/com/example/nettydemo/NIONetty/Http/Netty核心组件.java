package com.example.nettydemo.NIONetty.Http;

/**
 * ●Bootstrap、 ServerBootstrap。创建启动对象
 * 1) Bootstrap 意思是引导，--个Netty应用通常由-个Bootstrap开始，
 * 主要作用是配置整个Netty程序，串联各个组件，
 * Netty 中Bootstrap 类是客户端程序的启动引导类，
 * ServerBootstrap是服务端启动引导类
 * 2)常见的方法有
 * public ServerBootstrap group(EventLoopGroup parentGroup, EventLoopGroup childGroup)，
 * 该方法用于服务器端，用来设置两个EventLoop
 * public B group(EventLoopGroup group)，该方法用于客户端，用来设置一个EventLoop
 * public B channel(Class< ? extends > channels),该方法用来设置一个服务器端的通道实现
 * public <T> B option(ChannelOption<T> option, T value),用来给ServerChannel添加配置
 * public <T> ServerBootstrap childOption(ChannelOption<T> childOption, T value),
 * 用来给接收到的通道添加配置
 * public ServerBootstrap childHandler(ChannelHandler childHandler)，
 * 该方法用来设置业务处理类(自定义的handler)
 * public ChannelFuture bind(int inetPort)，该方法用于服务器端，用来设置占用的端口号
 * public ChanelFuture connect(String inetHost, int inetPort)，
 * 该方法用于客户端，用来连接服务器
 */

/**
 * Future、ChannelFuture
 * Netty中所有的I/O操作都是异步的，不能立刻得知消息是否被正确处理。
 * 但是可以过一会等它执行完成或者直接注册一个监听，
 * 具体的实现就是通过Future 和ChannelFutures，他们可以注册一个监听，
 * 当操作执行成功或失败时监听会自动触发注册的监听事件
 * 常见的方法有
 * Channel channel()，返回当前正在进行IO操作的通道
 * ChannelFuture sync()，等待异步操作执行完毕
 */

/**
 * ●Channel
 * 1) Netty 网络通信的组件，能够用于执行网络I/0操作。
 * 2)通过Channel可获得当前网络连接的通道的状态
 * 3)通过Channel可获得网络连接的配置参数(例如接收缓冲区大小)
 * 4) Channel 提供异步的网络I/0操作(如建立连接，读写，绑定端口)，异步调用意味着
 *  任何I/0调用都将立即返回，并且不保证在调用结束时所请求的I/0 操作已完成
 * 5)调用立即返回一个ChannelFuture实例，通过注册监听器到ChannelFuture上，
 * 可以I/O操作成功、失败或取消时回调通知调用方
 * 6) 支持关联I/0操作与对应的处理程序
 * 7) 不同协议、不同的阻塞类型的连接都有不同的Channel类型与之对应，常用的
 * Channel类型:
 *      Channel类型:
 *      ● NioSocketChannel, 异步的客户端TCP Socket连接。
 *      ●NioServerSocketChannel, 异步的服务器端TCP Socket连接。
 *      ●NioDatagramChannel, 异步的UDP连接。
 *      ●NioSctpChannel, 异步的客户端Sctp连接。
 *      ●NioSctpServerChannel, 异步的Sctp 服务器端连接，
 *       这些通道涵盖了UDP和TCP网络IO以及文件I0.
 */

/**
 * Selector 选择器
 * 1) Netty基于Selector对象实现I/O多路复用，
 * 通过Selector一个线程可以监听多个连接的Channel事件。
 * 2) 当向--个Selector中注册Channel后，Selector内部的机制就可以自动不断地查询
 * (Select)这些注册的Channel是否有已就绪的I/0事件(例如可读，可写，网络连接.
 * 完成等)，这样程序就可以很简单地使用一个线程高效地管理多个Channel
 */

/**
 * ●ChannelHandler 及其实现类
 * 1) ChannelHandler是--个接口，处理I/O事件或拦截I/O操作，并将其转发到其
 *    ChannelPipeline(业务处理链)中的下一个处理程序。
 * 2) ChannelHandler 本身并没有提供很多方法，因为这个接口有许多的方法需要实现，
 *    方便使用期间， 可以继承它的子类
 * 3) ChannelHandler 及其实现类一览图(后)
 *   ChannelInboundHandler入站处理器
 *    ChannelOutboundHandler出站处理器
 *
 */

/**
 * Pipeline和ChannelPipeline
 * ChannelPipeline是一个重点:
 * 1) ChannelPipeline是一个Handler的集合，它负责处理和拦截inbound或者
 *    outbound的事件和操作，相当于一个贯穿Netty的链。
 *    (也可以这样理解:ChannelPipeline是保存ChannelHandler的List,
 *    用于处理或拦截Channel的入站事件和出站操作)
 * 2) ChannelPipeline实现了一 种高级形式的拦截过滤器模式，使用户可以完全控制事
 *    件的处理方式，以及Channel中各个的ChannelHandler如何相互交互
 * 3) 在Netty中每个Channel都有且仅有一个ChannelPipeline与之对应，
 *    ●一个Channel包含了一个ChannelPipeline,
 *     而ChannelPipeline中又维护了一个由ChannelHandlerContext组成的双向链表，
 *     并且每个ChannelHandlerContext(通道处理程序上下文)中又关联着一个 ChannelHandler
 *    ●入站事件和出站事件在一个双向链表中，入站事件会从链表head往后传递到最后一个入站的handler,
 *     出站事件会从链表tail往前传递到最前一个出站的handler,两种类型的handler互不干扰
 */

/**
 * ●ChannelHandlerContext(通道处理程序上下文)
 * 1)保存Channel相关的所有上下文信息，同时关联一个ChannelHandler 对象
 * 2)即ChannelHandlerContext中 包含一个具体的事件处理器ChannelHandler ,
 * 同时ChannelHandlerContext中也绑定了对应的pipeline 和Channel的信息，方便
 * 对ChannelHandler进行调用.
 * 3)常用方法
 *   ●ChannelFuture close(), 关闭通道
 *   ●ChannelOutboundInvoker flush(),刷新
 *   ●ChannelFuture writeAndFlush(Object msg)，将数据写到ChannelPipeline中当前
 *    ChannelHandler的下一个ChannelHandler开始处理(出站)
 */

/**
 * ●ChannelOption
 * 1) Netty 在创建Channel实例后,一般都需要设置ChannelOption参数。
 * 2) ChannelOption 参数如下:
 * ChannelOption.SO_BACKLOG
 *   对应TGP/IP协议listen函数中的backlog参数，用来初始化服务器可连接队列大小。
 *   服务端处理客户端连接请求是顺序处理的，所以同一时间只能处理一一个客户端连接。
 *   多个客户端来的时候，服务端将不能处理的客户端连接请求放在队列中等待处理，
 *   backlog 参数指定了队列的大小。
 * ChannelOption.SO_KEEPALIVE
 *   一直保持连接活动状态
 */

/**
 * EventLoopGroup和其实现类NioEventLoopGroup
 * 1) EventLoopGroup是-组EventLoop的抽象，Netty为了更好的利用多核CPU资源，
 * 一般会有多个EventLoop同时工作，每个EventLoop维护着-个Selector 实例。
 * 2) EventLoopGroup提供next接口， 可以从组里面按照一定规则获取其中一个
 * EventLoop来处理任务。在Netty服务器端编程中，我们一般都需要提供两个
 * EventLoopGroup，例如: BossEventLoopGroup 和WorkerEventLoopGroup。
 * 3)通常一个服务端口即一个ServerSocketChannel对应一个Selector和一个EventLoop线程。
 * BossEventLoop 负责接收客户端的连接并将SocketChannel交给
 * （一个或多个）WorkerEventLoopGroup来进行1/0处理，
 * ●BossEventLoopGroup通常是一个单线程的EventLoop,
 *  EventLoop 维护着一个注册了ServerSocketChannel的Selector实例
 *  BossEventLoop不断轮询Selector将连接事件分离出来
 * ●通常是OP_ ACCEPT事件，然后将接收到的SocketChannel交给WorkerEventLoopGroup
 * ●WorkerEventLoopGroup会由next选择其中一个
 *  EventLoop来将这个SocketChannel注册到其维护的Selector并对其后续的I0事件进行处理
 */
public class Netty核心组件 {
}
