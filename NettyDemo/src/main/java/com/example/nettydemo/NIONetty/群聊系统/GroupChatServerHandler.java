package com.example.nettydemo.NIONetty.群聊系统;


import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.channel.group.ChannelGroup;
import io.netty.channel.group.DefaultChannelGroup;
import io.netty.util.concurrent.GlobalEventExecutor;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;

public class GroupChatServerHandler extends SimpleChannelInboundHandler<String> {
    //定义一个channel组 管理所有的channel
    //是一个全局时间执行器  一个单例
    private static ChannelGroup channels = new DefaultChannelGroup(GlobalEventExecutor.INSTANCE);
    SimpleDateFormat sdf = new SimpleDateFormat("yyy-MM-dd hh:mm:ss");

    public static Map<String,Channel> channelMap = new HashMap<>();//可以造一个  单独的

    @Override //表示连接建立 ，一旦建立，第一个被执行
    public void handlerAdded(ChannelHandlerContext ctx) throws Exception {
        Channel channel = ctx.channel();

        //该方法会将channelGroup中所有的channel 遍历，并发送
        channels.writeAndFlush("客户端"+channel.remoteAddress()+"加入聊天\n");

        channels.add(channel);//将该客户加入聊天  推送到其他客户

//        channelMap.put("id100",channel);
    }

    @Override //表示断开连接 告知在线客户    会自动删除离开的人
    public void handlerRemoved(ChannelHandlerContext ctx) throws Exception {
        Channel channel = ctx.channel();

        //该方法会将channelGroup中所有的channel 遍历，并发送
        channels.writeAndFlush("客户端"+channel.remoteAddress()+"离开了聊天\n");

    }

    @Override //表示channel 处于一个活动状态  提示xxx上线
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        System.out.println(ctx.channel().remoteAddress()+"上线了");
        System.out.println("现在有"+channels.size()+"个客户");
    }

    @Override //表示离线
    public void channelInactive(ChannelHandlerContext ctx) throws Exception {
        System.out.println(ctx.channel().remoteAddress()+"离线了");
        System.out.println("现在有"+channels.size()+"个客户");
    }

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, String msg) throws Exception {
        Channel channel = ctx.channel();//获取Channel

        channels.forEach(ch ->{
            if(channel != ch){//如果不是自己  直接转发
                ch.writeAndFlush(
                        "客户"+channel.remoteAddress()
                                +"发送了消息:" + msg + "\n");
            }else {
                ch.writeAndFlush("自己"+"发送了消息:" + msg + "\n");
            }
        });

    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        ctx.close();//关闭通道
    }
}