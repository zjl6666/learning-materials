package com.example.nettydemo.NIONetty;

/**
 * Netty对JDK自带的NIO 的API 进行了封装，解决了上述问题。
 * 1)设计优雅: 适用于各种传输类型的统一API阻塞和非阻塞Socket; 基于灵活且可扩展的事件模型，
 *   可以清晰地分离关注点;高度可定制的线程模型单线程，一个或多个线程池.
 * 2)使用方便: 详细记录的Javadoc， 用户指南和示例;
 *   没有其他依赖项，JDK5 (Netty3.x)或6 (Netty4x)就足够了。
 * 3)高性能、 吞吐量更高:延迟更低;诚少资源消耗;最小化不必要的内存复制。
 * 4)安全:完整的SSL/TLS 和StartTLS 支持。
 * 5)社区活跃、 不断更新:社区活跃，版本迭代周期短，
 *   发现的Bug 可以被及时修复，同时，更多的新功能会被加入
 */
public class Main {
    /**
     * Netty模型
     * 1)Netty抽象出两组线程池：
     *      BossGroup专门负责接受客户端的连接
     *      WorkerGroup专门负责网络的编写
     * 2) BossGroup和 WorkerGroup 类型都是NioEventLoopGroup
     * 3) NioEventLoopGroup相当于一个         事件  循环 组，
     *   这个组中含有多个事件循环，每一个事件循环是NioEventLoop
     * 4) NioEventLoop表示个不断循环的执行处理任务的线程，
     *   每个NioEventLoop都有一个selector ,
     *   用于监听绑定在其上的socket的网络通讯
     * 5) NioEventLoopGroup可以有多个线程，即可以含有多个NioEventLoop
     * 6) 每个Boss NioEventLoop执行的步骤有3步
     *    1. 轮询accept事件
     *    2. 处理accept事件，与client建立连接，生成NioSocketChannel ,
     *       并将其注册到某个worker NIOEventLoop上的selector
     *    3.处理任务队列的任务，即runAllTasks
     * 7)每个Worker NIOEventLoop循环执行的步骤
     *      1.轮询read, write事件
     *      2.处理I/O事件，即read, write事件，在对应NioSocketChannel处理
     *      3.处理任务队列的任务，即runAllTasks
     * 8)每个Worker NIOEventLoop处理业务时，会使用pipeline(管道)，
     *  pipeline中包含了channel ,即通过pipeline可以获取到对应通道，
     *  管道中维护了很多的处理器
     */
}
