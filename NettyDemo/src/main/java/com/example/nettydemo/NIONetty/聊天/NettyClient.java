package com.example.nettydemo.NIONetty.聊天;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;

public class NettyClient {
    public static void main(String[] args) {
        //客户端只要一个事件循环组
        NioEventLoopGroup group = new NioEventLoopGroup();

        Bootstrap bootstrap = new Bootstrap();//创建客户端的启动对象

        //设置相关参数
        bootstrap.group(group)//设置线程组
                .channel(NioSocketChannel.class)//设置客户端通道的实现类
                .handler(new ChannelInitializer<SocketChannel>() {
                    @Override
                    protected void initChannel(SocketChannel ch) throws Exception {
                        ch.pipeline().addLast(new NettyClientHandle());//加入自己的处理器
                    }
                });
        System.out.println("客户端准备好了");
        /**
         *Future-Listener机制
         * 1)当Future对象刚刚创建时，处于非完成状态，调用者可以通过返回的ChannelFuture
         * 来获取操作执行的状态，注册监听函数来执行完成后的操作。
         * 2)常见有如下操作
         * ●通过isDone方法来判断当前操作是否完成;
         * ●通过isSuccess方法来判断已完成的当前操作是否成功;
         * ●通过getCause方法来获取己完成的当前操作失败的原因;
         * ●通过isCancelled方法来判断已完成的当前操作是否被取消;
         * ●通过addListener方法来注册监听器，当操作已完成(isDone方法返回完成)，
         * 将会通知指定的监听器;如果Future对象已完成，则通知指定的监听器
         */
        try {
            //启动客户端 去链接服务器端 关于ChannelFuture 涉及到netty异步模型
            ChannelFuture sync = bootstrap.connect("127.0.0.1", 5438).sync();

            sync.addListener(new ChannelFutureListener() {
                @Override
                public void operationComplete(ChannelFuture future) throws Exception {
                    if(sync.isSuccess()){
                        System.out.println("5438连接成功");
                    }else {
                        System.out.println("5438连接失败");
                    }
                }
            });
            //给关闭通道进行监听
            sync.channel().closeFuture().sync();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }finally {
            group.shutdownGracefully();
        }


    }
}
