package com.example.myBatisDemo.ch.been;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.apache.ibatis.type.Alias;

import java.io.Serializable;

@Data
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
@Alias("user")
public class User implements Serializable {//Serializable实现序列化接口
    private static final long serialVersionUID = 996L;
    private Integer id;
    private String username;
    private String password;
    private Details details;
}
