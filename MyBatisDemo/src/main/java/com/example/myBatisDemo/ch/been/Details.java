package com.example.myBatisDemo.ch.been;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.apache.ibatis.type.Alias;

import java.io.Serializable;
import java.util.List;

@Data
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
@Alias("details")
public class Details implements Serializable {//Serializable实现序列化接口
    private static final long serialVersionUID = 997L;
    private Integer id;
    private Integer age;
    private String nb;
    private List<User> lUser;
}
