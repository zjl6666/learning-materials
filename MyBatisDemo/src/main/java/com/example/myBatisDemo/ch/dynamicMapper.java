package com.example.myBatisDemo.ch;

import com.example.myBatisDemo.ch.been.Dynamic;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface dynamicMapper {
    public List<Dynamic> getDynamicList(Dynamic dynamic);
    public List<Dynamic> getDynamicChoose(Dynamic dynamic);
    public List<Dynamic> MysqlOrOracle(Dynamic dynamic);
//    public List<Dynamic> MysqlOrOracle();

    public List<Dynamic> getDynamicForEach(List<Integer> list);

    public int updateDynamic(Dynamic dynamic);

    public int addDynamicList(List<Dynamic> list);
//    public int addDynamicList(int list);
    /**
     * 在MyBatis 中Mapper不能重载
     * 因为在xml的映射文件中   查找 id="addDynamicList"
     * 是根据K-V形式查找的  而K是根据   包名.类名.方法  的字符串形式
     * 如果在同一个类中方法名一样   就会重复
     * 而底层源码显示  如果之前存在过这个key  就直接报错
     */
}
