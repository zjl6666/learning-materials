package com.example.myBatisDemo.ch.been;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.apache.ibatis.type.Alias;

import java.io.Serializable;
import java.sql.Date;
@Data
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
@Alias("dynamic")
public class Dynamic implements Serializable {//Serializable实现序列化接口
    private static final long serialVersionUID = 998L;
    private Integer id;
    private String name;
    private Integer age;
    private Date date;
    private Integer sex;
}
