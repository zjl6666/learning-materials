package com.example.myBatisDemo.ch;

import com.example.myBatisDemo.ch.been.User;
import org.apache.ibatis.annotations.MapKey;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Map;

/**
 * mapper接口没有实现类，但是mybatis会为这个接口生成一个代理对象。
 * (将接口和xml进行绑定)
 * UserMapper userMapper = sqlSession. getMapper( UserMapper.class)
 *
 */
@Mapper
public interface UserMapper {
//    @Select("select *from login where id = #{id}")
    public User getUserid (Integer id);
//    public User getUseridList (Integer id);

    public List<User> getUser ();//和返回User 一样  它会自动添加
    public List<User> getUserResultMap ();//和返回User 一样  它会自动添加

    public List<User> getUserResultMapW ();//和返回User 一样  它会自动添加


    @MapKey("id")//指定map的k用谁指定
    public Map<Integer,User> getUserMap ();//和返回User 一样  它会自动添加

    //有多个参数时  @Param("username")  给他主动命名   或者主动传一个map  因为他还是主动封装到map中
    User loginOne(@Param("username") String username, @Param("password") String password);
    //或者在xml里  #{param1}   括号里为param1-paramN  第一个是param1  后续...
//     这个不管你该不该  加不加注解都会在封装一个这样的map的集合   括号里为param1-paramN
    User loginOneN( String username,  String password);

    void addUser(User user);
    void addUser(Map<String,String> map);//使用Map

    void updateUser(User user);
    //如果返回值是Boolean  就返回成功与失败
//    Integer返回值影响了多少

    Integer deleteUser(Integer id);

    /**
     * ========================思考================================
     * public Employee get Emp (@Param(”id")Integer id,String lastName );
     *      取值: id==>#{id/param1} lastName==>#{param2}
     * public Employee getEmp(Integer id, @Param("e")Employee emp) ;
     *      取值: id==>#{param1}
     *      lastName=== >#{ param2. lastName/e . lastName }
     * public Employee getEmpById(List<Integer> ids);
     * 取值:取出第一-个id的值: #{list[0]}    最终还是封装到map中  当是集合时会特殊处理
     */

}
