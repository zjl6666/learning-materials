package Java9new.Java9;

import java.io.*;

public class tryAndIO流Test {
    public static void main(String[] args) {
        String cpFile0="File\\hello.txt";
        String cpFile1="File\\hello1.txt";
        try {
            字节流的复制JKD9(cpFile0,cpFile1,50);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

    }
    private static void 字节流的复制JKD7(String cpFile0, String cpFile1, int sd) {
        File file = new File(cpFile0);
        File file1 = new File(cpFile1);
        FileInputStream fI = null;
        FileOutputStream fO = null;
        try {
            fI = new FileInputStream(file);
            fO = new FileOutputStream(file1,false);//覆盖  没有创建
//            fO = new FileOutputStream(file1,true);//向后添加  没有创建
            byte[] bytes = new byte[sd];//
            int len;
            while ((len = fI.read(bytes)) != -1){
//                String str = new String(bytes,0,len);
                fO.write(bytes,0,len);
                //由于汉字等某些字符  一个字符占不等的 字节 所以有汉字的用字符流好

//                System.out.print(str);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            try {
                fI.close();

            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                fO.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private static void 字节流的复制JKD8(String cpFile0, String cpFile1, int sd) {
        File file = new File(cpFile0);
        File file1 = new File(cpFile1);
        //在try（）里的资源自动关闭
        try ( FileInputStream fI = new FileInputStream(file);FileOutputStream fO = new FileOutputStream(file1,false)){
            //覆盖  没有创建
//            fO = new FileOutputStream(file1,true);//向后添加  没有创建
            byte[] bytes = new byte[sd];//
            int len;
            while ((len = fI.read(bytes)) != -1){
                fO.write(bytes,0,len);
                //由于汉字等某些字符  一个字符占不等的 字节 所以有汉字的用字符流好

            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void 字节流的复制JKD9(String cpFile0, String cpFile1, int sd) throws FileNotFoundException {
        File file = new File(cpFile0);
        File file1 = new File(cpFile1);
        FileInputStream fI = new FileInputStream(file);
        FileOutputStream fO = new FileOutputStream(file1,false);//覆盖  没有创建
        try (fI;fO){//仅适用于fI  new的时候不抛异常的 如InputStreamReader
//            fO = new FileOutputStream(file1,true);//向后添加  没有创建
//            byte[] bytes = new byte[sd];//
//            int len;
//            while ((len = fI.read(bytes)) != -1){
////                String str = new String(bytes,0,len);
//                fO.write(bytes,0,len);
//                //由于汉字等某些字符  一个字符占不等的 字节 所以有汉字的用字符流好
//
////                System.out.print(str);
//            }
            fI.transferTo(fO);//JDK9新出的  可以将输入流复制到输出流中 8192个单位

        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            try {
                fI.close();

            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                fO.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

}
