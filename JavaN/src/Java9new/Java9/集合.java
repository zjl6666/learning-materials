package Java9new.Java9;

import java.util.*;

public class 集合 {
    public static void main(String[] args) {
        List<String> name = new ArrayList<>();
        name.add("Joe");
        name.add("Bob");
        name.add("Bill");
        //使name不可修改JDK8
        name = Collections.unmodifiableList(name);//只读集合

//        name.add("666");//会报错
        System.out.println(name);
        List<Integer> list = Arrays.asList(1,2,3,4,5);//只读集合

        //JKD9新出的
        System.out.println("JKD9新出的");
        List<Integer> list1 = List.of(1,2,3,4,5);//of只读集合 JKD9新出的

        Map.of("1",1,"2",2);//只读集合
        Map.ofEntries(Map.entry("5",5),Map.entry("6",6));//只读集合

    }
}
