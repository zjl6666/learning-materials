package Java9new.Java9;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

/**
 * Stream的终止操作
 * * allMatch 是否匹配所有
 * * anyMatch 是否匹配一个
 * * noneMatch 是否没有匹配一个
 * * findFirst 返回第一个
 *
 * * count 返回总数
 * * max 返回最大
 * * min 返回最小
 *
 * * reduce 归约 将元素连续操作得到结果
 *
 * * collect 收集
 *   1 将流转换成其他的形式(其他集合)
 *   2 接收Collector接口 用于Stream中元素各种汇总      特例 Collectors.joining()拼接字符串
 */
public class StringAPIJKD9 {
    public static void main(String[] args) {
        List<Integer> list = Arrays.asList(1, 8, 3, 10, 5, 6, 7, 8, 2, 9, 4, 12);
        //返回从开头开始的尽量多的元素。  即 当x<10不满足后   就停止
        list.stream().takeWhile(x->x<10).forEach(System.out::print); // 1 8 3
        System.out.println("************************");
        //takeWhile的补集。  即 当x<10满足后 之后全要
        list.stream().dropWhile(x->x<10).forEach(System.out::print);

        System.out.println("*********************");
        Integer i=null;
        Stream<Integer> stream1 = Stream.of(i,i);//不能完全为null 即不能只有一个null
        Stream<Integer> stream2 = Stream.ofNullable(i);//只能有一个null

        stream1.forEach(System.out::println);//流终止了 在要用的要重新创建
//        System.out.println("两个null,null元素个数"+Stream1.count());
//        stream2.forEach(System.out::println);//输出为空
        System.out.println("一个null元素个数"+stream2.count());

        //iterate迭代    JDK8            每次加2   遍历前十个       循环输出
        Stream.iterate(0/*起始值*/,t -> t+2).limit(5).forEach(System.out::println);

        //iterate  JDK9   初始值 终止条件  循环条件         循环输出
        Stream.iterate(0,x -> x<10,x->x+1).forEach(System.out::println);


    }
}
