package Java9new.Java9;

public class 接口 implements java9_jk{
    public static void main(String[] args) {
        接口 jk = new 接口();
        jk.methodAbstract();
        jk.methodDefault();

        java9_jk.methodStatic();

    }

    @Override
    public void methodAbstract() {
        System.out.println("我是接口中的抽象方法");
    }
}
interface java9_jk{
    void methodAbstract();
    static void methodStatic(){
        System.out.println("我是接口中的静态方法");
    }
    default void methodDefault(){
        System.out.println("我是接口中的默认方法");
    }
    private void methodPrivate(){//JDK9中新出的接口私有方法
        System.out.println("我是JDK9中新出的接口私有方法");
    }
}

