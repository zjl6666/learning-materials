package Java9new.Java9;

/**
 * ●经过4次跳票，历经曲折的Java9终于终于在2017年9月21日发布。
 * ●从Java9这个版本开始，Java的计划发布周期是6个月，下一个Java的主版
 * 本将于2018年3月发布，命名为Java18.3，紧接着再过六个月将发布Java18.9。
 * ●这意味着Java的更新从传统的以特性驱动的发布周期，转变为以时间驱动的
 * (6个月为周期)发布模式，并逐步的将Oracle JDK原商业特性进行开源。
 * ●针对企业客户的需求，Oracle将以三年为周期发布长期支持版木(long term support)。
 * ●Java 9提供了超过150项新功能特性，包括备受期待的模块化系统、可交互
 *  * 的REPL工具: jshell, JDK 编译工具，Java公共API和私有代码，以及安
 * 全增强、扩展提升、性能管理改善等。可以说Java 9是一个庞大的系统工程，
 * 完全做了一个整体改变。
 *
 * ➢模块化系统
 * ➢jShell命令//可以直接在cmd中写Java代码  用/hellp查看所有方法
 * ➢多版本兼容jar包
 * ➢接口的私有方法
 * ➢钻石操作符的使用升级
 * ➢语法改进: try语句
 * ➢String存储结构变更 字符串底层的char[]数组改为byte[]数组
 * ➢便利的集合特性: of()//创建只读集合
 * ➢增强的Stream API
 * ➢全新的HTTP客户端API
 * ➢Deprecated的相关API
 * ➢javadoc的HTML 5支持
 * ➢Javascript引擎升级: Nashorn
 * ➢java的动态编译器
 */
public class Main {//目录改变
    public static void main(String[] args) {

    }
}
