package Java10;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

public class Main {
    public static void main(String[] args) {

        var list = List.of("java", "c++", "c");
        var list1 = List.copyOf(list);//创建一个不可变的
        System.out.println(list1);
        System.out.println(list==list1);//true

        var list2 = new ArrayList<String>();list2.add("java");
        var list3 = List.copyOf(list2);//复制一个不可变的（只读集合）
        System.out.println(list3==list2);//false
        //如果原来的集合也是只读的  那么就和原来集合一样
        //如果原来的集合不是只读的  那么就新创建一个新的集合
    }

    private static void varTest() {
        /**
         * 局部变量的类型推断 JDK10新特性
         * 必须赋值   否者没法推断
         * 接口无法推断
         * lambda表达式    不可以用
         * 方法引用  不可以用
         * var 不是关键字（int  long char等都是关键字）  是个标识符
         */
        var num = 10;
        int a =10;
        var list =new ArrayList<Integer>();
        for (var var :list){
            System.out.println(var);
        }
        Consumer<String> consumer = String :: new;
    }
}
