package txt;

import java.io.*;
import java.util.Scanner;

public class 阅读 {
    public static void main(String[] args) {
        复制();
    }


    private static void 复制() {
        BufferedReader br = null;
        Scanner s = new Scanner(System.in);
        BufferedWriter bw = null;
        int k=50;
        try {
            br = new BufferedReader(new FileReader("File\\小说\\太初.txt"));//new File  可以省略
            bw = new BufferedWriter(new FileWriter("File\\小说\\太初().txt"));
            String data;
            while ((data = br.readLine())!=null){//一次读一行 字符流才有
                data = data.trim();

                if("".equals(data)){

                }else {

                    int i = 0;
                    for (; i < data.length()/k; i++) {
//                        System.out.println(data.substring(i*k,i*k+k));
                        bw.write(data.substring(i*k,i*k+k));//不包括换行符

                        bw.newLine();//添加换行 导致  最后一行必定是空的
                    }
                    bw.write(data.substring(i*k));//不包括换行符

                    bw.newLine();//添加换行 导致  最后一行必定是空的
//                    System.out.println(data.substring(i*k));
//                    s.nextLine();
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                br.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                bw.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    private static void readTest() {
        File file = new File("File\\小说\\太初.txt");

        /**
         *  说明点：
         *     1. read()的理解：返回读入的一个字符。如果达到文件末尾，返回-1
         *     2. 异常的处理：为了保证流资源一定可以执行关闭操作。需要使用try-catch-finally处理
         *     3. 读入的文件一定要存在，否则就会报FileNotFoundException。
         *
         */
        FileReader fR = null;
        try {
            fR = new FileReader(file);
            //可能出现异常 但是这时fR还没有开启流 所以fR.close();
            // 要判断一下防止没有流  没法关闭  出现异常

            //读入
            int data = fR.read();//读取第一个字符
            while (data!=-1){
                System.out.print((char) data);
                data = fR.read();//读完了  返回-1
            }

//            //批量查询
//            char[] cbuf = new char[5];//fR.read(cbuf)
//            // 他的读取只是覆盖原来的数组 如果原本有值且没有全部覆盖，后几个char就是上次的char
//            System.out.println(fR.read(cbuf));//读到几个返回多少  没有返回-1

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if(fR!=null) fR.close();//流资源关闭   不关闭造成资源浪费
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    } //读入  把文件能容弄到java里

}
