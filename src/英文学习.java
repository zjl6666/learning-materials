public class 英文学习 {
    /**
     * Netty的英文学习
     * handler   处理者
     * accept    接受
     * Channel   通道
     * Pipeline  管道
     * Selector  选择器
     * Event     事件
     * Loop      环
     * Group     组
     * Server    服务器
     * Client    客户
     * Bootstrap 自举程序/独自创立
     * future    将来
     * Context   上下文
     */
    /**
     * DataSource   数据源
     * Function     函数
     * Predicate    断言
     * Supplier     供给
     * Consumer      消费者
     * format      安排…的版式
     * Optional   可选择的
     * generate    生成
     * filter     滤器
     * iterate     迭代.
     * reduce     减少
     */
}
