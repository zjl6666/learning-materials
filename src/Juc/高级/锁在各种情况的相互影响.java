package Juc.高级;

import java.util.concurrent.TimeUnit;
//取其根本  就是  synchronized 的锁 就是当前的这个对象
//static synchronized    锁是当前的class 全局唯一
class Phone {//static  锁的Class方法
    public static synchronized void B(){

    }//相当于👇

    //锁的是资源类方法
    public  synchronized void sendEmail()throws Exception //static  全局锁已经定了 不可多线程同时
    {
        TimeUnit.SECONDS.sleep(2);
        System.out.println("****sendEmail");
    }

    public synchronized void sendSMS()throws Exception
    {
        System.out.println("****sendSMS");
    }

    public void sayHello()throws Exception
    {
        System.out.println("****hello");
    } //没加锁说明他不是内部的方法

    /**
     * synchronized 和 static synchronized
     *         是两把不同的锁  相同锁才有影响
     * this            当前类.class
     *
     */
    public synchronized void A(){

    }//相当于👇

    public  void a(){
        synchronized (this){

        }
    }

    public  void b(){
        synchronized (Phone.class){

        }
    }

}// //八锁的关系

public class 锁在各种情况的相互影响 {
    public static void main(String[] args) {
        locks();
    }

    private static void locks() {
        Phone phone=new Phone();
        Phone phone2=new Phone();
        new Thread(()->{
            try {
                phone.sendEmail();
            }catch (Exception e){
                e.printStackTrace();
            }
        },"phone").start();
        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        new Thread(()->{
            try {
//                phone.sendSMS();// 1静态 1动 一部两部手机都不影响 静态和动态分开了所以一部二部不影响 都动态
                //phone.sayHello();//不加锁完全不影响
                phone2.sendSMS();
                // 动态 两部手机不影响（相当于造了两个函数）  静态 两部手机有影响(一共就一个对象 不得不争抢）
            }catch (Exception e){
                e.printStackTrace();
            }
        },"sms").start();
    }   //八锁的关系

}
