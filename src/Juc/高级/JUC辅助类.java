package Juc.高级;

import lombok.Getter;

import java.util.concurrent.*;

/**
 * //多线程
 * JUC :java.util.concurrent（并发）   .atomic（原子）    .locks(锁)
 *   进程/线程         并发/并行
 *
 * 我们程序设计追求“高内聚，低耦合’
 * ➢高内聚:类的内部数据操作细节自己完成，不允许外部干涉;
 * ➢低耦合:仅对外暴露少量的方法用于使用。
 *   在高内聚    低耦合  的前提下    线程  操作（对外暴露的调用方法）  资源类
 *   （都可以用  不能修改）
 *
 * synchronized  不可中断  不公平锁
 * ReentrantLock 可中断  1.设置超时方法tryLock(long timeout,TimeUnit unit).
 *                      2.lockInterruptibly()放在代码块中，调用interrupt()方法中断
 *
 *  用Condition 可精准唤醒
 *
 */

public class JUC辅助类 {//多线程
    private  int id;
    private String userName;
    private  int age;

    public  static  void main(String[] args) {//main线程   一切程序入口

        SemaphoreDemo();
//        System.out.println(Runtime.getRuntime().availableProcessors());//查看核数
    }

    private static void SemaphoreDemo() {
        Semaphore semaphore=new Semaphore(3);//总数1个
        for (int i = 0; i < 7; i++) {
            new Thread(()->{
                try {
                    semaphore.acquire();//获得··             抢到
                    System.out.println(Thread.currentThread().getName() + "\t抢到了");
                    TimeUnit.SECONDS.sleep(1);
                    System.out.println(Thread.currentThread().getName() + "\t离开了");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }finally {
                    semaphore.release();//释放
                }
            },String.valueOf(i+1)).start();

        }
        System.out.println("55");
    }//Semaphore   限流

    private static void CyclicBarrierDemo() {
        CyclicBarrier cyclicBarrier=new CyclicBarrier(7,()->{
            System.out.println("召唤神龙");
        });

        for (int i = 0; i < 8; i++) {//多余的话  会一直等待
            new Thread(()->{
                try {
                    System.out.println("收集第"+Thread.currentThread().getName() + "课龙珠");
                    cyclicBarrier.await();//只有集齐才往后继续执行
                    System.out.println(" i"+Thread.currentThread().getName());
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (BrokenBarrierException e) {
                    e.printStackTrace();
                }
            },String.valueOf(i+1)).start();
        }
    } //CyclicBarrier  控制线程 做完后在接下来运行(必须做完才结束)

    public enum CountryEnum{
        ONE(1,"齐国"), TWO(2,"楚国"),
        THREE(3,"燕国"),FOUR(4,"赵国"),
        FIVE(5,"魏国"),SIX(6,"韩国");
        @Getter private Integer retCode;
        @Getter private String retMessage;

        CountryEnum(Integer retCode,String retMessage) {
            this.retMessage = retMessage;
            this.retCode = retCode;
        }

        public static CountryEnum forEach_CountryEnum(int index){

            CountryEnum[] myArray = CountryEnum.values();
            for(CountryEnum countryEnum:myArray){
                if(index==countryEnum.getRetCode()){
                    return countryEnum;
                }
            }
            return null;
        }
    }  //枚举

    private static void CountDownLatchDemo() {
        //      倒计时锁  计数  向下 门闩
        CountDownLatch countDownLatch=new CountDownLatch(6);//6次结束
        for (int i = 0; i < 6; i++) {
            new Thread(()->{
                System.out.println("秦国把" + Thread.currentThread().getName() + "灭了");
                countDownLatch.countDown();//每做一次记录一次
            },CountryEnum.forEach_CountryEnum(i+1).getRetMessage()).start();//String.valueOf(i+1)
        }     //           枚举
        try {
            countDownLatch.await();//没结束等待
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(Thread.currentThread().getName() + "秦国统一");
    }//CountDownLatch 倒计时锁  控制线程 做完后在接下来运行



}