package Juc.深入.synchronized的深入;

/**
 * 可重入锁
 * 可重入锁又名递归锁，是指在同一个线程在外层方法获取锁的时候，在进入内层方法会自动获取锁。
 * 说的有点抽象，下面会有一个代码的示例。
 * 对于Java ReentrantLock而言, 他的名字就可以看出是一个可重入锁，其名字是Reentrant Lock重新进入锁。
 * 对于Synchronized而言,也是一个可重入锁。可重入锁的一个好处是可一定程度避免死锁。
 * synchronized void setA() throws Exception{
 *     Thread.sleep(1000);
 *     setB();
 * }
 * synchronized void setB() throws Exception{
 *     Thread.sleep(1000);
 * }
 * 上面的代码就是一个可重入锁的一个特点，如果不是可重入锁的话，setB可能不会被当前线程执行，可能造成死锁。
 *
 * 隐式锁  (即synchronized关键字使用的锁)默认是可重入锁
 * 显示锁  ReentrantLock
 *
 */
public class 可重入锁 {

}
