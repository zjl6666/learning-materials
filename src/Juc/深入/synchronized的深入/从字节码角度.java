package Juc.深入.synchronized的深入;

/**
 * 进入相关的.class文件 右键 Open in Terminal
 * 输入
 *  javap -c .\名字.class
 *  javap -c .\从字节码角度.class
 *        -v    可以输出附加信息 ( 包括行号、本地变量表，反汇)
 *
 *  m1  大概长这样
 *  public void m1();
 *     Code:
 *        0: aload_0
 *        1: getfield      #3  // Field o:Ljava/lang/Object;
 *        4: dup
 *        5: astore_1
 *        6: monitorenter
 *        7: getstatic     #4  // Field java/lang/System.out:Ljava/io/PrintStream;
 *       10: ldc           #5  // String hello
 *       12: invokevirtual #6  // Method java/io/PrintStream.println:(Ljava/lang/String;)V
 *       15: aload_1
 *       16: monitorexit
 *       17: goto          25
 *       20: astore_2
 *       21: aload_1
 *       22: monitorexit
 *       23: aload_2
 *       24: athrow
 *       25: return
 *
 *        monitorenter  加锁
 * 第一个  monitorexit   释放
 * 第二个  monitorexit   防止异常情况出现稳定释放
 *
 * 一般情况          1个monitorenter  2个monitorexit
 * 如果主动抛出异常   1个monitorenter  1个monitorexit
 *
 * 如果是在方法上加 m2
 * 在flags 有    ACC_SYNCHRONIZED  ACC_PUBLIC
 * 表示这个方法是同步方法
 *
 * 如果是    m3
 * 在flags 有    ACC_SYNCHRONIZED  ACC_PUBLIC  ACC_STATIC
 * 表示这个方法是同步方法
 */

/**
 * 为什么synchronized会加一个对象来当锁
 * 所有类都有一个父类 Object
 * java的底层是c++    而c++源码看过后，
 * 每个对象天生都带着一个对象监视器
 * 每一个被锁住的对象都会和Monitor关联起来
 */
public class 从字节码角度 {
    Object o = new Object();

    public static synchronized void m3(){
        System.out.println("m2");
    }

    public static void main(String[] args) {

    }

    public void m1(){
        synchronized (o){
            System.out.println("m1");
        }
    }

    public synchronized void m2(){
        System.out.println("m2");
    }
}
