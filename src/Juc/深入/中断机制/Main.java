package Juc.深入.中断机制;//自己自行停止，

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * 一个线程不应该由其他线程来强制中断或停止，而是应该由线程自己自行停止，
 * 自己来决定自己的命运。
 * 所以，Thread.stop, Thread.suspend, Thread.resume都已经被废弃了。
 *
 * 在Java中没有办法立即停止一条线程，然而停止线程却显得尤为重要，
 * 如取消一个耗时操作。
 * 因此，Java提供了一种用于停止线程的"协商"机制  中断，也即中断标识协商机制。
 * 中断只是一种协作协商机制，Java没有给中断增加任何语法，
 * 中断的过程完全需要程序员自己实现。
 * 若要中断一个线程， 你需要手动调用该线程的interrupt方法，
 * 该方法也仅仅是将线程对象的中断标识设成true;
 * 接着你需要自己写代码不断地检测当前线程的标识位，如果为true，
 * 表示别的线程请求这条线程中断，
 * 此时究竞该做什么需要你自己写代码实现。
 *
 * 每个线程对象中都有一个中断标识位，用于表示线程是否被中断;
 * 该标识位为true表示中断，为false 表示未中断;
 * 通过调用线程对象的interrupt方法将该线程的标识位设为true;
 * 可以在别的线程中调用，也可以在自己的线程中调用。
 */
public class Main {
    static volatile boolean isStop = false;
    static AtomicBoolean ab = new AtomicBoolean(false);
    public static void main(String[] args) {
        Thread thread = new Thread(()->{
            while (true){
                if(Thread.currentThread().isInterrupted()){
                    //判断当前线程的中断状态  通过中断标志位
                    System.out.println("\nStop");
                    break;
                }
                try {Thread.sleep(2);} //使用了sleep  会将中断状态清除  所以需要再执行一次状态改变
                catch (InterruptedException e) {
//                    Thread.currentThread().interrupt();;
                    e.printStackTrace();
                }
                System.out.print("运行中.");
            }
        });
        thread.start();

        try {TimeUnit.MILLISECONDS.sleep(20);} catch (InterruptedException e) {e.printStackTrace();}
        //间隔时间

        new Thread(()->{
            thread.interrupt();//设置中断线程(即改变thread的中断标志位)为true
            //低层调用的是interrupt0 的native 的c++方法
        }).start();

    }

    private static void Thread自带的中断() {
        Thread thread = new Thread(()->{
            while (true){
                if(Thread.currentThread().isInterrupted()){
                    //判断当前线程的中断状态  通过中断标志位
                    System.out.println("\nStop");
                    break;
                }
                System.out.print("运行中.");
            }
        });
        thread.start();

        try {TimeUnit.MILLISECONDS.sleep(20);} catch (InterruptedException e) {e.printStackTrace();}
        //间隔时间

        new Thread(()->{
            thread.interrupt();//设置中断线程(即改变thread的中断标志位)为true
            //低层调用的是interrupt0 的native 的c++方法
        }).start();
    }

    private static void Atomic实现中断() {
        new Thread(()->{
            while (true){
                if(ab.get()){
                    System.out.println("Stop");
                    break;
                }
                System.out.println("hello");
            }
        }).start();

        try {TimeUnit.MILLISECONDS.sleep(20);} catch (InterruptedException e) {e.printStackTrace();}
        //间隔时间

        new Thread(()->{
            ab.set(true);
        }).start();
    }

    private static void volatile的实现中断() {
        new Thread(()->{
            while (true){
                if(isStop){
                    System.out.println("Stop");
                    break;
                }
                System.out.println("hello");
            }
        }).start();

        try {
            TimeUnit.MILLISECONDS.sleep(20);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }//间隔时间
        new Thread(()->{
            isStop=true;
        }).start();
    }

    private static void 中断的三大方法() {
        Thread thread = new Thread();
        thread.start();

        //实例方法interrupt()仅仅是设 置线程的中断状态为true,
        // 发起一个协商而不会立刻停止线程
        thread.interrupt();
        //如果线程处于正常活动状态,  仅仅是设 置线程的中断状态为true, 没任何影响
        //如果是阻塞状态  如sleep  wait  join等   会抛出中断的异常  并将中断状态清除(即中断状态改为默认的false,)


        //静态方法  判断线程是否被中断并清除当前中断状态。
        //1返回当前线程的中断状态，测试当前线程是否已被中断
        //2将当前线程的 中断状态清零并重新设为false,清除线程的中断状态
        Thread.interrupted();

        //判断当前线程的中断状态  通过中断标志位  如果此线程已经死了  则返回false 中断标志位默认值
        boolean interrupted = thread.isInterrupted();

    }
}
