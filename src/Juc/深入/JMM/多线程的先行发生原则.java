package Juc.深入.JMM;

/**
 * 在JMM中,
 * 如果一个操作执行的结界需要对另一个操作可见性
 * 或者代码重排序，那么这两个操作之间必须存在happens-before(先行发生)原则。
 * 逻辑上的先后关系
 *
 * 如果对结果没影响,可以随意重排
 *
 */

/**
 * happens-before(先行发生)原则  生死   只有生下来才会死
 * 1.次序规则  按照代码顺序，写在前面的操作先行发生于写在后面的操作;
 * 2.锁定规则
 *      对于同一把锁objectLock, threadA一定先unlock同一把锁后B才能获理该锁， A先行发生于8
 * 3.volatile变量规则
 * 4.传递规则 爷爷->爸爸->我->儿子
 * 5.线程启动规则(Thread Start Rule)  必须先start执行后在执行县城里的内容
 * 6.线程中断规则(Thread Interruption Rule)
 *      对线程interrupt)方法的调用先行发生于被中断线程的代码检测到中断事件的发生;
 *      可以通过Thread.interrupted()检测到是杏发生中断
 *      也就是说你要先调用interrupt()方法设置过中断标志位，我才能检测到中断发送
 * 7.线程终止规则(Thread Termination Rule)
 *      线程中的所有操作都先行发生于对此线程的终止检测，
 *      我们可以通过isAlive()等手段检测线程是否已经终止执行。
 * 8.对象终结规则(Finalizer Rule)
 *      一个对象的初始化完成(构造函数执行结束)先行发生于它的finalize()方法的开始
 *      对象没有完成初始化之前，是不能调用finalized()方法的
 *
 * happens-before(先行发生)原则  相当于一个可见性
 *
 */
public class 多线程的先行发生原则 {
    public static void main(String[] args) {
        //happens-before

    }
}
