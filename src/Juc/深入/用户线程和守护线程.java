package Juc.深入;

/**
 * 用户线程   守护线程
 *   默认都是用户线程
 *
 * 用户线程
 *    是系统的工作线程，它会完成这个程序需要完成的业务操作，
 * 守护线程
 *    是一种特殊的线程为其它线程服务的，
 *    在后台默默地完成一些系统性的服务
 *    如：GC垃圾回收线程
 *    守护线程作为一个服务线程，没有服务对象就没有必要继续运行了，
 *    如果用户线程全部结束了，意味着程序需要完成的业务操作已经结束了，
 *    系统可以退出了。所以假如当系统只剩下守护线程的时候，java虛拟机会自动退出。、
 *   有一个  isDaemon()   true:守护线程
 *                      false:用户线程
 *
 */
public class 用户线程和守护线程 {
    public static void main(String[] args) {
        Thread t = new Thread(()->{
            System.out.println(Thread.currentThread().getName()+"\t开始运行"+
                    (Thread.currentThread().isDaemon()?"守护线程":"用户线程"));
        },"t1");

        //更改为守护线程   一定要在start前设置  否则会爆java.lang.IllegalThreadStateException
        t.setDaemon(true);//只能在运行前设置
        t.start();

    }
}
