package Juc.深入.FutureDemo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

public class Stream的使用 {
    public static void main(String[] args) {
        List<User> list=List数组();
        System.out.println("-----------串行-------------------");
        long Time=System.currentTimeMillis();
        list.stream()/*创建一个顺序流*///.filter(t->t.getId()%2==0)//filter过滤
//                .filter(t->t.getAge()>24)//
//                .sorted((o1, o2) ->  o2.getUsername().compareTo(o1.getUsername()))//排序
                .map(x->String.format("%s \t的年龄是 %d \t随机数是 %.2f",x.getUsername(),x.getAge(),x.getSs()))//合并
//                .limit(1)//使元素不得超过给定的数量
                .forEach(System.out::println);
        System.out.println("花费时间为"+(System.currentTimeMillis()-Time));
        System.out.println("------------多线程---保证顺序--------------------------");
        Time=System.currentTimeMillis();
        list.parallelStream()/*创建一个顺序流*///.filter(t->t.getId()%2==0)//filter过滤
//                .filter(t->t.getAge()>24)//
//                .sorted((o1, o2) ->  o2.getUsername().compareTo(o1.getUsername()))//排序
                .map(x->String.format("%s \t的年龄是 %d \t随机数是 %.2f",x.getUsername(),x.getAge(),x.getSs()))//合并
//                .limit(1)//使元素不得超过给定的数量
                .forEach(System.out::println);
        System.out.println("花费时间为"+(System.currentTimeMillis()-Time));
        System.out.println("--------------并行----不保证顺序-----------------------");
        Time=System.currentTimeMillis();
        list.stream()//创建一个顺序流//filter过滤
//                .filter(t->t.getAge()>24)//
//                .sorted((o1, o2) ->  o2.getUsername().compareTo(o1.getUsername()))//排序
                .map(x-> CompletableFuture.supplyAsync(()->
                        String.format("%s \t的年龄是 %d \t随机数是 %.2f",x.getUsername(),x.getAge(),x.getSs())))//合并
//                .limit(1)//使元素不得超过给定的数量
//                .collect(Collectors.toList()).stream()
                .map(s->s.join())
                .forEach(System.out::println);
        System.out.println("花费时间为"+(System.currentTimeMillis()-Time));
        System.out.println("-----------------------------------------");
        Optional<User> any = list.stream().findAny();//串行流
        Optional<User> any1 = list.parallelStream().findAny();//并行流  多线程

    }


    private static ArrayList<User> List数组() {
        ArrayList<User> list= new ArrayList<>();
        list.add(new User(1001, "马化腾", 34));
        list.add(new User(1002, "马云", 12));
        list.add(new User(1003, "刘强东", 33));
        list.add(new User(1004, "雷军", 26));
        list.add(new User(1005, "李彦宏", 65));
        list.add(new User(1006, "比尔盖茨", 42));
        list.add(new User(1007, "任正非", 26));
        list.add(new User(1008, "扎克伯格", 35));
        return list;
    }
}
