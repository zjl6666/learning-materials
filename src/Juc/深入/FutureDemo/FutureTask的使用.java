package Juc.深入.FutureDemo;

import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Future接口(FutureTask实现类)定义了操作异步任务执行一些方法，
 * 如获取异步任务的执行结果、取消任务的执行、判断任务是否被取消、判断任务执行是否完毕等。
 * 比如主线程让一个子线程去执行任务，子线程可能比较耗时，启动子线程开始执行任务后，.
 * 主线程就去做其他事情了，忙其它事情或者先执行完，
 * 过了一会才去获取子任务的执行结果或变更的任务状态
 *
 * Future接口可以为主线程开一个分支任务，专门为主线程处理耗时和费力的复杂业务。
 *
 */
public class FutureTask的使用 { //简单的异步任务可以
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        FutureTask<Integer> futureTask1 = new FutureTask(new MyCallable1());
        Thread f1 = new Thread(futureTask1, "f1");
        f1.start();

        try {
            //在指定时间内  如果不返回   报错 java.util.concurrent.TimeoutException
            System.out.println(futureTask1.get(1,TimeUnit.SECONDS));
        } catch (TimeoutException e) {
            e.printStackTrace();
        }

        //调用时  会必须等待futureTask1算完才向后执行
//        System.out.println(futureTask1.get());


        while (true){
            //轮询的方式会耗费无谓的CPU资源，而且也不见得能及时地得到
            //如果想要异步获取结果，通常都会以轮询的方式去获取结果   尽量不要阻塞
            if(futureTask1.isDone()){//轮询判断futureTask1是否执行完
                System.out.println(futureTask1.get());
                break;
            }else {
                try {TimeUnit.MILLISECONDS.sleep(500); }catch (Exception e){e.printStackTrace();}
                System.out.println("正在处理中，请稍后.....");
            }
        }

    }
}
class MyCallable1 implements Callable<Integer> {
    AtomicInteger atomicInteger=new AtomicInteger(10);//原子性的int
    @Override
    public Integer call() {
        System.out.println("线程"+Thread.currentThread().getName()+"        call() 的执行！！！！！");
        try {
            TimeUnit.SECONDS.sleep(2);} catch (InterruptedException e) {e.printStackTrace();}
        return 1024;
    }//只会执行一次
}  //Callable<Integer>的高手有返回值

