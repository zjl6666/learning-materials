package Juc.深入.FutureDemo;

import java.util.concurrent.*;

/**
 * 使用Future之前提供的那点API就囊中羞涩，处理起来不够优雅，
 * 这时候还是让CompletableFuture以声明式的方式优雅的处理这些需求
 *
 * 阻塞的方式和异步编程的设计理念相违背，而轮询的方式会耗费无谓的CPU资源。
 * 因此，JDK8设计出CompletableFuture。
 *
 * CompletableFuture implements Future<T>, CompletionStage<T>
 *  Future  老的
 *  CompletionStage  一堆方法
 *
 * CompletableFuture底层默认使用 ForkJoinPool   分支合并池子
 *
 * 从Java8开始引入了CompletableFuture, 它是Future的功能增强版，减少阻塞和轮询
 * 可以传入回调对象，当异步任务完成或者发生异常时，自动调用回调对象的回调方法
 *
 */
public class CompletableFuture了解 { //可以执行复杂的异步任务
    public static void main(String[] args) throws Exception {
        ExecutorService executorService = Executors.newFixedThreadPool(5);//固定线程

        CompletableFuture<Integer> integerCompletableFuture = CompletableFuture.supplyAsync(() -> {
            int result = ThreadLocalRandom.current().nextInt(10);//随机获取[0-9]
            try {TimeUnit.MILLISECONDS.sleep(1000); }catch (Exception e){e.printStackTrace();}
            return result;
        },executorService);

        integerCompletableFuture.whenComplete((t, u) -> {
            System.out.println("****t\t" + t);//null   因为出异常了  这个return没有执行   如果没异常t==返回的值
            System.out.println("****u\t" + u);//返回异常信息  没异常  返回null
        }).exceptionally(f -> {//异常处理
            System.out.println("excpyion\t" + f.getMessage());
            //excpyion	java.lang.ArithmeticException: / by zero
            return 4444;//重新定义一个返回值
        });

        System.out.println("main线程忙其他的");//会发现没有打印出结果
        //因为CompletableFuture 底层默认使用 ForkJoinPool  默认的线程池是守护线程

        executorService.shutdown();//关闭线程池
    }

    private static void CompletableFuture的基本使用() throws InterruptedException, ExecutionException {

//        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(七个参数);//大家用的
        ExecutorService executorService = Executors.newFixedThreadPool(5);//固定线程


        CompletableFuture<Void> completableFuture1 = new CompletableFuture<>();//不推荐
        //推荐使用CompletableFuture.runAsync 和 CompletableFuture.supplyAsync

        CompletableFuture<Void> completableFuture =
                CompletableFuture.runAsync(() -> {// runAsync 没有返回值
            System.out.println(Thread.currentThread().getName() + "\t 没有返回值");
        },executorService);//使用自己的线程池.
        //默认时ForkJoinPool线程池
        completableFuture.get();//返回null


        CompletableFuture<Integer> integerCompletableFuture =
                CompletableFuture.supplyAsync(() -> {// supplyAsync 有返回值
            System.out.println(Thread.currentThread().getName() + "\t 有  返  回");
//            int age=10/0;
            return 1024;
        });

        executorService.shutdown();//关闭线程池

    }
}
