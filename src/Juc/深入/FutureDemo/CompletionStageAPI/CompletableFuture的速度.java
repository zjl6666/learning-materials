package Juc.深入.FutureDemo.CompletionStageAPI;

import java.util.concurrent.*;

public class CompletableFuture的速度 {
    public static void main(String[] args) {
        ExecutorService executorService = Executors.newFixedThreadPool(5);//固定线程

        CompletableFuture<Integer> cf1 =
                CompletableFuture.supplyAsync(() -> {
                    int result = ThreadLocalRandom.current().nextInt(10)+1000;//随机获取[0-9]
                    try { TimeUnit.MILLISECONDS.sleep(1000); }catch (Exception e){e.printStackTrace();}
                    return result;
                    },executorService);
        CompletableFuture<Integer> cf2 =
                CompletableFuture.supplyAsync(() -> {
                    int result = ThreadLocalRandom.current().nextInt(10)+100;//随机获取[0-9]
                    try { TimeUnit.MILLISECONDS.sleep(1000); }catch (Exception e){e.printStackTrace();}
                    return result;
                    },executorService);

        CompletableFuture<Integer> cf0 = cf1.applyToEither(cf2, (f) -> {
            return f + 10;
        });// applyToEither 谁速度快  用哪个

        System.out.println(cf0.join());

        executorService.shutdown();


    }
}
