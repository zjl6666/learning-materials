package Juc.深入.FutureDemo.CompletionStageAPI;

import java.util.concurrent.*;

public class CompletableFuture线程池的选择 {
    public static void main(String[] args) {
        ExecutorService executorService = Executors.newFixedThreadPool(5);//固定线程

        CompletableFuture<Integer> integerCompletableFuture =
                CompletableFuture.supplyAsync(() -> {
            int result = ThreadLocalRandom.current().nextInt(10);//随机获取[0-9]
            try { TimeUnit.MILLISECONDS.sleep(1000); }catch (Exception e){e.printStackTrace();}
            return result;
        },executorService);//需要有返回值
        CompletableFuture.runAsync(()->{
            System.out.println(Thread.currentThread().getName()+"qqqqqqqqq");
        }); //相当与接着执行新的任务  没返回值
        /**
         * thenRun()       使用的是自定义的线程池
         * 如果主任务(supplyAsync()的任务)执行太快，可能会直接使用 main 线程执行
         *
         * thenRunAsync()  使用的是默认的线程池   ForkJoinPool   除非 在()里指定线程池
         * Async//异步的
         */
        integerCompletableFuture.thenRun(()->{
            try { TimeUnit.MILLISECONDS.sleep(1000); }catch (Exception e){e.printStackTrace();}
            System.out.println(Thread.currentThread().getName()+"我是thenRun001");
        }).thenRun(()->{
            System.out.println(Thread.currentThread().getName()+"我是thenRun002");
        });

        integerCompletableFuture.join();
    }
}
