package Juc.深入.FutureDemo.CompletionStageAPI;

import java.util.concurrent.*;

/**
 * CompletableFuture  继承的接口 有 Future和 CompletionStage
 *
 */
public class CompletionStageAPIDemo {
    public static void main(String[] args) {

        CompletableFuture<Integer> cf = CompletableFuture.supplyAsync(() -> {
            int result = ThreadLocalRandom.current().nextInt(10);//随机获取[0-9]
            try { TimeUnit.MILLISECONDS.sleep(2000); }catch (Exception e){e.printStackTrace();}
            return result;
        });//CompletableFuture默认是守护线程  主线程不能过快结束

        cf.join();
    }

    private static void 对结果处理(CompletableFuture<Integer> cf) {
        /**
         * cf.thenRun()
         * 任务A执行完执行B，并且B不需要A的结果
         * cf.thenAccept()   消费性接口
         * 任务A执行完执行B, B需要A的结果，但是任务B无返回值
         * cf.thenApply()  函数式接口
         * 任务A执行完执行B，B需要A的结果，同时任务B有返回值
         */
        cf.thenAccept(r->{System.out.println(r);});//没有返回值 是Consumer接口 消费性接口

        //Function 函数式接口  有入参和出参
        cf.thenApply(f->{//中间出错   就会戛然而止  一般使用这个
            System.out.println("第一步");
            return f+10;
        }).thenApply(f->{
            System.out.println("第二步");
            int a=1/0;
            return f+100;
        }).thenApply(f->{
            System.out.println("第三步");
            return f+1000;
        }).whenComplete((v,e)->{
            if(e==null){
                System.out.println("thenApply结果为:"+v);
            }
        }).exceptionally(e->{
            e.printStackTrace();
            System.out.println(e.getMessage());
            return null;
        });

        cf.handle((f, e)->{//中间出错   只终结其中一个
            System.out.println("第一步");
            return f+10;
        }).handle((f,e)->{
            int a=10/0;
            System.out.println("第二步");
            return f+100;
        }).handle((f,e)->{
            System.out.println("第三步");
            return f+1000;
        }).whenComplete((v,e)->{
            if(e==null){
                System.out.println("thenApply结果为:"+v);
            }else
                System.out.println("异常thenApply结果为:"+v);//null
        }).exceptionally(e->{
            e.printStackTrace();
            System.out.println(e.getMessage());
            return null;
        });

        System.out.println("运算结果为:"+ cf.join());
    }

    private static void 获取结果(CompletableFuture<Integer> cf) {
        System.out.println(cf.getNow(100));//当运算还没出结果的时候 会返回100 否则返回运算结果的值

        //是否打断计算  true 打断了结果会成为我给的值 50
        System.out.println(cf.complete(50));
        System.out.println(cf.join());

        try {
            System.out.println(cf.get(1L,TimeUnit.SECONDS));//获取指定时间内结果否则报错
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (TimeoutException e) {
            e.printStackTrace();
        }

        try {
            System.out.println(cf.get());//一直等待获取结果
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }


        System.out.println(cf.join());//和get区别是   他不抛异常
    } //获取结果
}
