package Juc.深入.FutureDemo.CompletionStageAPI;

import java.util.concurrent.*;

public class CompletableFuture的结果合并 {
    public static void main(String[] args) {

        CompletableFuture<Integer> cf1 =
                CompletableFuture.supplyAsync(() -> {
                    int result = ThreadLocalRandom.current().nextInt(10);//随机获取[0-9]
                    try { TimeUnit.MILLISECONDS.sleep(1000); }catch (Exception e){e.printStackTrace();}
                    return result;
                });
        CompletableFuture<Integer> cf2 =
                CompletableFuture.supplyAsync(() -> {
                    int result = ThreadLocalRandom.current().nextInt(10);//随机获取[0-9]
                    try { TimeUnit.MILLISECONDS.sleep(1000); }catch (Exception e){e.printStackTrace();}
                    return result;
                });
        CompletableFuture<String> cfh = cf1.thenCombine(cf2, (x, y) -> {
            return "第一个值为:"+x +"\n第二个值为:"+ y+"\n两个值得和为:"+(x+y);
        });//结果合并
        System.out.println(cfh.join());


    }
}
