package Juc.深入.FutureDemo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.concurrent.ThreadLocalRandom;

@Data
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
class User {
    private Integer id;
    private String  username;
    private int     age;
    private double     ss;
    public User(Integer id,String username,int age){
        this.id=id;
        this.username=username;
        this.age=age;
        this.ss=ThreadLocalRandom.current().nextDouble();
    }
}