package Juc.深入.FutureDemo;

import java.util.concurrent.*;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

class MyTask extends RecursiveTask<Integer> {

    public static final Integer ADJUST_VALUE = 10;
    private  int begin;
    private  int end;
    private  int result;

    public MyTask(int begin, int end) {
        this.begin = begin;
        this.end = end;
    }

    @Override
    protected Integer compute(){
        if(end - begin<=ADJUST_VALUE){
            for (int i = begin; i <= end; i++) {
                result+=i;
            }
        }else{
            int middle=(end+begin)/2;
            MyTask task01=new MyTask(begin,middle);
            MyTask task02=new MyTask(middle+1,end);
            task01.fork();
            task02.fork();
            result=task01.join()+task02.join();
        }
        return result;
    }
}//把大问题   分割成一堆小问题

/**
 *
 * Stream自己不会存储
 * Stream  流式计算 延迟执行
 * 集合讲的是数据   流讲的是计算
 *
 *
 * ForkJoinPool
 * ForkJoinTask
 *
 */
public class JUF函数式接口 {//函数值接口

    private static void ForkJoin_Demo() throws InterruptedException, ExecutionException {

        MyTask myTask=new MyTask(0,100);

        ForkJoinPool forkJoinPool =new ForkJoinPool();//分支合并池子  也是一种线程池
        ForkJoinTask<Integer> forkJoinTask = forkJoinPool.submit(myTask);

        System.out.println(forkJoinTask.get());
        forkJoinPool.shutdown();//关闭池子
    }
    //分支合并框架
    private static void InterfaceDemo() {
        Function<String,Integer> function=s->{return  s.length();}; //函数型接口
        Function<String,Integer> function1=String::length; //方法引用的函数型接口
        System.out.println(function.apply("abc"));

        Predicate<String> predicate=s->{return s.isEmpty();}; //bool型接口
        Predicate<String> predicate1=String::isEmpty; //方法引用的bool型接口
        System.out.println(predicate.test("hh"));

        Consumer<String> consumer=s->{System.out.println(s);}; //消费性接口
        Consumer<String> consumer1= System.out::println; //方法引用的bool消费性接口
//        Consumer<String> consumer2= PrintStream::println; //虽然println是PrintStream类的 但是用这个System.out
        consumer.accept("傻逼");

        Supplier<String> supplier=()->{return "你猜";}; //供给型接口
        Supplier<String> supplier1=()-> "你猜";//供给型接口
        Supplier<User> supplier2=User :: new;//供给型接口

        System.out.println(supplier2.get());

    }  //四大函数式接口  juf

}
