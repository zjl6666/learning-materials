package Juc.基础;

import org.apache.tomcat.util.collections.SynchronizedQueue;

import java.util.concurrent.*;

public class 阻塞队列 {
    public static void main(String[] args) throws InterruptedException{
        BlockingQueue_demoQueue();
    }

    private static void BlockingQueue_demoQueue() throws InterruptedException {


        /**
         * 线程池用的阻塞队列
         * 1、ArrayBlockingQueue：由数组结构组成的有界阻塞队列  默认10
         *
         * 2、LinkedBlockingQueue：由链表组成的有界(但大小默认值为Integer.MAX_Value（21亿多)
         *
         * 3、PriorityBlockingQueue：支持优先级排序的无界阻塞队列
         *
         * 4、DelayQueue：使用优先级队列实现的延迟无界阻塞队列
         *
         * 5、SynchronizedQueue：不存储元素的阻塞队列，也即单个元素的队列
         *
         * 6、LinkedTransferQueue：由链表结构组成的无界阻塞队列
         *
         * 7、LinkedBlockingDeque：由链表结构组成的双向阻塞队列
         *
         * 其中橘色的三种是常用的。其中 LinkedBlockingQueue 和
         * SynchronizedQueue 是两个极端，
         * SynchronizedQueue 是没有容量的阻塞队列，
         * 而 LinkedBlockingQueue 在未指定容量时可以看作是容量无穷大的阻塞队列。
         *
         *
         */

        BlockingQueue<String> blockingQueue=new ArrayBlockingQueue<>(1);

        /*
            System.out.println(blockingQueue.add("A"));
            //多于3个java.lang.IllegalStateException
            System.out.println(blockingQueue.remove());
            //把值取出来并删除 java.util.NoSuchElementException  没了还删  没法删异常
            System.out.println(blockingQueue.element());
            //把值取出来不删除 java.util.NoSuchElementException  没了还取  没法取异常

         */



//        System.out.println(blockingQueue.offer("B0"));
//        System.out.println(blockingQueue.offer("B1"));
//        //添加  多于1个 返回false 不添加.
//        System.out.println(blockingQueue.peek());//查看队头元素   不删除 没有返回null
//        System.out.println(blockingQueue.poll());
//
//        System.out.println(blockingQueue.poll());
//        //删除  没有 返回false 不删除


        System.out.println(blockingQueue.offer("B",3L, TimeUnit.SECONDS));//等一定时间 在时间内没位置等这添加  到时间后结束
        System.out.println(blockingQueue.offer("B",3L,TimeUnit.SECONDS));


        new Thread(()->{
            try {
                TimeUnit.SECONDS.sleep(1);
                System.out.println(blockingQueue.take());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }).start();

        new Thread(()->{
            try {
                TimeUnit.SECONDS.sleep(2);
                blockingQueue.put("傻逼3333");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }).start();
        blockingQueue.put("傻逼");//如果有位置  就添加  如果没 等着直到有
        blockingQueue.put("傻逼2");//如果有位置  就添加  如果没 等着直到有
        System.out.println(blockingQueue.take());//取值  没了一直等着取
        System.out.println(blockingQueue.take());//取值  没了一直等着取
//        System.out.println(blockingQueue.take());//取值  没了一直等着取

    }  //队列

}
