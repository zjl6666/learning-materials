package JavaN.Java8.Lambda表达式;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

@Data
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
class User {
    private Integer id;
    private String  username;
    private int     age;
}

class MyTask extends RecursiveTask<Integer> {

    public static final Integer ADJUST_VALUE = 10;
    private  int begin;
    private  int end;
    private  int result;

    public MyTask(int begin, int end) {
        this.begin = begin;
        this.end = end;
    }

    @Override
    protected Integer compute(){
        if(end - begin<=ADJUST_VALUE){
            for (int i = begin; i <= end; i++) {
                result+=i;
            }
        }else{
            int middle=(end+begin)/2;
            MyTask task01=new MyTask(begin,middle);
            MyTask task02=new MyTask(middle+1,end);
            task01.fork();
            task02.fork();
            result=task01.join()+task02.join();
            System.out.println(Thread.currentThread().getName());
        }
        return result;
    }
}//把大问题   分割成一堆小问题

/**
 *
 * Stream自己不会存储
 * Stream  流式计算 延迟执行
 * 集合讲的是数据   流讲的是计算
 *
 *
 * ForkJoinPool
 * ForkJoinTask
 *
 */
public class JUF函数式接口 {//函数值接口

    public static void main(String[] args) throws ExecutionException, InterruptedException {
        ForkJoin_Demo();
    }

    private static void CompletableFutureDemo() throws InterruptedException, ExecutionException {
        CompletableFuture<Void> completableFuture = CompletableFuture.runAsync(() -> {
//            int age=10/0;
            System.out.println(Thread.currentThread().getName() + "没有返回值");
        });

//        System.out.println(completableFuture.get());

        CompletableFuture<Integer> integerCompletableFuture = CompletableFuture.supplyAsync(() -> {
            System.out.println(Thread.currentThread().getName() + "有  返  回");
            int age=10/0;
            return 1024;
        });
        System.out.println(integerCompletableFuture.whenComplete((t, u) -> {
            System.out.println("****t\t" + t);//null   因为出异常了  这个return没有执行   如果没异常t==返回的值
            System.out.println("****u\t" + u);//返回异常信息  没异常  返回null
        }).exceptionally(f -> {//异常处理
            System.out.println("excpyion\t" + f.getMessage());
            //excpyion	java.lang.ArithmeticException: / by zero
            return 4444;//重新定义一个返回值
        }).get());
    }

    //分支合并框架
    private static void ForkJoin_Demo() throws InterruptedException, ExecutionException {

        MyTask myTask=new MyTask(0,100);

        ForkJoinPool forkJoinPool =new ForkJoinPool();//分支合并池子  也是一种线程池
        ForkJoinTask<Integer> forkJoinTask = forkJoinPool.submit(myTask);

        System.out.println(forkJoinTask.get());
        forkJoinPool.shutdown();//关闭池子
    }

    private static void InterfaceDemo() {
        Function<String,Integer> function=s->{return  s.length();}; //函数型接口
        Function<String,Integer> function1=String::length; //方法引用的函数型接口
        System.out.println(function.apply("abc"));

        Predicate<String> predicate=s->{return s.isEmpty();}; //bool型接口
        Predicate<String> predicate1=String::isEmpty; //方法引用的bool型接口
        System.out.println(predicate.test("hh"));

        Consumer<String> consumer=s->{System.out.println(s);}; //消费性接口
        Consumer<String> consumer1= System.out::println; //方法引用的bool消费性接口
//        Consumer<String> consumer2= PrintStream::println; //虽然println是PrintStream类的 但是用这个System.out
        consumer.accept("傻逼");

        Supplier<String> supplier=()->{return "你猜";}; //供给型接口
        Supplier<String> supplier1=()-> "你猜";//供给型接口
        Supplier<User> supplier2=User :: new;//供给型接口

        System.out.println(supplier2.get());

    }  //四大函数式接口  juf

}
