package JavaN.Java8.Lambda表达式;

import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.function.BiFunction;
import java.util.function.Function;

/**
 * 引用的使用
 * 1.使用情境：当要传递给Lambda体的操作，已经有实现的方法了，可以使用方法引用！
 * 2.方法引用，本质上就是Lambda表达式，而Lambda表达式作为函数式接口的实例。所以
 *   方法引用，也是函数式接口的实例。
 * 3. 使用格式：  类(或对象) :: 方法名
 * 4. 具体分为如下的三种情况：
 *    情况1     对象 :: 非静态方法
 *    情况2     类 :: 静态方法
 *
 *    情况3     类 :: 非静态方法
 * 5. 方法引用使用的要求：要求接口中的抽象方法的形参列表和返回值类型与方法引用的方法的
 *    形参列表和返回值类型相同！（针对于情况1和情况2）
 *
 * 一、构造器引用
 *      和方法引用类似，函数式接口的抽象方法的形参列表和构造器的形参列表一致。
 *      抽象方法的返回值类型即为构造器所属的类的类型
 * 二、数组引用
 *     大家可以把数组看做是一个特殊的类，则写法与构造器引用一致。
 *
 */
@FunctionalInterface
 interface Methods{
    int max(int x,int y);

}


public class 方法数组构造器引用 {
    public static void main(String[] args) {
        Methods methods = new Methods() {
            @Override
            public int max(int x, int y) {
                return Math.max(x,y);
            }
        };
        Methods methods2 = ((x, y) -> {return Math.max(x,y);});
        Methods methods3 = (x, y) -> Math.max(x,y);
        Methods methods4 = Math::max;

        System.out.println(methods.max(10, 60));


        Comparator<String> comparator = (s1,s2)->s1.compareTo(s2);
        Comparator<String> comparator2 = String::compareTo;//方法引用

        Function<Integer,String[]> function = a->new String[a];
        Function<Integer,String[]> function1 = String[]::new;//数组引用




    }
}
