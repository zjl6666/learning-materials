package JavaN.Java8;

/**
 * Java 8 (又称为jdk 1.8)是Java语言开发的一个主要版本。
 * Java 8是oracle公司于2014年3月发布，可以看成是自Java 5以
 * 来最具革命性的版本。Java 8为Java语言、编译器、类库、开发
 * 工具与JVM带来了大量新特性。
 *
 * ●速度更快
 * ●代码更少(增加了新的语法: Lambda 表达式)
 * ●强大的Stream API
 * ●便于并行
 * ●最大化减少空指针异常: Optional
 * ●Nashorn引擎，允许在JVM.上运行JS应用
 */
public class Main {
    public static void main(String[] args) {
    }
}
