package 反射;


import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * 类加载器
 * 引导类加载器:用C++编写的，是JVM自带的类加载器，
 *          负责Java平台核心库，用来装载核心类
 *          库。该加载器无法直接获取
 * 扩展类加载器:负责ie/ib/ex目录下的jar包或
 *          -D java.ext.dirs指定目录下的ar包装入工作库
 * 系统类加载器:负责java -classpath或
 *          -D java.class.path所指的目录下的类与
 *          jar包装入工作，是最常用的加载器
 */
public class ClassloaderTest {
    public static void main(String[] args) throws FileNotFoundException {
        配置文件();
    }

    private static void 配置文件() throws FileNotFoundException {
        Properties pros =  new Properties();
        //此时的文件默认在当前的module下。
        //读取配置文件的方式一：
//        FileInputStream fis = new FileInputStream("jdbc.properties");
//        FileInputStream fis = new FileInputStream("src\\jdbc1.properties");
//        try {pros.load(fis);} catch (IOException e) {e.printStackTrace();}

        //读取配置文件的方式二：使用ClassLoader
        //配置文件默认识别为：当前module      的src下
        ClassLoader classLoader = ClassloaderTest.class.getClassLoader();//必须在当前module的src下
        InputStream is = classLoader.getResourceAsStream("jdbc.properties");
        try {
            pros.load(is);
        } catch (IOException e) {
            e.printStackTrace();
        }


        String user = pros.getProperty("name");
        String password = pros.getProperty("age");
        System.out.println("name = " + user + ",password = " + password);
        try {
            is.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void 类加载器的顺序() {
        ClassLoader classLoader = ClassloaderTest.class.getClassLoader();
        System.out.println(classLoader);
        //sun.misc.Launcher$AppClassLoader@18b4aac2//系统类加载器

        System.out.println(classLoader.getParent());
        //sun.misc.Launcher$ExtClassLoader@61bbe9ba//扩展类加载器

        System.out.println(classLoader.getParent().getParent());
        //null//引导类加载器获取不到  主要负责加载java的核心类库
    }

    private static void ClassLoaderDemo() {
        Object object =new Object();
        //System.out.println(object.getClass().getClassLoader().getParent().getParent());
        //System.out.println(object.getClass().getClassLoader().getParent());
        System.out.println(object.getClass().getClassLoader());//根加载器 无

        ClassloaderTest jvm=new ClassloaderTest();

        System.out.println(jvm.getClass().getClassLoader().getParent().getParent());//jvm的上上一个装载
        System.out.println(jvm.getClass().getClassLoader().getParent());//jvm的上一个装载器
        System.out.println(jvm.getClass().getClassLoader());//jvm是谁装载的
    } //类装载器
}
