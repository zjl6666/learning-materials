package 反射;

import lombok.Data;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.HashMap;
import java.util.List;

@SuppressWarnings("846")
public class Fs extends HashMap<String,Integer> implements Comparable<String> {
    private String name;
    protected String namess;
    String names;
    public int age;

    public Fs() {
    }

    public Fs(String name, int age) {
        this.name = name;
        this.age = age;
    }
    private Fs (int age){
        this.age=age;
        this.name = "默认名字";
    }
    private Fs (String name){
        this.name = name;
    }

    @Override
    public String toString() {
        return "Fs{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
    private void show(){
        System.out.println("我是私有的");
    }

    @Deprecated//表示已废弃
    private static void show(int a){
        System.out.println("我是静态的"+a);
    }
    private String show(String gj){
        System.out.println("我的国籍是"+gj);
        return gj;
    }

    public String getName() throws Exception {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public int compareTo(String o) {
        return 0;
    }
}
