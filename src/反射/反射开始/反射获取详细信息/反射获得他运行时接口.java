package 反射.反射开始.反射获取详细信息;

import 反射.Fs;

public class 反射获得他运行时接口 {
    public static void main(String[] args) {
        运行时接口();
    }

    private static void 运行时接口() {
        Class<Fs> fsClass = Fs.class;
        System.out.println("获得他运行时**********的接口**********");
        Class<?>[] interfaces = fsClass.getInterfaces();
        for (Class c:interfaces){
            System.out.println(c);
        }
        System.out.println("获得运行时他父类*******的接口**********");
        Class<?>[] interfaces1 = fsClass.getSuperclass().getInterfaces();
        for (Class c:interfaces1){
            System.out.println(c);
        }
    }
}
