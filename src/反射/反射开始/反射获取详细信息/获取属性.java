package 反射.反射开始.反射获取详细信息;

import 反射.Fs;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
//如修饰符   类型   变量名 等
public class 获取属性 {
    public static void main(String[] args) {
        反射获取属性();
    }

    private static void 反射获取属性() {
        Class<Fs> fsClass = Fs.class;
        Field[] fields = fsClass.getFields();
        //只能获取当前运行时类及父类的public属性
        for (Field f:fields){
            System.out.println("public的属性:"+f);
        }

        System.out.println("***************************************************");
        Field[] declaredFields = fsClass.getDeclaredFields();
        //获取当前运行时类中声明的所有属性。（不包含父类中声明的属性）
        for (Field f:declaredFields){
            System.out.println("*****************详细信息*************");
            System.out.println(f);////当前运行时类中声明的所有属性
            System.out.println(f.getModifiers()+//2 private
                    "对相应的修饰符"+//1  public    0:缺省   4:protected
                    //如果   有static或者volatile或者final或者等等等等  会有不同的值
                    Modifier.toString(f.getModifiers()));//获得f的权限修饰符
            Class<?> type = f.getType();
            System.out.println(type);//类型

            System.out.println("变量名"+f.getName());//变量名
        }
    }
}
