package 反射.反射开始.反射获取详细信息;

import 反射.Fs;

import java.lang.reflect.Constructor;

public class 反射获取构造器 {
    public static void main(String[] args) {
        获取构造器();
    }

    private static void 获取构造器() {
        Class<Fs> fsClass = Fs.class;
        System.out.println("***获取运行实例中public构造器*****如下**************************");
        final Constructor<?>[] constructors = fsClass.getConstructors();
        for (Constructor c:constructors){
            System.out.println(c);
        }
        System.out.println("***获取运行实例中所有构造器*****如下**************************");
        final Constructor<?>[] declaredConstructors = fsClass.getDeclaredConstructors();
        for (Constructor c:declaredConstructors){
            System.out.println(c);
        }
    }

}
