package 反射.反射开始.反射获取详细信息;

import 反射.Fs;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

public class 反射获取运行实例中父类 {
    public static void main(String[] args) {
        反射获取运行实例中父类();
    }

    private static void 反射获取运行实例中父类() {
        Class<Fs> fsClass = Fs.class;
        System.out.println("******获取获取运行实例中的父类************");
        Class<? super Fs> superclass = fsClass.getSuperclass();
        System.out.println(superclass);//获取获取运行实例中的父类
        System.out.println("******获取运行实例中带泛型的父类************");
        Type type = fsClass.getGenericSuperclass();
        System.out.println(type);//取获取运行实例中带泛型的父类
        System.out.println("******获取运行实例中父类的泛型************");
        ParameterizedType paramType = (ParameterizedType)type;//参数
        System.out.println(paramType);//要转成ParameterizedType  再转成Type[]
        Type[] types = paramType.getActualTypeArguments();
        //((ParameterizedType) type).getActualTypeArguments();
        for (Type t:types){
            System.out.println(t.getTypeName());//getTypeName()去掉class
        }//遍历泛型类型
    }

}
