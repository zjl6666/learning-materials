package 反射.反射开始.反射获取详细信息;

import 反射.Fs;

import java.lang.annotation.Annotation;


public class 反射获得他运行时包和注解 {
    public static void main(String[] args) throws Exception {
        反射获得他运行时包和注解();
        // TODO: 2021/11/2 java反射获取import的导入 获取不到
    }

    private static void 反射获得他运行时包和注解() {
        Class<Fs> fsClass = Fs.class;

        Package aPackage = fsClass.getPackage();//
        System.out.println("获得他所在的包"+aPackage);
        Annotation[] annotations = fsClass.getAnnotations();
        for (Annotation a:annotations){
            System.out.println(a);
        }//遍历他的注释
    }

}
