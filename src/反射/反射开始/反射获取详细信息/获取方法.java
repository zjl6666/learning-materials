package 反射.反射开始.反射获取详细信息;

import 反射.Fs;

import java.lang.annotation.Annotation;
import java.lang.reflect.AnnotatedType;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Arrays;

/**
 *
 * @Xxxx s
 * 修饰符 返回值类型 方法名（ 参数类型s  参数名） 抛出的异常s
 * Annotations 注释
 * Modifiers   修饰语
 *  Parameter  参数
 */
public class 获取方法 {
    public static void main(String[] args) {
        反射获取方法();
    }

    private static void 反射获取方法() {
        Class<Fs> fsClass = Fs.class;

        System.out.println("***只能获取当前运行时类及父类(可以套娃)的public的方法*******************************");
        Method[] methods = fsClass.getMethods();
        for (Method m:methods){
            System.out.println(m);
        }
        System.out.println("**获取当前运行时类中声明的所有方法。（不包含父类中声明的方法）***************************");
        final Method[] methods1 = fsClass.getDeclaredMethods();
        for (Method m:methods1){
//            System.out.println(m);
            Annotation[] annotations = m.getAnnotations();//获取当前方法注解
            for (Annotation a : annotations){
                System.out.println("注解" + a);
            }//@Retention(RetentionPolicy.RUNTIME)//只有声明为RUNTIME生命周期的注解，才能通过反射获取。

            System.out.print(Modifier.toString(m.getModifiers()) + " ");//方法的修饰符（public/start等）
            System.out.print(m.getReturnType().getName() + " ");//获取返回值类型   .getName()   去掉了class
            System.out.print(m.getName() + "(");//返回m的方法名
            Class<?>[] parameterTypes = m.getParameterTypes();//获取全部参数类型
            if (!(parameterTypes.length == 0)){
                for (Class c:parameterTypes){
                    System.out.print(c.getName() + " ");
                }//遍历参数类型
            }
            System.out.print(")");
            Class<?>[] exceptionTypes = m.getExceptionTypes();//获得异常
            if(!(exceptionTypes.length == 0)){
                System.out.print(" throws ");
                for (Class c : exceptionTypes){
                    System.out.print(c.getName() + " ");
                }//遍历异常
            }

            System.out.println();
        }
    }
}
