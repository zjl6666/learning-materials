package 反射.反射开始;

import 反射.Fs;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;


public class 获取类的指定结构 {
    public static void main(String[] args) throws InstantiationException, IllegalAccessException, NoSuchMethodException, InvocationTargetException {
        获取指定名的方法();


    }

    private static void 获取构造器()  {
        /**
         * .newInstance  只有class和构造器Constructor  有这个方法  相当于 new 类
         *
         * Accessible 可到达的
         */
        try {
            Class<Fs> fsClass = Fs.class;
            //通过反射可以调用私用结构
            //调用私有构造器
            Constructor consS = fsClass.getDeclaredConstructor(int.class);//获取私有的构造器
            consS.setAccessible(true);//保证当前构造器可访问的
            Fs oS = (Fs) consS.newInstance(15);//相当于new Fs(15);
            System.out.println(oS);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    private static void 获取指定名的方法() {
        try {
            Class<Fs> fsClass = Fs.class;
            Fs fs = fsClass.newInstance();//相当于new Fs();
            Method show = fsClass.getDeclaredMethod("show", String.class);//获得方法
            show.setAccessible(true);//加这个  可以调用私有的
            String chn = (String) show.invoke(fs, "中国");//执行fs  的show方法
            System.out.println(chn);
            System.out.println("反射执行静态方法**************************");
            Method show2 = fsClass.getDeclaredMethod("show", int.class);//获得方法
            show2.setAccessible(true);//加这个  可以调用私有的
            show2.invoke(fsClass,5);//执行静态
            // 等同于show2.invoke(null,5);因为静态 他只有一个  不需要加Fs的某个
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    private static void 获取指定名的属性() {
        try {
            Class<Fs> fsClass = Fs.class;
//            Field age = fsClass.getField("age");//只能找到public的
            Field age = fsClass.getDeclaredField("age");//能找到所有的的
            age.setAccessible(true);//保证当前属性可访问的 除了public  其他都要加
            System.out.println(age);//public int 反射.Fs.age
            System.out.println("*******相当于new Fs();******************");
            Object tom = fsClass.newInstance();//相当于new Fs();
            Fs fs = (Fs)tom;
            age.set(fs,10);//将fs里的age属性的值修改
            System.out.println(fs);
            System.out.println("*******获得fs的指定属性的值******************");
            int page = (int) age.get(fs);//fs.getAge()的反向
            System.out.println(page);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }
}
