package 笔试;

import java.util.ArrayList;
import java.util.List;

public class 每日1 {
    public static void main(String[] args) {
        每日1 m = new 每日1();
        int[] stones ={6,4,4,6};
        System.out.println(m.mergeStones(stones, 2));
    }


    public int mergeStones(int[] stones, int k) {
        if (k > 2 && stones.length % (k - 1) != 1) {
            return -1;
        }
        int h = 0;
        int len = stones.length;
        List<Integer> l = new ArrayList<>();
        List<Integer> ls = new ArrayList<>();
        for (int i = 0; i < len; i++) {
            ls.add(stones[i]);
        }
        for (int i = 0; i <= len - k ; i++) {
            int he = 0;
            for (int j = 0; j < k; j++) {
                he += stones[i + j];
            }
            l.add(he);
        }
        while (l.size() != 0) {
            int min = min(l);//最小值角标
            int m = l.get(min);//最小值
            h += m;
            ls.set(min,m);
            for (int i = 1; i < k; i++) {
                ls.remove(min + 1);
            }

            l = new ArrayList<>();
            for (int i = 0; i <= ls.size() - k ; i++) {
                int he = 0;
                for (int j = 0; j < k; j++) {
                    he += ls.get(i + j);
                }
                l.add(he);
            }

        }
        return h;
    }

    private int min(List<Integer> l) {
        int m = 0;
        for (int i = 1; i < l.size(); i++) {
            if (l.get(i) < l.get(m)) {
                m = i;
            }
        }
        return m;
    }
}
