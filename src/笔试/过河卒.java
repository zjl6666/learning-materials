package 笔试;

import java.util.Scanner;

public class 过河卒 {
    private static int k = 0;
    private static int mx = 0;
    private static int my = 0;
    private static int zx = 0;
    private static int zy = 0;
    private static int b[][] = new int[21][21];
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        zx=sc.nextInt();
        zy=sc.nextInt();//终点
        mx=sc.nextInt();//马的位置
        my=sc.nextInt();
        b[mx][my]=1;

        if(mx>=2){
            if(my>=1) b[mx-2][my-1]=1;
            if(my+1<=zy) b[mx-2][my+1]=1;
        }
        if(mx>=1){
            if(my>=2) b[mx-1][my-2]=1;
            if(my+2<=zy) b[mx-1][my+2]=1;
        }
        if(mx+2<=zx){
            if(my>=1) b[mx+2][my-1]=1;
            if(my+1<=zy) b[mx+2][my+1]=1;
        }
        if(mx+1<=zx){
            if(my>=2) b[mx+1][my-2]=1;
            if(my+2<=zy) b[mx+1][my+2]=1;
        }

//        dfs(0,0);
//        System.out.println(k);

    }
    private static void dfs(int x,int y){
        if(x == zx && y == zy){ k++; return;}
        if(x==mx&&y==my){return;}
        if((Math.abs(x-mx)+Math.abs(y-my))==3&&(x!=mx&&y!=my)){
            return;
        }
        if(x+1<=zx) dfs(x+1,y);
        if(y+1<=zy) dfs(x,y+1);
        return;
    }
}
