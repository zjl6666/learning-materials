package 笔试;

public class 递归 {
    static int k=3;//分为三组
    static int[][] a = new int[3][];
    static int[][] b = new int[3][];
    static int[] z = new int[3];
    static int[] m = new int[]{854,564,8,564,654,78,86};
    static int min = 999999999;//最小
    static int min2 = 0;//最小
    static int pj = 0;//平均
    public static void main(String[] args) {
        for (int i = 0; i < m.length; i++) {
            pj+=m[i];
        }
        pj/=k;
        System.out.println(pj);
        a[0]=new int[2];
        b[0]=new int[2];
        a[1]=new int[2];
        b[1]=new int[2];
        a[2]=new int[3];
        b[2]=new int[3];
        z[0]=0;
        z[1]=0;
        z[2]=0;//保存第几个数组运到第几个数了
        dp(0);
        for (int i = 0; i < k; i++) {
            for (int j = 0; j < b[i].length; j++) {
                System.out.print(b[i][j]+"\t");
            }
            System.out.println();
        }
    }
    private static void dp(int s){//s表示是现在运行到第几个数了  0开始
        if(s>=m.length){
            for (int i = 0; i < k; i++) {
                int q=0;
                for (int j = 0; j < a[i].length; j++) {
                    q+=a[i][j];
                }
                min2+=(pj-q)*(pj-q);//算出差值
            }
            if(min2<min) {
                min=min2;
                for (int i = 0; i < k; i++) {//保存起来
                    for (int j = 0; j < a[i].length; j++) {
                        b[i][j]=a[i][j];
                    }
                }
                System.out.println(min2);
            }

            min2=0;
            return;
        }

        for (int i = 0; i < k; i++) {
            if(a[i].length>z[i]){
                a[i][z[i]]=m[s];
                z[i]++;
                dp(s+1);
                z[i]--;//返回
            }
        }
    }
}
