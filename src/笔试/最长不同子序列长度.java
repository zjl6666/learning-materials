package 笔试;

import java.util.HashSet;
import java.util.Set;

public class 最长不同子序列长度 {
    public static void main(String[] args) {
        String s = "wagsdrthdjhsaeRSDHS";
        int begin=0,max=0;
        Set<Character> set = new HashSet<>();
        for(int i=0;i<s.length();i++){
            if(set.contains(s.charAt(i))){
                max=Math.max(max,i-begin);
                while (s.charAt(begin)!=s.charAt(i)&&begin<s.length()){
                    set.remove(s.charAt(begin));
                    begin++;
                }
                begin++;
            }else{
                set.add(s.charAt(i));
            }
        }
        max=Math.max(max,s.length()-begin);
        System.out.println(max);

    }
}
