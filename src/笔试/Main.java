package 笔试;

import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        System.out.println(Math.pow(10, 5));
    }
    public int lengthOfMaxQueue (ArrayList<Integer> nums) {
        // write code here
        int m = nums.size();
        int max=0;

        int[] a= new int[m];
        for (int i = 0; i < m; i++) {
            a[i]=1;
            for (int j = 0; j < i; j++) {
                if(nums.get(j)<nums.get(i)){
                    a[i]=Math.max(a[i],a[j]+1);
                }
            }
            max=Math.max(max,a[i]);
        }
        return max;
    }

    public int GiveChargeSum (int N) {
        // write code here
        int max=0;
        for (int i = 0; i < N/10; i++) {
            for (int j = 0; j < (N-i*10)/5; j++) {
                for (int k = 0; k < ((N - j * 5 - i * 10)) / 2; k++) {
                    max++;
                }
            }
        }
        return max;
    }
}
