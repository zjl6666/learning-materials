package 笔试;

import base.Java关键字.枚举enum;
import lombok.Getter;

public enum CountryEnum {
        ONE(1,"齐国"),TWO(2,"楚国"),
        THREE(3,"燕国"),FOUR(4,"赵国"),
        FIVE(5,"魏国"),SIX(6,"韩国");
        @Getter
        private Integer retCode;
        @Getter private String retMessage;

        CountryEnum(Integer retCode,String retMessage) {
            this.retMessage = retMessage;
            this.retCode = retCode;
        }

        public static 枚举enum.CountryEnum forEach_CountryEnum(int index){

            枚举enum.CountryEnum[] myArray = 枚举enum.CountryEnum.values();
            for(枚举enum.CountryEnum countryEnum:myArray){
                if(index==countryEnum.getRetCode()){
                    return countryEnum;
                }
            }
            return null;
        }
      //枚举
}
