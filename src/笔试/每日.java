package 笔试;

import java.util.*;
import java.util.concurrent.PriorityBlockingQueue;

public class 每日 {
    public static void main(String[] args) {
        每日 m = new 每日();

//        int[][] grid = {{0,2,0,0,0,0,0},{0,0,0,2,2,1,0},{0,2,0,0,1,2,0},{0,0,2,2,2,0,2},{0,0,0,0,0,0,0}};
        int[][] grid = {{0,2,0,0,1},{0,2,0,2,2},{0,2,0,0,0},{0,0,2,2,0},{0,0,0,0,0}};
        m.maximumMinutes(grid);


    }

    private static String w(int n) {
        if(n == 0)
            return "0";
        StringBuilder s = new StringBuilder();
        boolean f = true;//是否是正数
        int w = 1;//
        int r = 0;//最终值
        while (n >= w){
            if(f){//正数
                if((n&w)!=0){//当前位数 不是0
                    if ((r&w)!= 0){//当前位数 不是0
                        r ^= w;
                        r |= (w<<1);
                        r |= (w<<2);
                    }else {
                        r |= w;
                    }
                }
            }else {//负数
                if((n&w)!=0){//当前位数 不是0
                    if ((r&w)!= 0){//当前位数 不是0
                        r^=w;
                    }else {
                        r |= (w<<1);
                        r |= w;
                    }
                }
            }
            f=!f;
            w<<=1;
        }
        return Integer.toBinaryString(r);
    }

    private static String ww(int n,int m) {
        StringBuilder s = new StringBuilder();
        int y = 0;
        while (n!=0){
            y = Math.abs(n%m);
            s.append(y);
            n-=(y);
            n/=m;
        }
        return s.reverse().toString();
    }

    public boolean db(int[][] grid,int[][] run,int h,int c,int hh,int cc){//h c  当前位置
        if(h == hh && c == cc){
            return true;
        }
        boolean b = false;

        int k = run[h][c];
        if(h-1>=0 && grid[h-1][c] == 0){
            if((k+1)<run[h-1][c]){
                run[h-1][c] = k+1;
                if(db(grid,run,h-1,c,hh,cc)){
                    b=true;
                }
            }
        }
        if(h+1<=hh && grid[h+1][c] == 0){
            if((k+1)<run[h+1][c]){
                run[h+1][c] = k+1;
                if(db(grid,run,h+1,c,hh,cc)){
                    b=true;
                }
            }
        }
        if(c-1>=0 && grid[h][c-1] == 0){
            if((k+1)<run[h][c-1]){
                run[h][c-1] = (k+1);
                if(db(grid,run,h,c-1,hh,cc)){
                    b=true;
                }
            }
        }
        if(c+1<=cc && grid[h][c+1] == 0){
            if((k+1)<run[h][c+1]){
                run[h][c+1] = k+1;
                if(db(grid,run,h,c+1,hh,cc)){
                    b=true;
                }
            }
        }
        if(!b){
            run[h][c] = Integer.MAX_VALUE;
        }
        return b;
    }

    public int maximumMinutes(int[][] grid) {
        int h = grid.length;
        int c = grid[0].length;
        System.out.println("---原始--------------------");
        for (int i=0;i<h;i++){
            for (int j=0;j<c;j++){
                System.out.print(grid[i][j] + "\t");
            }
            System.out.println();
        }
        int[][] run = new int[grid.length][grid[0].length];
        for (int i=0;i<h;i++){
            for (int j=0;j<c;j++){
                run[i][j] = Integer.MAX_VALUE;
            }
        }
        run[0][0] = 0;

        db(grid,run,0,0,h-1,c-1);
        if(run[h-1][c-1]==Integer.MAX_VALUE){
            return -1;
        }
        System.out.println("---人--------------------");
        for (int i=0;i<h;i++){
            for (int j=0;j<c;j++){
                System.out.print(run[i][j] + "\t");
            }
            System.out.println();
        }
        System.out.println("-----------------------");
        int min = Integer.MAX_VALUE;
        for (int i=0;i<h;i++){
            for (int j=0;j<c;j++){
                if(grid[i][j]==1){
                    run[i][j] = -1;
                    min = Math.min(min,dbHuo(grid,run,i,j,h-1,c-1,1,run[h-1][c-1]));
                }
            }
        }
        System.out.println(min);
        System.out.println("-----火------------------");
        for (int i=0;i<h;i++){
            for (int j=0;j<c;j++){
                System.out.print(run[i][j] + "\t");
            }
            System.out.println();
        }
        return 1000000000;
    }

    public int dbHuo(int[][] grid,int[][] run,int h,int c,int hh,int cc,int time,int end){//h c  当前位置
        if(h == hh && c == cc){
            return end - time +1;
        }
        int minTime = Integer.MAX_VALUE;
        int k = -run[h][c];
        if(h-1 >= 0 && grid[h-1][c] != 2){
            if(run[h-1][c] != Integer.MAX_VALUE&&run[h-1][c] >=0){
                 minTime = Math.min(minTime,time-run[h-1][c]-1);
            }else if(run[h-1][c] == Integer.MAX_VALUE || (k+1) < -run[h-1][c]){
                run[h-1][c] = -(k+1);
                minTime = Math.min(minTime,dbHuo(grid,run,h-1,c,hh,cc,time+1,end));
            }
        }
        if(h+1<=hh && grid[h+1][c] != 2){
            if(run[h+1][c] != Integer.MAX_VALUE&&run[h+1][c] >=0){
                minTime = Math.min(minTime,time-run[h+1][c]-1);
            }else if(run[h+1][c] == Integer.MAX_VALUE || (k+1) < -run[h+1][c]){
                run[h+1][c] = -(k+1);
                minTime = Math.min(minTime,dbHuo(grid,run,h+1,c,hh,cc,time+1,end));
            }
        }
        if(c-1>=0 && grid[h][c-1] != 2){
            if(run[h][c-1] != Integer.MAX_VALUE&&run[h][c-1] >=0){
                minTime = Math.min(minTime,time-run[h][c-1]-1);
            }else if(run[h][c-1] == Integer.MAX_VALUE || (k+1) < -run[h][c-1]){
                run[h][c-1] = -(k+1);
                minTime = Math.min(minTime,dbHuo(grid,run,h,c-1,hh,cc,time+1,end));
            }
        }
        if(c+1<=cc && grid[h][c+1] != 2){
            if(run[h][c+1] != Integer.MAX_VALUE&&run[h][c+1] >=0){
                minTime = Math.min(minTime,time-run[h][c+1]-1);
            }else if(run[h][c+1] == Integer.MAX_VALUE || (k+1) < -run[h][c+1]){
                run[h][c+1] = -(k+1);
                minTime = Math.min(minTime,dbHuo(grid,run,h,c+1,hh,cc,time+1,end));
            }
        }

        return minTime;
    }


    public int[] successfulPairs(int[] spells, int[] potions, long success) {
        int spellsLen = spells.length;
        int potionsLen = potions.length;
        Arrays.sort(potions);
        for (int i=0;i<spellsLen;i++){
            long k = success/spells[i] + (success%spells[i]==0?0:1)-1;
            spells[i] = potionsLen+binarySearch(potions,0,potionsLen-1,k);
        }
        return spells;
    }

    public int binarySearch(int[] arr, int begin, int end, long target) {
        int res = end + 1;
        while (begin <= end) {
            int mid = begin + (end - begin) / 2;
            if (arr[mid] > target) {
                res = mid;
                end = mid - 1;
            } else {
                begin = mid + 1;
            }
        }
        return res;
    }

    public int maxProduct(String[] words) {
        int len = words.length;
        int[] wor = new int[len];
        for (int i=0;i<len;i++){
            wor[i] = 0;
            char[] chars = words[i].toCharArray();
            for (int j=0;j<chars.length;j++){
                wor[i] |= (1<<(chars[j]-'a'));
            }
        }
        int max = 0;
        int z = 0;
        for (int i=0;i<len-1;i++){
            for (int j=i+1;j<len;j++){
                if((wor[i]&wor[j])==0){
                    z=words[i].length()*words[j].length();
                    if(z>max){
                        max = z;
                    }
                }
            }
        }
        return max;
    }

    public int[] smallestMissingValueSubtree(int[] parents, int[] nums) {
        int[] re = new int[parents.length];

        return re;
    }

    public int singleNumber(int[] nums) {
        int k = 0;
        for (int i:nums){
            k^=i;
        }
        return k;
    }

    public long pickGifts(int[] gifts, int k) {
        PriorityBlockingQueue<Integer> q = new PriorityBlockingQueue<>(gifts.length,(a, b) -> b - a);
        for (int i=0;i<gifts.length;i++){
            q.offer(gifts[i]);
        }
        while (k -->0){
            Integer remove = q.remove();
            q.add((int)Math.sqrt((double) remove));
        }
        int he = 0;
        for (int i=0;i<gifts.length;i++){
            he += q.remove();
        }
        return he;
//        System.out.println(q);
//        return 1L;
    }

    public int maxArea(int h, int w, int[] horizontalCuts, int[] verticalCuts) {
        Arrays.sort(horizontalCuts);
        Arrays.sort(verticalCuts);
        long maxh = 0;
        long maxw = 0;
        int sh = 0;
        int sw = 0;
        for(int i:horizontalCuts){
            if(i-sh>maxh){
                maxh = i-sh;
            }
            sh = i;
        }
        if(h-sh>maxh){
            maxh = h-sh;
        }
        for(int i:verticalCuts){
            if(i-sw>maxw){
                maxw = i-sw;
            }
            sw = i;
        }
        if(w-sw>maxw){
            maxw = w-sw;
        }
        return (int)((maxh*maxw) % 1000000007);
    }

    public long maxKelements(int[] nums, int k) {
        PriorityQueue<Integer> q = new PriorityQueue<Integer>(nums.length,(a, b) -> b - a);
        for (int i=0;i<nums.length;i++){
            q.add(nums[i]);
        }
        long he = 0;
        int q1 = 0;
        while (k-->0){
            System.out.println(q);
            int kk = q.remove();
            he += kk;
            if(kk % 3 == 0){
                q1 = 0;
            }else {
                q1 = 1;
            }
            q.add(kk/3 + q1);
        }
        return he;
    }

    public int maxSatisfaction(int[] satisfaction) {
        int begin = 0;
        int end = satisfaction.length -1;
//        Comparator<Integer> comparator = (s1, s2)-> s2.compareTo(s1);
        Arrays.sort(satisfaction);
        int[] z = new int[end+1];
        int zz = 0;
        for (int i = end ;i>=0;i--){
            z[i] = zz;
            zz+=satisfaction[i];
        }
        int he = 0;
        int k = 1;
        for (int i = 0;i<=end;i++){
            if(satisfaction[i]<0){
                if(-satisfaction[i] >= z[i]){
                    continue;
                }
            }
            he += satisfaction[i]*k;
            k++;
        }

        return he;

    }

    public int smallestRepunitDivByK(int k) {
        int m = 0;
        if(k%2 == 0||k%5 == 0) return -1;
        int[] s = new int[10];
        int z = 0;
        return 1;
    }

    public  boolean queryString(String s, int n) {
        StringBuilder sb = new StringBuilder();
        while (n>0){
            sb.append(n&1);
            n>>=1;
        }
        String ss = sb.reverse().toString();

        System.out.println(ss);

        return s.indexOf(ss)!=-1;
    }

    public int minNumberOfFrogs(String croakOfFrogs) {
        int[] g = new int[5];
        int max = 0;
        int d = 0;
        for (char c : croakOfFrogs.toCharArray()){
            switch (c){
                case 'c':
                    g[0]++;
                    d++;
                    break;
                case 'r':
                    g[0]--;
                    if(g[0]<0) return -1;
                    g[1]++;
                    break;
                case 'o':
                    g[1]--;
                    g[2]++;
                    if(g[1]<0) return -1;
                    break;
                case 'a':
                    g[2]--;
                    g[3]++;
                    if(g[2]<0) return -1;
                    break;
                case 'k':
                    d--;
                    g[3]--;
                    g[4]++;
                    if(g[3]<0) return -1;
                    break;
                default:
                    return -1;
            }
            if(d>max){
                max = d;
            }
        }
        for (int i = 0; i < 4; i++) {
            if(g[i]!=0) return -1;
        }
        return max;
    }

    public List<Boolean> camelMatch(String[] queries, String pattern) {
        List<Boolean> lb = new ArrayList<>(queries.length);
        int len = pattern.length();
        for (String s:queries){
            if(s.length()<len){
                lb.add(false);
                continue;
            }
            boolean b = true;
            int i=0;
            for (char c:s.toCharArray()){
                if(i<len){
                    if(c == pattern.charAt(i)){
                        i++;
                    }else if(c>='A'&&c<='Z'){
                        b=false;
                        break;
                    }
                }else {
                    if(c>='A'&&c<='Z'){
                        b=false;
                        break;
                    }
                }
            }
            if(i<len) b=false;
            lb.add(b);
        }
        return lb;
    }

    public int[] numMovesStonesII(int[] stones) {
        int[] m = {Integer.MAX_VALUE,0};//0最小   1最大
        int len = stones.length;
        Arrays.sort(stones);
        int j = 1;
        for (int i = 0; i < len-1; i++) {
            int k = stones[i]+len-1;//排到数字几
            int z = i + len-j;
            while (j!=len && stones[j]<=k){
                j++;
                z--;
            }
            if(j==len){
                if(stones[j-1]==k){
                    z--;
                    if(z>m[1]) m[1] = z;
                    if(z<m[0]) m[0] = z;
                }
                break;
            }
            if(z==1&&i==0) z++;

            if(z>m[1]) m[1] = z;
            if(z<m[0]) m[0] = z;
        }
        return m;
    }

    public int mergeStones(int[] stones, int k) {
        if (k > 2 && stones.length % (k - 1) != 1) {
            return -1;
        }
        int h = 0;
        int len = stones.length;
        List<Integer> l = new ArrayList<>();
        List<Integer> ls = new ArrayList<>();
        for (int i = 0; i < len; i++) {
            ls.add(stones[i]);
        }
        for (int i = 0; i <= len - k ; i++) {
            int he = 0;
            for (int j = 0; j < k; j++) {
                he += stones[i + j];
            }
            l.add(he);
        }
        while (l.size() != 0) {
            int min = min(l);//最小值角标
            int m = l.get(min);//最小值
            h += m;

            int he = 0;
            ls.set(min,m);
            for (int i = 1; i < k-1; i++) {

                int ss = k-1+i-1;
                he += ls.get(min+ss);
                if (min-i>= 0) {
                    l.set(min-i, l.get(min-i)+he);
                }
            }
            for (int i = 1; i < k; i++) {
                ls.remove(min + 1);
            }
            for (int i = 1; i < k; i++) {
                l.remove(min+1);
            }

        }
        return h;
    }

    private int min(List<Integer> l) {
        int m = 0;
        for (int i = 1; i < l.size(); i++) {
            if (l.get(i) < l.get(m)) {
                m = i;
            }
        }
        return m;
    }


}
