package 笔试;

public class 力扣 {
    public static void main(String[] args) {

    }
    public static int maxRotateFunction(int[] nums) {
        int max = 0;
        int a=0;
        int m=nums.length;
        for (int i = 0; i < m; i++) {
            max+=i*nums[i];
            a+=nums[i];
        }
        for (int i = m-1; i >0; i--) {
            max=Math.max(max,max+a-nums[i]*m);
        }
        return max;
    }//396. 旋转函数
    public static int[] numberOfLines(int[] widths, String s) {
        int count = 0;
        int sum = 0;
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            int a = c-'a';
            int width = widths[a];
            sum = sum+width;
            if (sum>100){
                count++;
                sum=width;
            }

        }

        return new int[]{++count,sum};

    }//806. 写字符串需要的行数
    public static boolean validUtf8(int[] data) {
        int k=0;
        for(int i=0;i<data.length;i++){
            if(k>0){
                if((192&data[i])==128){
                    k--;
                }else{ return false;
                }
            }else{
                for(int j=7;j>=0;j--){
                    if(((1<<j)&data[i])!=0){
                        k++;
                    }else {break;}
                }
                if(k==1) return false;
                if(k!=0) k--;
                if(k>7){return false;}
            }
        }
        if(k!=0){ return false; }
        return true;
    }
}
