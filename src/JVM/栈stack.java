package JVM;

/**
 * 6. stack  线程私有
 *    6.1 栈（stack）管运行  //内存小  线程 无垃圾回收
 *        堆(heap)管存储    //内存大  有垃圾回收
 *    6.2 栈保存的：
 *        8种基本类型的变量  +对象的引用变量   +实例方法都是在函数的栈
 *    6.3  java.lang.StackOverflowError  //Error 错误不是异常
 *          栈内存有限  递归死循环内存撑爆了          堆    溢出    错误
 *
 * 栈也叫栈内存，主管Java程序的运行，是在线程创建时创建，
 * 它的生命期是跟随线程的生命期，线程结束栈内存也就释放，
 * 对于栈来说不存在垃圾回收问题，只要线程--结束该栈就Over，
 * 生命周期和线程一致，是线程私有的。
 *
 * "8种基本类型的变量+对象的引用变量"+实例方法都是在函数的栈内存中分配。
 *
 * 栈帧中主要保存3类数据:
 * 本地变量(Local Variables) :输入参数和输出参数以及方法内的变量;
 * 栈操作(Operand Stack) :记录出栈、入栈的操作;
 * 栈帧数据(Frame Data) :包括类文件、方法等等。
 *     刚开始执行main方法               main栈帧           压入栈中
 *     main调用StackText方法           StackText栈帧      压入栈中
 *     StackText调用changeValue1方法   changeValue1栈帧   压入栈中
 *     changeValue1方法执行完后  弹出changeValue1  继续执行StackText...
 * *********************************
 * 程序 = 算法+数据结构
 * 程序 = 框架+业务逻辑//工作中
 * 队列（FIFO）先进先出
 * 栈（FILO）先进后出
 * java方法   叫栈帧
 * **********************************
 */
public class 栈stack {
    public static void main(String[] args) {
        StackText();
    }

    public  void changeValue1(int  age){age=30;}//传的是复制品
    public void changeValue2(Stack1 stack1){
        stack1.setName("xxx");
    }//传的是地址
    public void changeValue3(String str){
        str="xxx";
        //String传的也是地址   但是String是不可变的
        // 修改它等同于 自动找新的空间地址改变
    }

    private static void StackText() {
        栈stack jvm=new 栈stack();

        int age=20;//基本类型  穿得是复印件  值
        jvm.changeValue1(age);
        System.out.println(age);//20

        Stack1 person =new Stack1("abc");
        jvm.changeValue2(person);//引用类型  传的是地址
        System.out.println(person.getName());//xxx

        String str="abc";//String  特殊 传的是地址  但是如果穿的值修改了  修改的不是源地址的值
        jvm.changeValue3(str);//他传的str会重新 指定一个新地址  原本的str还是原来的str
        System.out.println(str);//abc   他保存在字符串常量池中
    }//堆得分层

    private static void stackOverflowError() {
        stackOverflowError();//递归
        //java.lang.StackOverflowError//Error 错误不是异常
    }//栈溢出错误   栈满了


}
class Stack1{
    public String a=null;

    public Stack1(String abc) {
        this.a=abc;
    }
    public Stack1() {

//        空参默认有 super();
    }

    public void setName(String xxx) {
        this.a=xxx;
    }

    public String getName() {
        return this.a;
    }

}
