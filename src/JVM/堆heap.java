package JVM;

/**
 * 堆heap  有垃圾回收  存储的是  一切new出来的对象
 *    物理上的堆只有新生区和养老区    概念上有永久代（jdk7） /元空间（jdk8）
 *                 <
 *                 | 1/3 1: 新生区(Young)(共十份)  new   Eden ：GC==YGC==轻GC（伊甸区和from）满了
 *                 |       8份   1.1:  伊甸区（Eden Space）满了 （死了一大部分（活得年龄+1））->
 *                 |       1份   1.2:  幸存者0区(S0  from) （每一次GC 谁存活(活得年龄+1)放在from） ->
 *                 |       1份   1.3:  幸存者1区(S1  to（谁空谁是to）)   （年龄为15（默认）的） ->养老区
 *   7 堆 heap    <  2/3 2：养老区(old) （再满了）   Full GC==FGC  （筛选(全筛选包括young)）
 *                 |        一直满  实在没办法 出现OOM异常(java.lang.OutOfMemoryError)堆内存不足
 *                 |
 *                 |    3：永久代（jdk7） /元空间（jdk8）
 *                 |         //jdk7  （其实在heap 但他自己不认为）
 *                 |         //java8 以后原空间不在jvm了  在使用本机物理机
 *                 |        永久代是堆的一部分，和新生代，老年代地址是连续的，而元空间属于本地内存
 *                 |        元空间存储类的元信息，静态变量和常量池等并入堆中。相当于永久代的数据被分到了堆和元空间中
 *                 <
 *  8.heap-->对象的生命周期---》OOM(java.lang.OutOfMemoryError) 内存不足错误
 *     更改java的虚拟机默认最大内存 -Xms1024m -Xmx1024m -XX:+PrintGCDetails
 *     -Xmn：新生区
 *     GC算法：
 *           1.引用计数法
 *                  //不咋用  缺点 每次赋值都要引用计数起麻烦    无法循环
 *           2.复制算法（Copying）
 *                  //常用算法   是年轻代用的垃圾回收算法    优点：不会产生内存碎片    缺点：费空间
 *           3.标记清除（Mark-Sweep）
 *                  //用在老年代    优点：节约空间    缺点：产生内存碎片  扫描2次  耗时长
 *           4.标记压缩（标记整理）（Mark-Compact）
 *                  //  老年代（多次清除时在压缩）  缺点：耗时最长（标记清除+整理）
 */
public class 堆heap {//存储的是  一切new出来的对象

    public static void main(String[] args) {
        OutOfMemoryError_Java_heap_space();
    }
    private static void OutOfMemoryError_Java_heap_space() {
        /**
         * 配置
         * -Xms10m -Xmx10m -XX:+PrintGCDetails
         */
//        String str = "java.lang.OutOfMemoryError";
//        while (true){
//            str+= str+new Random().nextInt(11111111)+new Random().nextInt(2222222);
//            str.intern();
//        }
        byte[] bytes=new byte[80*1024*1024];
    }  //堆溢出


}
