package JVM;

 /**
  * 3.Native方法表示本地方法库(了解)
  *     调用c++的程序  但现在越来越少 NativeThread
  *    3.1  Native是关键字
  *    3.2  有声明  没实现  调用c++的程序  但现在越来越少 NativeThread
  *
  *    本地接口的作用是融合不同的编程语言为Java 所用，它的初衷是融合c语言程序，
  *    Java诞生的时候是c语言横行的时候，要想立足，必须有调用c语言程序，
  *    于是就在内存中专门开辟了一块区域处理标记为native的代码，
  *    它的具体做法是Native Method Stack中登记native方法，
  *    在Execution Engine 执行时加载native libraries。
  *    目前该方法使用的越来越少了，除非是与硬件有关的应用
  *    比如通过Java程序驱动打印机或者Java系统管理生产设备，
  *    在企业级应用中已经比较少见。因为现在的异构领域间的通信很发达，
  *    比如可以使用Socket通信，也可以使用Web Service等等，不多做介绍。
  */
public class Native本地方法栈 {
     public static void main(String[] args) {

     }
     private static void NativeThread() {
         Thread t=new Thread();
         t.start();
         //t.start();//java.lang.IllegalThreadStateException start两次不可以
     }//Native 方法表示本地方法库 调用c/c++的程序  但现在越来越少 NativeThread

 }
