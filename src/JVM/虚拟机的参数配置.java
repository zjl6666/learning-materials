package JVM;

import java.util.Random;

/**
 *  第一种擦看参数盘点家底
 * jps(查看java后台进程) -l
 * jinfo(查看java 具体进程详细信息) -flag  -flags(查看所有)
 *        PrintGCDetails        线程号//是否开启垃圾回收
 *        MetaspaceSize         线程号//原空间大小
 *        MaxTenuringThreshold  线程号//进入养老区计数多少
 * 更改java的虚拟机默认最大内存 -Xms1024m(初始堆内存) -Xmx1024m(最大堆内存) -XX:+PrintGCDetails（开启垃圾回收细节） -XX:MetaspaceSize=1024M(设置原空间大小)
 *   JVM参数类型:  1.标配参数 java     -version(java版本)和 -help
 *               2.x参数（不重要）  java
 *                           -Xint(解释执行)
 *                           -Xcomp(第一次使用就编译成本地代码)
 *                           -Xmixed(混合模式)
 *               3.xx参数
 *                   1.Boolean  -XX:+(表开启) -(表关闭)
 *                   2.KV设值类型
 *  可能会出现  命令有问题的红色字体
 *      File->Settings->Tools->Terminal->Application Settings
 *              Shell path: C:\Windows\system32\cmd.exe
 *  路径>jps -l//擦看自己的进程
 * 10448
 * 4592 jdk.jcmd/sun.tools.jps.Jps
 * 25860 org.jetbrains.idea.maven.server.RemoteMavenServer36
 * 6948 org.jetbrains.jps.cmdline.Launcher
 * 10840 JVM.虚拟机的参数配置
 *
 * 路径>jinfo -flag PrintGCDetails 14644
 * -XX:+PrintGCDetails
 *
 * 路径>jinfo -flags 进程号//所有配置
 *
 *  路径>java -XX:+PrintFlagsInitial            //查看所有默认    =默认
 *  路径>java -XX:+PrintFlagsFinal -version     //查看所有修改   :=是修改过的或者是自己修改过
 *  路径>java -XX:+PrintCommandLineFlags -version  //查看默认的垃圾回收器
 *
 *  Print//打印
 *
 */

/**
 *  常用参数:
 * -Xms  等价于-XX:initialHeapSize //初始大小内存  默认为物理内存的1/64
 * -Xmx  等价于-XX:MaxHeapSize     //最大分配内存  默认为物理内存的1/4
 * -Xss  等价于-XX:ThreadStackSize //单个线程栈的大小  一般默认为513k-1024k
 *       默认显示0  大小：Linux/x64(64-bit):1024k  OS X(64-bit):1024k Oracle Solaris/x64(64-bit):1024k
 * -Xmn       //新生代大小
 * -XX:MetaspaceSize   //原空间大小  默认20多m
 *
 * -XX:+UseSerialGC     串行垃圾回收器//默认并行
 * -XX:+UseParallelGC   并行垃圾回收器
 * -XX:+PrintGCDetails  查看垃圾回收细节（日志）
 * -XX:SurvivorRatio   设置新生代中eden和S0/S1空间的比例
 *     默认-XX:SurvivorRatio=8,   Eden:S0:S1=8:1:1
 *        -XX:SurvivorRatio=4,   Eden:S0:S1=4:1:1
 *     SurvivorRatio得值是设置eden的比例
 * -XX:NewRatio    设置新生代和老年代的比例
 *     默认-XX:NewRatio=2      新生代:老年代=1:2
 *        -XX:NewRatio=4      新生代:老年代=1:4
 * -XX:MaxTenuringThreshold   设置最大年龄数
 *     默认-XX:MaxTenuringThreshold=15(必须0-15)
 * -XX:MetaspaceSize=8m -XX:MaxMetaspaceSiz=8m//设置原空间大小
 * -XX:MaxDirectMemorySize=5m  //最大直接内存化   即最大使用的内存
 *
 */

public class 虚拟机的参数配置 {
    public static void main(String[] args) {
        heap_xms_xmx();
    }
    private static void OutOfMemoryError_Java_heap_space() {
        /**
         * 配置
         * -Xms10m -Xmx10m -XX:+PrintGCDetails
         * -XX:+PrintGCDetails 查看垃圾回收细节（日志）
         */
        String str = "java.lang.OutOfMemoryError";
        while (true){
            str+= str+new Random().nextInt(11111111)+new Random().nextInt(2222222);
            str.intern();
        }

    }  //堆溢出.

    private static void heap_xms_xmx() {
        //-Xms10m -Xmx10m -XX:+PrintGCDetails   //配置
        //在"生产上"  Xmx（最大内存）  Xms（初始值大小）  必须一样   避免内存忽高忽低产生停顿 产生莫名其妙的异常
        System.out.println(Runtime.getRuntime().availableProcessors());//看你几核处理器
        long maxMemory=Runtime.getRuntime().maxMemory();//返回Java虚拟机默认试图使用的最大内存   物理内存的1/4
        long totalMemory =Runtime.getRuntime().totalMemory();//返回Java虚拟机总容量           物理内存的1/64
        System.out.println("Xmx:"+maxMemory+"(字节)\t    "+(maxMemory)/(double)1024/1024+"MB");
        System.out.println("Xms:"+totalMemory+"(字节)\t    "+(totalMemory)/(double)1024/1024+"MB");
    }   // 虚拟机默认最大内存

    /**
     * -XX:+PrintGCDetails  查看垃圾回收细节（日志）
     * [GC (Allocation Failure) [PSYoungGen: 2026K   -> 503K        (2560K)]   2026K->       719K      (9728K), 0.0007752 secs] [Times: user=0.00   sys=0.00, real=0.00 secs]
     *  使用GC                      使用YGC   原使用内存   GC后使用内存 新生区总内存  堆原使用大小   GC后堆使用内存 堆总大小    用时时间    秒           YGC用户耗时  YGC系统耗时  YGC实际耗时
     * [GC (Allocation Failure) [PSYoungGen: 41K->0K(2560K)] 5841K->5800K(9728K), 0.0005822 secs] [Times: user=0.00 sys=0.00, real=0.00 secs]
     * [Full GC (Ergonomics) [PSYoungGen: 0K->0K(2560K)] [ParOldGen: 5800K->4461K(7168K)] 5800K->4461K(9728K), [Metaspace: 3307K->3307K(1056768K)], 0.0045814 secs] [Times: user=0.09 sys=0.00, real=0.01 secs]
     * [GC (Allocation Failure) [PSYoungGen: 0K->0K(1536K)] 4461K->4461K(8704K), 0.0002446 secs] [Times: user=0.00 sys=0.00, real=0.00 secs]
     * [Full GC (Allocation Failure) [PSYoungGen: 0K->0K(1536K)] [ParOldGen: 4461K->4441K(7168K)] 4461K->4441K(8704K), [Metaspace: 3307K->3307K(1056768K)], 0.0047600 secs] [Times: user=0.00 sys=0.00, real=0.00 secs]
     *  使用FGC                                                   使用FGC      原大小  FGC后大小 总大小                       原空间
     *
     * Heap
     *  PSYoungGen      total 1536K, used 51K [0x00000000ffd00000, 0x0000000100000000, 0x0000000100000000)
     *   eden space 1024K, 4% used [0x00000000ffd00000,0x00000000ffd0cc50,0x00000000ffe00000)
     *   from space 512K, 0% used [0x00000000fff80000,0x00000000fff80000,0x0000000100000000)
     *   to   space 1024K, 0% used [0x00000000ffe00000,0x00000000ffe00000,0x00000000fff00000)
     *  ParOldGen       total 7168K, used 4441K [0x00000000ff600000, 0x00000000ffd00000, 0x00000000ffd00000)
     *   object space 7168K, 61% used [0x00000000ff600000,0x00000000ffa56560,0x00000000ffd00000)
     *  Metaspace       used 3339K, capacity 4500K, committed 4864K, reserved 1056768K
     *   class space    used 363K, capacity 388K, committed 512K, reserved 1048576K
     */
}
