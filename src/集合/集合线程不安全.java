package 集合;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.CopyOnWriteArraySet;

public class 集合线程不安全 {
    public  static void main(String[] args) {

        listNotSafe();
    }

    private static void 集合异常是因为迭代器在遍历时对集合的改变() {
        /**
         * 快速失败机制：在遍历的时候  如果数组修改了   就会触发   导致
         * java.util.ConcurrentModificationException 会出现这种异常 并发异常
         */
        List<String> list=new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            list.add(i+"");
        }
        for (String s:list){
            list.add(10+"");
            System.out.println(s);
        }
    }

    private static void mapNotSafe() {
        Map<String,String> map= new ConcurrentHashMap<>();//安全//new HashMap<>();//不安全
        //new Hashtable<>();//
        //最好的解决方式  一种新的接口      并发    HashMap
        /**
         * 在1.8之前   用的是ReentrantLock和分段锁的思想
         * 1.8  觉得太过复杂   改成了 synchronized加原语中的 CAS来进行保证安全
         *      并且每个节点都是volatile修饰的   所以不用给get加锁就能保证不脏读（数据是最新的）
         */

        /**
         * HashMap k,v键值对
         * put方法其实是放到node<k,v>  节点上了
         *
         * modCount 用于记录HashMap的修改次数,
         * 在HashMap的put(),get(),remove(),Iterator()等方法中,都使用了该属性
         * 由于HashMap不是线程安全的,所以在迭代的时候,会将modCount赋值到迭代器的expectedModCount属性中,然后进行迭代,
         * 如果在迭代的过程中HashMap被其他线程修改了,modCount的数值就会发生变化,
         * 这个时候expectedModCount和ModCount不相等,
         * 迭代器就会抛出ConcurrentModificationException()异常
         */
        HashMap<String,String> m=new HashMap<>();
        for (int i = 0; i < 10; i++) {
            new  Thread(()->{
                map.put(UUID.randomUUID().toString().substring(0,8),"0");//随机产生8个字符
                System.out.println(map);
            },"a").start();
        }
        /**
         * HashMap 是可以   put(null,null)
         * ConcurrentHashMap  不可以
         * 因为  他添加的时候 if (key == null || value == null) throw new NullPointerException();
         * 会判断
         * 原因是  如果可以添加null  就会引起  “二义性问题”
         * 因为 ConcurrentHashMap 为多线程被使用，所以会出现“二义性问题”  所以被禁用了
         */
    }  //map线程不安全

    private static void setNotSafe() {
        CopyOnWriteArraySet<String> set = new CopyOnWriteArraySet<>();//成为有序


        //最好的解决方式  一种新的接口   复制写操作

        for (int i = 0; i < 10; i++) {
            new  Thread(()->{
                set.add(UUID.randomUUID().toString().substring(0,8));//随机产生8个字符
                System.out.println(set);
            },"a0"+i).start();
        }
        for (int i = 0; i < 10; i++) {
            set.add(10-i+"");//随机产生8个字符
        }
    }    //set线程不安全

    private static void listNotSafe() {

        /**
         *
         * List<String> list=new ArrayList<>();
         * java.util.ConcurrentModificationException 会出现这种异常 并发异常
         *
         * List<String> list=new Vector<>(); //它自带锁synchronized   不会并发异常
         * List<String> list= Collections.synchronizedList(new ArrayList<>()); //强行加锁
         * new CopyOnWriteArrayList<>(); //最好的解决方式  一种新的接口
         * public boolean add(E e) {
         *         final ReentrantLock lock = this.lock;
         *         lock.lock();
         *         try {
         *             Object[] elements = getArr(ay);
         *             int len = elements.length;
         *             Object[] newElements = Arrays.copyOf(elements, len + 1);
         *             newElements[len] = e;
         *             setArray(newElements);
         *             return true;
         *         } finally {
         *             lock.unlock();
         *         }
         *     }
         */


        String[] s = {"1","2"};
        List<String> ls = Arrays.asList(s);

        //不能add 和 remove Arrays#asList 返回的 ArrayList 仅仅只是 Arrays 一个内部类，
        // 并非真正的 java.util.ArrayList
//        ls.add("qq");
        System.out.println(ls);


        List<String> list= new CopyOnWriteArrayList<>();//写实复制  读写分离思想
        //最好的解决方式  一种新的接口   复制写操作


//        for (int i = 0; i < 100; i++) {
//            list.add(UUID.randomUUID().toString().substring(0,8));//随机产生8个字符System.out.println(list);
//
//        }
        for (int i = 0; i < 100; i++) {
            new  Thread(()->{
                list.add(UUID.randomUUID().toString().substring(0,8));//随机产生8个字符System.out.println(list);
                System.out.println(list);
            },"a"+i).start();
        }
//        for (int i = 0; i < 100; i++) {
//            new  Thread(()->{
//                while (list.size()<=0);
//                list.remove(0);
////                System.out.println(list);
//            },"b"+i).start();
//        }
//        System.out.println(list.size());
//        Optional<String> any = list.stream().filter(s -> s.isEmpty()).findAny();
//        System.out.println(any.orElse("为空"));
        while (Thread.activeCount()>2);
        System.out.println(list.size());//xiao于总添加数
    }   //list线程不安全
}
