public class 路径 {

    public static void main(String[] args) {
        int[][] is = new int[][]{{1, 2}, {2, 3}, {3, 4}, {2, 9}, {4, 9}, {6, 7}, {2, 4}};
        dfs(is, 999, 0, 1, 9);

    }

    private static boolean dfs(int[][] is, int m, int k, int d, int mb) {//  m最小路径  k当前步骤  d当前版本  mb目标版本
        for (int i = 0; i < is.length; i++) {
            boolean a = false;
            if (is[i][0] == d) {
                if (is[i][1] == mb) {
                    System.out.print(is[i][1] + "->");
                    return true;
                } else {
                    if (k >= m) {
                        a = false;
                    } else {
                        a = dfs(is, m, ++k, is[i][1], mb);
                        if (a && k < m) {
                            m = k;
                            System.out.print(is[i][1] + "->");
                        }
                    }
                }
            }
            if (a) {
                System.out.println();
            }
        }
        return false;
    }
}
