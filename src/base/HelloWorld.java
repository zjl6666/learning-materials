package base;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

/**
 *对于整数，有四种表示方式:
 * >二进制(binary): 0,1，满2进1.以0b或0B开头。
 * >十进制(decimal): 0-9，满10进1。
 * >八进制(octal): 0-7，满8进1.以数字0开头表示。
 * >十六进制(hex): 0-9及A-F，满16进1.以0x或ex开头表示。此处的A-F不区分大小写。
 * 如: 0x21AF +1= 0X21B0
 *
 * javadoc -d 自己起的名字 -author -version HelloJava. java
 *
 *
 * << 空位补0，被移除的高位丢弃，空缺位补0。
 * >> 被移位的二进制最高位是0，右移后，空缺位补0; 最高位是1，空缺位补1。
 * >>>  被移位二进制最高位无论是0或者是1，空缺位都用0补。
 * & 二进制位进行&运算，只有1&1时结果是1，否则是0;
 * | 二进制位进行|运算，只有0 | 0时结果是0，否则是1;
 * ^ 相同二进制位进行^运算，结果是0; 1^1=0 ，0^0=0
 *  不相同二进制位^运算结果是1。1^0=1， 0^1=1
 * ~ 正数取反，各二进制码按补码各位取反
 *  负数取反，各二进制码按补码各位取反
 */
import java.lang.*;

public class HelloWorld {
    public static void main(String[] args) {
        int a=0b101101;//二进制
        System.out.println(Integer.toBinaryString(a));
        System.out.println();
//        int b=0561654167;//八进制
//        int c=0x64f67;//十六进制
//        System.out.println(a);
//        System.out.println(-999>>1);
       System.out.println(-999>>>1);

        /**
         * 1.java.lang----包含一些Java语言的核心类，如String、Math、Integer、System和Thread，提供常用功能。
         * 2.java.awt----包含了构成抽象窗口工具集（abstract window toolkits）的多个类，
         *              这些类被用来构建和管理应用程序的图形用户界面(GUI)。
         * 3.java.applet----包含applet运行所需的一些类。
         * 4.java.net----包含执行与网络相关的操作的类。
         * 5.java.io----包含能提供多种输入/输出功能的类。
         * java.util----包含一些实用工具类，如定义系统特性、使用与日期日历相关的函数。
         */
//        Sca(args);



    }
    private static void 八大基本类型() {

        /**
         * 整数型    数组元素是整型: 0
         *      byte:  1字节   8bit
         *      short  2字节   16bit
         *      int    4字节   32bit
         *      long   8字节   64bit
         * 浮点型    数组元素是浮点型: 0.0
         * 规范浮点数的小数点左侧必须为1，所以在保存尾数时，
         * 可以省略小数点前面这个1，从而腾出一个二进制位来保存更多的尾数
         *      float  4字节   32bit   1位符号位  8位指数为   23位小数位（尾数）
         *          单精度类型,精度是8位有效数字，
         *          取值范围是10的-38次方到10的38次方，
         *          浮点数被表示为尾数乘以2的指数次方再带上符号
         *      double 8字节   64bit  1位符号位  11位指数为   52位小数位（尾数）
         *          双精度类型，精度是17位有效数字，
         *          取值范围是10的-308次方到10的308次方
         * 字符型  元素是char型: 0或'u0000'，而非'0'
         *      short  2字节   16bit
         * boolean	4（根据编译环境而定）	false
         *
         * 关于boolean占几个字节，众说纷纭，
         * 虽然boolean表现出非0即1的“位”特性，但是存储空间的基本计量单位是字节，不是位。
         * 所以boolean至少占1个字节。
         * JVM规范中，boolean变量当作int处理，也就是4字节；
         * 而boolean数组当做byte数组处理，即boolean类型的数组里面的每一个元素占1个字节。
         *
         *          *
         *
         */
    }

    private static void 进制转换() {
        int a=1;
        System.out.println(a++ + ++a + ++a+a++);//12   1+3+4+4
        int abc=0x1fa;

        System.out.println(Integer.parseInt("z", 36));//z  在36进制中 十进制的数   36进制的z-》10进制中的35
//        String str="abc";
//        Integer[] integers = new Integer[5];
//        System.out.println(integers);
//        System.out.println(Integer.toString(9));

    }

    private static void ArraysDemo() {
        /**
         * 数组操作
         * a.equals(b)//a和b是否相等
         *
         */
        String[] a= new String[]{"AA","BB","CC","DD","EE"};
        String B="BB";
        Arrays.fill(a,"1");//数组遍历赋值
        System.out.println(Arrays.toString(a));//遍历

        for (int i = 0; i < a.length; i++) {
            if(a[i].equals(B)){//a和b是否相等   Arrays.equals(a[i],B)//int数组
                System.out.println("相等");
            }
        }
    }

    private static void Array02Demo() {
        int[][] arr = new int[][]{{0,0,0},{0,5},{1,1,1}};
        System.out.println(arr[1][1]);
        int[][] arr1 = new int[3][];
        arr1[1]=new int[5];//加个这不会异常
        System.out.println(arr1[1][1]);//空指针异常
        System.out.println(arr.length);
        System.out.println(arr[1].length);
        int[][] arr01 = new int[4][];
        System.out.println(arr01[0]);//空  只有当定义时  只有第一个中括号有数字 为空


    }

    private static void Array01Demo() {
        /**
         * 赋值和给数组个数
         * 只能用一个
         *
         * ⑤数组元素的默认初始化值
         * >数组元素是整型: 0
         * >数组元素是浮点型: 0.0
         * >数组元素是char型: 0或'u0000'，而非'0'
         * >数组元素是boolean型: false
         *
         * >数组元素是引用数据类型: null
         *
         * float:单精度类型,精度是8位有效数字，取值范围是10的-38次方到10的38次方，float占用4个字节的存储空间
         * double:双精度类型，精度是17位有效数字，取值范围是10的-308次方到10的308次方，double占用8个字节的存储空间
         */
        int[] ids = new int[]{1,2,3,4};
        System.out.println(ids[1]);
        String[] name=new String[5];//数组大小为5
        System.out.println(ids.length);
        String[] arr=new String[5];
        arr[1]="111";
        arr = new String[2];//会重新定义   地址改变  之前的成为垃圾
        System.out.println(arr[1]);
    }

    private static void Sca(String[] args) {
        Scanner cin = new Scanner(System.in);
        String s="sasasa";
        char c=s.charAt(2);
        switch (c){
            default:
                System.out.println("结束");
            case 'a':
                System.out.println("A");
                break;
            case 'c':
                System.out.println("aaaa");

        }
        System.out.println(args.length);
        for (String a: args){
            System.out.println(a);
        }
    }
}
