package base;

import java.util.Arrays;
import java.util.stream.Collectors;

public class 小问题 {
    public static void main(String[] args) {
                String str = "ab";
        str.chars().filter(x->x>3).forEach(x-> System.out.println(x));//97   98    a的阿斯克码97
//
        Arrays.asList(4,5,9,10).parallelStream()//多线程stream
                .filter(m->{
                    System.out.println(m);
                    return (m>1);
                }).map(m -> m+1)
                .collect(Collectors.groupingBy(x ->x%10))//分组排序
                .forEach((x,y)-> System.out.println(y));//输出
        /**
         * //前四个是随机的  后四个固定
         * 10
         * 4
         * 9
         * 5
         * [10]
         * [11]
         * [5]
         * [6]
         */

//
//        Map<Integer,Integer> map = new HashMap<>();
//        map.put(5,6);
//        System.out.println(map.get(4));
    }

    private static void ArraysTest() {
        int a;
        Object o=null;
        if(o instanceof Arrays){
            System.out.println("666");
        }


        Integer[] integer=new Integer[5];
        if(integer instanceof Integer[]){
            System.out.println("666");
        }
    }
}
