package Zookeeper;

public class 配置信息 {
}
/**
 * 配置zoo.cfg文件
 * （1）重命名/opt/module/zookeeper-3.5.7/conf
 *      这个目录下的 zoo_sample.cfg 为 zoo.cfg
 * [atguigu@hadoop102 conf]$ mv zoo_sample.cfg zoo.cfg
 * （2）打开 zoo.cfg 文件
 * [atguigu@hadoop102 conf]$ vim zoo.cfg
 * #修改数据存储路径配置
 * dataDir=/opt/module/zookeeper-3.5.7/zkData
 * #增加如下配置
 * #######################cluster##########################
 * server.2=hadoop102:2888:3888
 * server.3=hadoop103:2888:3888
 * server.4=hadoop104:2888:3888
 * （3）配置参数解读
 * server.A=B:C:D。
 * A 是一个数字，表示这个是第几号服务器；  保存在myid里   为以后集群方便
 * 集群模式下配置一个文件 myid，这个文件在 dataDir 目录下，这个文件里面有一个数据
 * 就是 A 的值，Zookeeper 启动时读取此文件，拿到里面的数据与 zoo.cfg 里面的配置信息比
 * 较从而判断到底是哪个 server。
 * B 是这个服务器的地址；
 * C 是这个服务器 Follower 与集群中的 Leader 服务器交换信息的端口；
 * D 是万一集群中的 Leader 服务器挂了，需要一个端口来重新进行选举，选出一个新的
 * Leader，而这个端口就是用来执行选举时服务器相互通信的端口。
 * （4）同步 zoo.cfg 配置文件
 * [atguigu@hadoop102 conf]$ xsync zoo.cfg
 */
