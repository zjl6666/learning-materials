import java.util.BitSet;

public class 奇怪的类 {
    public static void main(String[] args) {
        BitSetDemo();

    }

    private static void BitSetDemo() {
        /**
         * 底层用Long数组存储，默认大小就是一个Long（64位）
         */
        BitSet bit1 = new BitSet(16);
        BitSet bit2 = new BitSet(16);
        //位运算
        bit1.and(bit2);//相当于bit1 & bit2   都为1   才为1
        bit1.or(bit2);//相当于bit1 | bit2    有  1   就为1
        bit1.xor(bit2);//相当于bit1 ^ bit2   相同0   不同1 (相当于+  不进位)
        bit1.set(5);//将第五位设为1  true
        bit1.get(5);//获得第五位的值    0/1   false/true
    }
}
