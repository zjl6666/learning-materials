public class Main {
    /**
     * 技术的分类
     *
     * ➢解决功能性的问题
     *  Java、
     *  Jsp、("不用"  动态页面)、
     *  RDBMS、(关系型数据管理)
     *  Tomcat、（服务器）
     *  HTML、（静态页面）
     *  Linux、（操作系统）  有点稳定  漏洞少   稳定传播
     *  Jdbc、（java数据库连接）
     *  SVN、  （工具   把项目整合到总项目中）
     * ➢解决扩展性的问题
     * Struts、
     * Spring、（设计性框架）
     * SpringMVC、（设计性框架）
     * Hibernate、（持久性框架）
     * Mybatis、（持久性框架）
     * ➢解决性能的问题
     * NoSQL、（不仅仅是SQL   专指非关系型数据库）
     * Java线程、
     * Hadoop、
     * Nginx、（负载均衡）通过某种算法   将不同的请求  分别前不同的服务器数据库进行访问
     * MQ、（消息队列）
     * ElasticSearch、(搜索引擎) 在项目中搜索
     */
}
/**
 * Java 核心基础、
 * Java 多线程、
 * 高并发、
 * Spring、
 * 微服务、
 * Netty 与 RPC、
 * Zookeeper、
 * Kafka、
 * RabbitMQ、
 * Habase、
 * 设计模式、
 * 负载均衡、
 * 分布式缓存、
 * Hadoop、
 * Spark、
 * Storm、
 * 云计算等
 *
 */
