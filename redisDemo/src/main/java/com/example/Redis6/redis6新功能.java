package com.example.Redis6;

public class redis6新功能 {
    public static void main(String[] args) {
        /**
         * ACl
         * Redis ACL是Access Control List (访问控制列表)的缩写,该功能允许根据可
         * 以执行的命令和可以访问的键来限制某些连接。。
         * 在Redis 5版本之前, Redis安全规则只有密码控制还有通过rename来调整
         * 危命令比如flushdb，KEYS *, shutdown 等。Redis 6则提供ACL的功能对用户
         * 进行更细粒度的权限控制:
         * (1)接入权限:用户名和密码。
         * (2)可以执行的命令。
         * (3)可以操作的KEY
         *
         * 连接redis后//redis-cli
         * acl list//查看所有用户权限
         * "user default  on                          nopass               ~*                   +@all"
         *       用户名和  是否启用on:启用  off:不启用     密码  nopass：没密码   可操作的key  *:都可以  可执行的命令
         * acl whoami//查看当前使用的用户名名称
         * acl cat//查看可用命令
         *
         * acl setuser 用户名 on >password ~key:* +get
         * // 添加用户        启用  密码     只能对带key开头的的键进行操作  只能用get操作
         *
         * auth mary 密码//进入这个用户
         *
         */
        /**
         * IO多线程
         * I0多线程其实指客户端交互部分的网络I0交互处理模块多线程，
         * 而非执行命令多线程。Redis6 执行命令依然是单线程。
         *
         * Redis 6加入多线程，但跟Memcached这种从I0处理到数据访问多线程的实现模式有些差异。
         * Redis的多线程部分只是用来处理网络数据的读写和协议解析，执行命令仍然是单线程。
         * 之所以这么设计是不想因为多线程而变得复杂，需要去控制key、lua、事务，
         * LPUSH/LPOP等等的并发问题。
         * 另外,多线程I0默认也是不开启的,需要再配置文件中配置:redis.conf
         *
         * io-threads-do-reads yes//yes:是开启    no：关闭
         * io-threads 4//支持线程数
         *
         */
        /**
         * 工具支持Cluster
         * 之前老版Redis想要搭"集群"需要单独安装ruby环境,
         * Redis 5将redis-trib.rb的功能集成到redis-cli。
         * 另外官方redis-benchmark工开始支持cluster模式了,
         * 通过多线程的方式对多个分片进行压测。
         *
         */
        System.out.println(3%9);
    }
}
