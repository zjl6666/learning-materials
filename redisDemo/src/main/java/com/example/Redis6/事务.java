package com.example.Redis6;

public class 事务 {
    /**
     * ●Redis事务是一个单独的隔离操作:事务中的所有命令都会序列化、
     * 按顺序地执行。事务在执行的过程中，不会被其他客户端发送来的命令
     * 请求所打断。
     * ●Redis事务的主要作用就是串联多个命令防止别的命令插队
     *
     * multi 开启事务
     *   之后的get  set操等对redis的作都保存起来
     *   先不执行     等到执行
     *   如果中间   有单词拼错  这个事务会强制停止
     *   但是      执行错误  如
     *   incr key//将key的value值+1  value必须为integer类型
     *   会跳过这个执行语句  其他依旧执行
     * exec 后开始一条一条执行
     *
     * discard //停止事务
     *
     * watch key1 key2 //监视key1等键  在事务开启前执行
     * 如果  这些建在事务执行前如果改动
     *  exec后会返回nil 也就是不执行
     * unwatch key1 key2 //取消监视
     *
     */

}
