package com.example.Redis6.Redis高级;

/**
 * 主从复制，//读写分离   容灾快速恢复
 * 就是主机数据更新后根据配置和策略，自动同步到备机的
 * master/slaver机制，Master以写为主， Slave以读为主
 *
 * 不能解决内存压力   只解决读写压力
 * 主：写     从：读
 * 配从(服务器)不配主(服务等).   拷贝多个redis.conf文件include
 * 开启daemonize yes          Pid文件名字pidfile
 * 指定端口port                Log文件名字
 * Dump.rdb名字dbfilename     Appendonly关掉或者换名字
 *
 * 主从复制的配置
 * info replication//打印主从复制的相关信息
 * slaveof ip port//效忠 主的ip 主的端口号
 * slaveof no one//不对任何一个效忠 //删除他的从服务器身份
 *
 * 一主二仆模式演示
 * 1切入点问题? slave1. slave2 是从头开始复制还是从切入点开/头开始复制
 * 复制?比如从k4进来，那之前的123是否也可以复制//可以
 * 2从机是否可以写? set可否?//不可以
 * 3主机shutdown后情况如何?从机是上位还是原地待命//原地待命
 * 4主机又回来了后，主机斯增记录，从机还能否顺利复制?//能
 * 5其中一台从机down后情况如何?依照原有它能跟上大部队吗?//能
 * 6主机回来后   依旧是主机
 *
 */
public class Redis集群方式一主从复制 {
    /**
     * 主从机配置 主机可读可写  从机 只能读不能写
     * ●.创建一个/myredis文件夹//
     *      mkdir /myredis
     *      cd /myredis
     * ●.复制redis.conf
     *      cp /etc/redis.conf /myredis/redis.conf
     *           linux安装Redis一般都在/etc下
     * ●.创建多个配置文件//为了方便一主两从  大概内容如下
     *    redis6379.conf//文件名称   创三个  其三个名字
     *      include /myredis/redis.conf//表示在哪个配置文件上修改 引入配置文件
     *      pidfile "/var/run/redis_6379.pid"
     *      port 6379
     *      dbfilename "dump6379.rdb"//关闭AOF否则"还要"该AOF相关配置
     * ● 在/myredis文件夹下
     *      redis-server redis6379.conf//以这个配置文件启动redis
     *      redis-server redis6380.conf
     *      redis-server redis6381.conf
     *
     *      ps -ef | grep redis//查看redis相关进程详细信息
     *      kill -9 进程号//杀死这个进程
     * ●redis-cli -p 6379//连接6379端口号的redis
     * ●info replication//查看当前redis的详情
     *       # Replication
     *       role:master//主机：master    从机：slave
     *       connected_slaves:0//从机个数
     *          slave0:ip=127.0.0.1,port=6380,state=online ,offset=28,lag=1
     *          //从机信息                     online可用状态
     *       master_failover state :no- failover
     *       master_repl id :76a8 f86ad5c35cd67dc 15e6332d f04747d635eb8
     *       master_repl id:00000000000000000000000000000000
     *       master_repl offset :0
     *       second_repl offset:-1
     *       repl_backlog_ active:0
     *       repl_backlog size : 1048576
     *       repl_backlog_first_byte_offset :0
     *       repl_backlog_histlen:0
     * ●slaveof 主机ip 主机端口号
     *      设置当前服务器为从服务器  但是从服务器从其后就无了
     *
     */

    /**
     * ➢复制原理  执行keys *   //他的key的查询顺序并不是一致的
     * 每次从机联通后，都会给主机发送sync指令
     * 主机立刻进行存盘操作，发送RDB文件,给从机
     * 从机收到RDB文件后，进行全盘加载
     * 之后每次主机的写操作，都会立刻发送给从机，从机执行相同的命令
     */

    /**
     * 薪火相传
     * 主 <- 从1 <- 从2
     * 当这个主死后   这个 从1 变成主
     * 当从1死后   主和从2没关系
     *
     * slaveof no one//不对任何一个效忠 //删除他的从服务器身份
     */
}
