package com.example.Redis6.Redis高级;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPoolConfig;
import redis.clients.jedis.JedisSentinelPool;

import java.util.HashSet;
import java.util.Set;

/**
 * 哨兵模式
 * ●反客为主的自动版，能够后台监控主机是否故障，
 *  如果故障了根据投票数自动将从库转换为主库.
 *
 * ➢配置哨兵
 * 调整为一主二从模式
 * ●自定义的/myredis目录下新建sentinel.conf文件
 * 在配置文件中填写内容:
 * ●sentinel monitor mymaster 127.0.0.1 6379 1
 *
 * 其中mymaster为监控对象起的服务器名称，1 为至少有多少个sentinel.conf
 * 哨兵同意迁移的数量。//也就是  只要有1个说主废了就要选新的主
 * 哨兵的启动：redis-sentinel /myredis/sentinel.conf
 * 哨兵默认端口//26379
 *
 * 当旧主服务器没了后 新主登基
 * 当旧主服务器上线后 旧主变成 从服务器
 *
 * 新主登基
 * 从下线的主服务的所有从服务里面挑选一个从服务，将其转成主服务
 * 选择条件依次为:
 * 1、选择优先级靠前的（值越小优先级越高） 优先级在redis.conf中slave-priority 100
 * 2、选择偏移量最大的                 偏移量是指获得原主数据最多的
 * 3、选择runid最小的从服务            每个redis实例启动后都会随机生成一个40位的runid
 *
 *
 */
public class Redis集群方式二哨兵模式 {
    private static JedisSentinelPool jedisSentinelPool = null;
    public static Jedis getJedisFromSentinel(){
        if(jedisSentinelPool == null){
            Set<String> sentinelSet = new HashSet<>();
            sentinelSet.add("127.0.0.1:26379");
            JedisPoolConfig jedisPoolConfig = new JedisPoolConfig();
            jedisPoolConfig.setMaxTotal(10);//最大可用连接数
            jedisPoolConfig.setMaxIdle(5);//最大闲置连接数
            jedisPoolConfig.setMinIdle(5);//最小闲置连接数
            jedisPoolConfig.setBlockWhenExhausted(true);//连接耗尽是否等待
            jedisPoolConfig.setMaxWaitMillis(2000);//等待时间 毫秒
            jedisPoolConfig.setTestOnBorrow(true);//取连接时进行ping pong测试
            jedisSentinelPool = new JedisSentinelPool("mymaster"
                    ,sentinelSet,jedisPoolConfig);
        }
        return jedisSentinelPool.getResource();
    }
}
