package com.example.Redis6;

public class 新的数据类型 {
    public static void main(String[] args) {
    }
    /**
     * Bitmaps  //适合大量的用户
     * Redis提供了Bitmaps这个“数据类型”可以实现对位的操作:。
     * (1) Bitmaps 本身不是一种数据类型，实际上它就是字符串( key-value )
     *     但是它可以对字符串的位进行操作。
     * (2)Bitmaps单独提供了一套命令，所以在Redis中使用Bitmaps和使用字
     *   符串的方法不太相同。可以把 Bitmaps 想象成--个以位为单位的数组,
     *   数组的每个单元只能存储0和1，数组的下标在Bitmaps中叫做偏移量。
     *
     * //二进制
     *   setbit key 偏移量 1//将偏移量这"位"的0改为1
     *   getbit key 偏移量//返回0/1   0表示没执行过    1：执行过
     *   bitcount key//返回设置为1的bit数
     *   bitcount key 0 -1//返回从第0个(每八位为1个bit)（0表示第一个）到-1（-1表示最后一个   -2表示倒数第二个）设置为1的bit数
     *   bitop and/or/not/xor destkey key1 key2
     *      //将key1和key2进行 and（交集）    or（并集）   not（非）    xor（异或）  保存到新的destkey键中
     *
     * //适合谁访问过  某个网站  ip是几   就把第几位改成1
     */
    /**
     * HyperLogLog//主要解决基数问题  会自动去重
     * 基数估计就是在误差可接受的范围内, 快速计算基数。
     * ①Redis HyperLogLog是用来做基数统计的算法, HyperLogLog的优点是,
     *  在输入元素的数量或者体积非常非常大时,
     *  计算基数所需的空间总是固定的、并且是很小的。
     * ②在Redis里面,每个HyperLogLog键只需要花费12 KB内存，就可以计算接近2^64个不同元素的基数。
     *  这和计算基数时，元素越多耗费内存就越多的集合形成鲜明对比。
     * ③但是,因为HyperLogLog只会根据输入元素来计算基数,而不会储存输入元素本身,
     *  所以HyperLogLog不能像集合那样,返回输入的各个元素。
     *
     *  pfadd key value1 value2//添加
     *  pfcount key//返回key有几个值
     *  pfmerge destkey key1 key2//将key1和key2求和（合并）   新的放到destkey里
     *
     *
     */
    /**
     * GEO 经纬度
     * Redis 3.2中增加了对GEO类型的支持。GEO , Geographic ,地理信息的缩写。
     * 该类型,就是元素的2维坐标，在地图上就是经纬度。redis 基于该类型,提供了经纬
     * 度设置,查询,范围查询,距离查询,经纬度Hash等常见操作。。
     *
     * 两极无法直接添加,一般会下载城市数据，直接通过Java程序一次性导入。
     * 有效的经度从-180度到180度。有效的纬度从-85.05112878 度到 85.05112878度。
     * 当坐标位置超出指定范围时，该命令将会返回一个错误。
     *      已经添加的数据,是无法再次往里面添加的。
     *
     * geoadd key 经度1 纬度1 member1  经度2 纬度2 member2//在key里存经纬度和这个经纬度的备注
     * geopos key member//返回key这个建的member对应的经纬度
     * geodist key member1 member2 m/km/ft/mi   //计算key的 member1和member1的距离
     *            m:米         km:千米     mi:英里       ft:英尺
     * georadius key 经度 纬度 距离 单位//以给定的经纬度为中心  距离为半径  返回在key里包含的member
     * georadiusbymember key member 距离 单位//以给定的member为中心  距离为半径  返回在key里包含的member
     * geohash key member1 member2//返回key的member1的哈希值
     */
}
