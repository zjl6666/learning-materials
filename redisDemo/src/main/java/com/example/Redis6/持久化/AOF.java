package com.example.Redis6.持久化;
//➢AOF和RDB同时开启，redis听AOF

/**
 * AOF //相对于  TCP/UDP    中的TCP
 * ●以日志的形式来记录每个写操作，
 * 将Redis执行过的所有写指令记录下来(读操作不记录)，
 * 只许追加文件但不可以改写文件，
 * Redis启动之初会读取该文件重新构建数据，
 * 换言之，Redis重启的话就根据日志文件的内容
 * 将写指令从前到后执行一次以完成数据的恢复工作。
 * ●AOF文件的保存路径，同RDB的路径一致。
 *
 * redis 文件下.conf//配置文件
 * appendonly no//yes 是开启
 * appendfilename "appendonly.aof"//文件名字
 *
 * ●AOF和RDB同时开启，系统默认取AOF的数据
 * 如遇到AOF文件损坏，可通过
 * redis-check-aof --fix appendonly.aof
 * 进行恢复  其实  没啥用
 *
 * ➢AOF同步频率设置
 * ●始终同步，每次Redis的写入都会立刻记入日志 # appendfsync always
 * ●每秒同步，每秒记入日志一次，如果宕机，本秒的数据可能丢失。 appendfsync everysec
 * ●把不主动进行同步，把同步时机交给操作系统。# appendfsync no
 *
 * Rewrite
 * AOF采用文件追加方式，文件会越来越大为避免出现此种情况,
 * 新增了重写机制，当AOF文件的大小超过所设定的阈值时,
 * Redis就会启动AOF文件的内容压缩，只保留可以恢复数据的
 * 最小指令集.可以使用命令bgrewriteaof.
 * 如set 1 1   set 1 2   set 1 3 //之前的都覆盖了  只保存set 1 3指令
 *  //根据redis保存的内容   智能识别出指令保存
 *
 * AOF文件持续增长而过大时，会fork出一条新进程来将文件
 * 重写(也是先写临时文件最后再rename),遍历新进程的内存
 * 中数据，每条记录有一条的Set语句。重写aof文件的操作,
 * 并没有读取旧的aof文件，而是将整个内存中的数据库内容用
 * 命令的方式重写了一个新的aof文件，这点和快照有点类似。
 * //根据redis保存的内容   智能识别出指令保存
 *
 * ➢何时重写
 * 重写虽然可以节约大量磁盘空间，减少恢复时间。但是每次重
 * 写还是有一定的负担的，因此设定Redis要满足一定条件才会
 * 进行重写。
 * auto-aof- rewrite- percentage 100
 * auto-aof- rewrite- min-size 64mb
 * ●系统载入时或者.上次重写完毕时，Redis会记录此时AOF大小,
 * 为base_size,如果Redis的AOF
 * 当前大小>= base_size+base_size*100% (默认)
 * 且当前大小>=64mb(默认)的情况下，
 * Redis会对AOF进行重写。
 *
 * ➢AOF优点
 * ●备份机制更稳健，丢失数据概率更低。
 * ●可读的日志文本，通过操作AOF稳健，可以处理误操作。
 * ➢AOF的缺点
 * ●比起RDB占用更多的磁盘空间。
 * ●恢复备份速度要慢。
 * ●每次读写都同步的话，有一定的性能压力。
 * ●存在个别Bug，造成恢复不能。
 *
 *
 */
public class AOF {
}
