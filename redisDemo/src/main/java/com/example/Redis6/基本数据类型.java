package com.example.Redis6;

/**
 * String
 * List/可以重复  只能存String
 * set//不可重复  适合秒杀（一个人只能买一个）  set提供了某个成员是否在一个set集合里的重要接口
 * hash//是一个键值对集合   是一个string的映射表类似于Map<String,String>
 * zset//与set非常相似   他都关联了一个评分由从第到高方式排序  评分可重复
 * bitmap//位图
 * HyperLogLog//统计
 * GEO//地理
 * geospatial
 * Stream
 *
 *  小细节
 *     命令不区分大小写，而key是区分大小写的
 *
 */

public class 基本数据类型 {
    /**
     * Redis的五种数据类型的增删改查
     * 都是   key-value  key都是String
     * value：
     *   1.string：他是二进制安全的，意味着可以包含任何数据，如图片或序列化对象
     *        String类型是Redis最基本的数据类型，
     *        一个Redis中字符串value最多可以是512M
     *
     * Redis的增删改查
     *
     * select 数字//Redis  有及数据库16个数据库  0-15 数字几  进入几数据库
     * keys *   //查看当前数据库所有键
     * exists key /判断这个key是否存在  1表示true  0表示false
     * type key  //擦看这个建的值的类型
     * del key //删除这个键
     * unlink key //根据value选择非阻塞删除+仅将keys从keyspace元数据中删除,真正的删除会在后续异步操作、
     * expire key seconds//为键值设置过期时间seconds秒  单位秒
     * ttl key//查看这个键还有多少秒过期  -1表示永不过期  -2表示已过期
     * dbsize//查看当前数据库key的数量
     * Flushdb//清空当前库（0-15库）
     * Flushall//清空所有库 全部16个库
     */

    /** String//是一个动态字符串  类似于  ArrayList   预分配
     *      set key value  //添加键和值
     *      get key    //根据建返回值
     *      命令   升级版.
     *      set key value ex (过期时间秒) NX(但key不存在时才创建key ==setnx) 数字（过期时间）
     *      set key value px (过期时间毫秒秒) xx(覆盖) 数字（过期时间）
     *
     *      setnx key value//当key不存在时添加   当key存在时添加失败
     *      append key value //在原有key的value末尾添加
     *      strlen key //获得key的value长度
     *    integer
     *      incr key//将key的value值+1  value必须为integer类型
     *      decr key//将key的value值-1  value必须为integer类型
     *      incr/decr key 数字//key的value值 +/- 数字
     *       //经常用在   是否喜欢的文章      点赞
     *       //商品编号、订单号采用INCR命令生成
     *
     *      mset key1 value1 key2 value2   //同时添加多个key-value对
     *      mget key1 key2.。//同时获得多个key对应的value
     *      msetnx key1 value1 key2 value2//当"所有key"不存在时添加  当有一个key存在时添加失败
     *      getrange key 起始位置 结束位置//获得key对应的value的[起始位置 结束位置]的部分  0是第一个
     *      setrange key 起始位置 value//从起始位置开始[  覆盖成value
     *      setex key 过期时间 value//添加同时   设置过期时间
     *      getset key value//修改key的值为value  并返回原来的value
     *
     *      内部为当前字符串实际分配的空间一般要高于实际字符串长度
     *      当字符串长度小于1M时,扩容都是"加倍"现有的空间,如果超过1M,扩容时--次
     *      只会多扩1M的空间。需要注意的是字符串最大长度为512M。
     */
    /** List/可以重复  只能存String  是一个双向链表
     *      lpush/rpush key value1 value2等等//从左边/右边插入一个或多个值
     *         如lpush list a b c d e//从左边一个或多个值
     *           rpush list 1 2 3 4 5//从右边插入一个或多个值
     *           list  存的就是   e d c b a 1 2 3 4 5
     *      lpop/rpop key //从左边/右边吐出一个值  如果key的value取完了 这个key会自动删除
     *      rpoplpush key1 key2//从key1的右边吐出一个值  存在key2的左边
     *      lrange key start stop//查询key  从start开始到stop结束元素 0表示第一个（最左边） -1最后一个表示最右边
     *      lindex key index//获取指定下标的元素
     *      llen key //获取key的长度
     *      linsert key before/after value newvalue//插入 在key的指定value 左边/右边 插入newvalue
     *      lrem key n value//删除 key   n（n>0从左开始删   n<0从右开始删  n=0表示全部删除）个value//因为list可重复*
     *      lset key index value//将key的下标为index的值替换为value
     *      订阅的文章公共号
     *
     *      List的数据结构为块表quickList
     *      它将所有的元素紧挨着一起存储,分配的是一块连续的内存
     *          当数据量比较多的时候才会改成quicklist。
     *          因为普通的链表需要的附加指针空间太大,会比较浪费空间。比如这个列表里存的只
     *          是int类型的数据,结构上还需要两个额外的指针prev和next。。
     *               ziplist<->ziplist<->ziplist<->ziplist<->ziplist
     *          Redis将链表和ziplist结合起来组成了quicklist。也就是将多个ziplist使用双向指
     *          针串起来使用。这样既满足了快速的插入删除性能,又不会出现太大的空间冗余。。
     *
     */
    /** set//不可重复  适合秒杀（一个人只能买一个）  set提供了某个成员是否在一个set集合里的重要接口
     *      1.sadd key value1 value2 //添加元素
     *      2.smembers key//获取key所有的值
     *      3.sismember key value//判断key是否有这个value//1：有     0：没有
     *      4.scard key//返回key有多少value
     *      5.srem key value1 value2//删除key的value//返回删除了几个
     *      6.spop key//随机吐出一个值
     *      7.srandmember key n//随机获取key的n个值 不删除
     *      8.smove k1 k2 value//把k1的value移动到k2中   k1的value必定消失相当于5+1
     *      9.sinter key1 key2//返回这两个集合的交集
     *      10.sunion key1 key2//返回这两个集合的并集
     *      11.sdiff key1 key2//返回这两个集合的差集  key1-key2
     *      //微信抽奖小程序   spop key//随机"吐出"一个值//最早以前和哈希一样其实有顺序  但是修改了
     *      //给别人朋友圈点赞   sadd key value1//点赞    srem key value1//取消点赞
     *      //微博好友关注社交关系
     *           共同关注的人   sinter key1 key2//返回这两个集合的交集
     *           我关注的某个人还有谁关注了 sismember key value//判断key是否有这个value//1：有     0：没有
     *      //qq内推的共同的好友
     *
     *      Java中HashSet的内部实现使用的是HashMap ,不过所有的value都指向同一个对象。
     *      Redis的set结构也是一样,它的内部也使用hash结构,所有的value都指向同一个内部值。
     *      Set数据结构是 dict 字典,字典是用哈希表实现的。
     */
    /**  hash//是一个键值对集合   是一个string的映射表类似于Map<String,String>
     *      hset key field1 value1 field2 value2//给key  加field1-value1（键值对）
     *      hget key field //获取key建的  field 的对应的值
     *      hexists key field//擦看key  是否有  field的键
     *      hkeys key//列出key的所有的键field
     *      hvals key//列出key的所有的值value
     *      hgetall key//返回key的所有键值对 //键1\n 值1\n  键2\n 值2\n
     *      hincrby key field increment//给key  的键值对 field对应的值+increment//increment负数就为减
     *      hsetnx key field value//当field不存在时  才添加
     *      hlen key//获取key的数量
     *           结构
     *      Hash类型对应的数据结构是两种: ziplist (压缩列表)，hashtable (哈希表)。当
     *       field-value长度较短且个数较少时，使用ziplist,否则使用hashtable.
     *     购物车
     */
    /** zset//与set非常相似   他都关联了一个评分由从第到高方式排序  评分可重复
     *      zadd key score1 value1 score2 value2 //添加元素value并给他一个评分score
     *      zrange key start stop //start到stop元素 0是第一个 -1是最后一个 评分从低到高
     *      zrevrange key start stop withscores// 评分从高到低     加withscores可以返回评分
     *      zrangbyscore key min max//查询key评分在[min, max]的所有value   从小到大
     *      zrevrangbyscore key max min //查询key评分在[max,min]的所有value   从大到小
     *      zscore key value//擦看key value的评分
     *      zincrby key increment value//为key的value加评分increment
     *      zrem key value//删除key的value
     *      zcard key//返回key的值的个数
     *      zcount key min max //返回评分[min, max]的个数
     *      zrank key value//返回这个key 的value下标索引位置0开始
     *
     *      //抖音热搜
     *      有序集合在生活中比较常见,例如根据成绩对学生排名,根据得分对玩家排名等。
     *      对于有序集合的底层实现,可以用数组、平衡树、链表等。数组不便元素的插入、删除;
     *      平衡树或红黑树虽然效率高但结构复杂;链表查询需要遍历所有效率低。
     *      Redis采用的是跳跃表。跳跃表效率堪比红黑树,实现远比红黑树简单。。
     *
     */

}
